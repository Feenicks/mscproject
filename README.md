## Getting Started

### Prerequisites

What things you need to install the software:

```
npm install
```

### Compiling the Elm files

To compile the Elm files into JavaScript:

```
./node_modules/elm/binwrappers/elm-make lib/highlevel-elm/Exports.elm --output=lib/highlevel-elm/exports.js
./node_modules/elm/binwrappers/elm-make lib/virtualmachine-elm/VMExports.elm --output=lib/virtualmachine-elm/vmexports.js
./node_modules/elm/binwrappers/elm-make lib/stack-elm/StackExports.elm --output=lib/stack-elm/stackexports.js
```

or

```
npm run-script build-elm-1
npm run-script build-elm-2
npm run-script build-elm-3
```

### Testing the application

The Electron application:

```
npm start
```

The website on localhost:8000 :

```
python2.7 server.py
```

### Building the application

To build the Electron application

```
npm run-script package-windows
npm run-script package-linux
npm run-script package-darwin
```
