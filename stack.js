'use strict';

$(function(){
  $(document).ready(function() {

    document.createElement('callback-stackloaded');
    var node, app,
      stackCodeDiv, debuggingBox, hexCode, vmCodeDiv, vmHexCode,
      loadButton, top, bottom, hideBarButton, vmBox;

    /* Importing the elm compiler app */
    node = document.getElementById('stackCompiler');
    app = Elm.StackExports.embed(node, []);



    stackCodeDiv  = $('#stackCode');
    vmCodeDiv     = $('#vmCode');
    debuggingBox  = $('#debuggingBox');
    hexCode       = $('#hexCode');
    vmHexCode     = $('#vmHexCode');
    top           = $('#top');
    bottom        = $('#bottom');
    hideBarButton = $('#hideBarButton');
    vmBox         = $('#vmBox');

    /* New Stack code was received, sent it to compiler */
    stackCodeDiv.on('NEW_CODE', function () {
      app.ports.stackassembler.send([stackCodeDiv.text(), vmCodeDiv.text()]);
    });

    /* Stack compiler output */
    app.ports.result.subscribe(function(received) {
      if (loadButton == undefined) {
        loadButton = $("#loadButton");
      }

      hexCode.text(received[0]);
      vmHexCode.text(received[1]);

      /* Wrong result => Hide VM and disable load button */
      if (received[0] == "") {
        if (!(debuggingBox.prop("classList").contains("hidden"))) {
          loadButton.removeClass("enabled");
          top.addClass('full')
          debuggingBox.addClass("hidden");
          bottom.trigger("POSITION_CHANGE");
        }
      }
      /* Good result => Show VM and enable load button */
      else {
        loadButton.addClass("enabled");
        if (debuggingBox.prop("classList").contains("hidden")) {
          top.removeClass('full')
          debuggingBox.removeClass("hidden");
          bottom.trigger("POSITION_CHANGE");
        }
      }
    });

      hideBarButton.click(function(){
        if (hideBarButton.prop("classList").contains("up")) {
          hideBarButton.removeClass("up");
          hideBarButton.addClass("down");
          top.addClass("almostFull");
          debuggingBox.addClass("tiny");
          vmBox.addClass("hidden");
          bottom.trigger("POSITION_CHANGE");
        }
        else {
          hideBarButton.removeClass("down");
          hideBarButton.addClass("up");
          top.removeClass("almostFull");
          debuggingBox.removeClass("tiny");
          vmBox.removeClass("hidden");
          bottom.trigger("POSITION_CHANGE");
        }
      })

  });

})
