'use strict';

function initBlocklyBlocks () {

  /* ------- Variables --------- */

  Blockly.Blocks['create_variable'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("variable")
          .appendField(new Blockly.FieldTextInput("name"), "variable_name");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(60);
      this.setTooltip('Creating new variable');
      this.setHelpUrl('');
    }
  };

  Blockly.Blocks['assign_variable'] = {
    init: function() {
      this.appendValueInput("variable_value")
          .setCheck("Number")
          .setAlign(Blockly.ALIGN_CENTRE)
          .appendField(new Blockly.FieldVariable(""), "NAME")
          .appendField("=");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(60);
      this.setTooltip('Assigning value to variable');
      this.setHelpUrl('');
    }
  };

  Blockly.Blocks['arith_operations'] = {
    init: function() {
      this.appendValueInput("op1")
          .setCheck("Number");
      this.appendValueInput("op2")
          .setCheck("Number")
          .appendField(new Blockly.FieldDropdown([["+","+"], ["-","-"], ["*","*"], ["/","/"]]), "op");
      this.setInputsInline(true);
      this.setOutput(true, "Number");
      this.setColour(110);
      this.setTooltip('');
      this.setHelpUrl('');
    }
  };

  Blockly.Blocks['variable'] = {
    init: function() {
      this.appendDummyInput()
          .appendField(new Blockly.FieldVariable(""), "variable_name");
      this.setInputsInline(true);
      this.setOutput(true, "Number");
      this.setColour(110);
      this.setTooltip('');
      this.setHelpUrl('');
    }
  };

  Blockly.Blocks['number'] = {
    init: function() {
      this.appendDummyInput()
          .appendField(new Blockly.FieldNumber(0, -2147483648, 2147483647, 1), "NAME");
      this.setInputsInline(true);
      this.setOutput(true, "Number");
      this.setColour(100);
      this.setTooltip('');
      this.setHelpUrl('');
    }
  };

  /* ------- Notes --------- */

  var notes = "C1 C1# D1 D1# E1 F1 F1# G1 G1# A1 A1# B1 C2 C2# D2 D2# E2 F2 F2# G2 G2# A2 A2# B2 C3 C3# D3 D3# E3 F3 F3# G3 G3# A3 A3# B3 C4 C4# D4 D4# E4 F4 F4# G4 G4# A4 A4# B4 C5 C5# D5 D5# E5 F5 F5# G5 G5# A5 A5# B5 C6".split(" ")
  for (var i = 0; i < notes.length; i++) {
    var note = notes[i];
    Blockly.Blocks[note.toLowerCase()] = {
      init: function() {
        this.appendDummyInput()
            .appendField(this.type.toUpperCase());
        this.setInputsInline(true);
        this.setOutput(true, "Number");
        this.setColour(110);
        this.setTooltip('');
        this.setHelpUrl('');
      }
    };
  }

  /* ------- Colours --------- */

  var colours = "black blue green cyan red magenta yellow white".split(" ")
  for (var i = 0; i < colours.length; i++) {
    var colour = colours[i];
    Blockly.Blocks[colour.toLowerCase()] = {
      init: function() {
        this.appendDummyInput()
            .appendField(this.type);
        this.setInputsInline(true);
        this.setOutput(true, "Number");
        this.setColour(110);
        this.setTooltip('Can also use ' + colours.indexOf(this.type));
        this.setHelpUrl('');
      }
    };
  }


  /* --- Conditional Branches and Loops --- */

  Blockly.Blocks['if_then_else'] = {
    init: function() {
      this.appendValueInput("if_cond")
          .setCheck("Boolean")
          .appendField("if");
      this.appendDummyInput()
          .appendField("then");
      this.appendStatementInput("then_stat")
          .setCheck(null);
      this.appendDummyInput()
          .appendField("else");
      this.appendStatementInput("else_stat")
          .setCheck(null);
      this.appendDummyInput()
          .appendField("endif");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(160);
      this.setTooltip('');
      this.setHelpUrl('');
    }
  };

  Blockly.Blocks['if_then'] = {
    init: function() {
      this.appendValueInput("if_cond")
          .setCheck("Boolean")
          .appendField("if");
      this.appendDummyInput()
          .appendField("then");
      this.appendStatementInput("then_stat")
          .setCheck(null);
      this.appendDummyInput()
          .appendField("endif");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(160);
      this.setTooltip('');
      this.setHelpUrl('');
    }
  };

  Blockly.Blocks['while_do'] = {
    init: function() {
      this.appendValueInput("while_cond")
          .setCheck("Boolean")
          .appendField("while");
      this.appendDummyInput()
          .appendField("do");
      this.appendStatementInput("do_stat")
          .setCheck(null);
      this.appendDummyInput()
          .appendField("endwhile");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(160);
      this.setTooltip('');
      this.setHelpUrl('');
    }
  };

  Blockly.Blocks['bool_operations'] = {
    init: function() {
      this.appendValueInput("op1")
          .setCheck("Number");
      this.appendValueInput("op2")
          .setCheck("Number")
          .appendField(new Blockly.FieldDropdown([["==","=="], ["!=","!="], ["<=","<="], ["<","<"], [">",">"], [">=",">="]]), "op");
      this.setInputsInline(true);
      this.setOutput(true, "Boolean");
      this.setColour(160);
      this.setTooltip('');
      this.setHelpUrl('');
    }
  };

  /* --- Functions --- */

  Blockly.Blocks['assign_array_1'] = {
    init: function() {
      this.appendDummyInput()
          .appendField(new Blockly.FieldVariable(""), "val1")
      this.appendValueInput("function_value")
          .setCheck("Array")
          .appendField("=");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(200);
      this.setTooltip('');
      this.setHelpUrl('');
    }
  };

  Blockly.Blocks['assign_array_2'] = {
    init: function() {
      this.appendDummyInput()
          .appendField(new Blockly.FieldVariable(""), "val1")
      this.appendDummyInput()
          .appendField(new Blockly.FieldVariable(""), "val2")
      this.appendValueInput("function_value")
          .setCheck("Array")
          .appendField("=");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(215);
      this.setTooltip('');
      this.setHelpUrl('');
    }
  };

  Blockly.Blocks['assign_array_3'] = {
    init: function() {
      this.appendDummyInput()
          .appendField(new Blockly.FieldVariable(""), "val1")
      this.appendDummyInput()
          .appendField(new Blockly.FieldVariable(""), "val2")
      this.appendDummyInput()
          .appendField(new Blockly.FieldVariable(""), "val3")
      this.appendValueInput("function_value")
          .setCheck("Array")
          .appendField("=");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(230);
      this.setTooltip('');
      this.setHelpUrl('');
    }
  };

  /* --- Procedure and Function Definition --- */

  Blockly.Blocks['proc_def'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("define")
          .appendField(new Blockly.FieldTextInput("name"), "proc_name")
          .appendField(new Blockly.FieldTextInput("arguments"), "arguments")
          .appendField("as");
      this.appendStatementInput("proc_stat")
          .setCheck(null);
      this.appendDummyInput()
          .appendField("enddefine");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(160);
      this.setTooltip('');
      this.setHelpUrl('');
      this.setOnChange(function(changeEvent) {
        if (this.getFieldValue('proc_name').includes(" ")) {
          this.setWarningText("Cannot have blank in name. Use '_' instead.")
        }
        else {
          this.setWarningText(null);
        }
      });
    }
  };

  Blockly.Blocks['func_def'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("function")
          .appendField(new Blockly.FieldTextInput("name"), "func_name")
          .appendField(new Blockly.FieldTextInput("arguments"), "arguments")
          .appendField("as");
      this.appendStatementInput("func_stat")
          .setCheck(null);
      this.appendValueInput("return_values")
          .setCheck("Array")
          .appendField("return ");
      this.appendDummyInput()
          .appendField("endfunction");
      this.setInputsInline(false);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(245);
      this.setTooltip('');
      this.setHelpUrl('');
      this.setOnChange(function(changeEvent) {
        if (this.getFieldValue('func_name').includes(" ")) {
          this.setWarningText("Cannot have blank in name. Use '_' instead.")
        }
        else {
          this.setWarningText(null);
        }
      })
    }
  };

  Blockly.Blocks['array_1'] = {
    init: function() {
      this.appendValueInput("val1")
          .setCheck("Number");
      this.setInputsInline(true);
      this.setOutput(true, "Array");
      this.setColour(200);
      this.setTooltip('');
      this.setHelpUrl('');
    }
  };

  Blockly.Blocks['array_2'] = {
    init: function() {
      this.appendValueInput("val1")
          .setCheck("Number");
      this.appendValueInput("val2")
          .setCheck("Number");
      this.setInputsInline(true);
      this.setOutput(true, "Array");
      this.setColour(215);
      this.setTooltip('');
      this.setHelpUrl('');
    }
  };

  Blockly.Blocks['array_3'] = {
    init: function() {
      this.appendValueInput("val1")
          .setCheck("Number");
      this.appendValueInput("val2")
          .setCheck("Number");
      this.appendValueInput("val3")
          .setCheck("Number");
      this.setInputsInline(true);
      this.setOutput(true, "Array");
      this.setColour(230);
      this.setTooltip('');
      this.setHelpUrl('');
    }
  };

  Blockly.Blocks['start_block'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("Start");
      this.appendStatementInput("NAME")
          .setCheck(null);
      this.setColour(300);
      this.setTooltip('');
      this.setHelpUrl('');
      this.setDeletable(false);
    }
  };

}

function addBlocklyBlocks (procedures, functions) {
  function add_procedure(procedure_usage) {
    var name_args = procedure_usage.split(" ");
    Blockly.Blocks[name_args[0].toLowerCase()] = {
      init: function() {
        this.appendDummyInput()
            .appendField(name_args[0]);
        for (var i = 1; i < name_args.length; i++) {
          if (name_args[i] === "") {
            break;
          }
          this.appendValueInput("arg" + i)
              .setCheck("Number");
        }
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(160);
        this.setTooltip(procedure_usage);
        this.setHelpUrl('');
      }
    };
  }

  function add_function(function_usage) {
    var name_args = function_usage.split(" -- ")[0].split(" ");
    var return_number = function_usage.split(" -- ")[1].split(" ")[1];
    var color = [245, 200, 215, 230][return_number]
    Blockly.Blocks[name_args[0].toLowerCase()] = {
      init: function() {
        this.appendDummyInput()
            .appendField(name_args[0]);
        for (var i = 1; i < name_args.length; i++) {
          if (name_args[i] === "") {
            break;
          }
          this.appendValueInput("arg" + i)
              .setCheck("Number");
        }
        this.setInputsInline(true);
        this.setOutput(true, "Array");
        this.setColour(color);
        this.setTooltip(function_usage);
        this.setHelpUrl('');
      }
    };
  }

  for (var i = 0; i < procedures.length; i++) {
    add_procedure(procedures[i]);
  }
  for (var i = 0; i < functions.length; i++) {
    add_function(functions[i]);
  }
}
