'use strict'

function initBlocklyToolbox (proceduresUsage, functionsUsage) {
  var toolbox =
    `<xml>`;
  toolbox +=
      `<category name=\"Variables\">
        <block type=\"create_variable\"></block>
        <block type=\"assign_variable\"></block>
        <block type=\"arith_operations\"></block>
        <block type=\"variable\"></block>
        <block type=\"number\"></block>
      </category>`;

  toolbox +=
    `<category name=\"Notes\">`;
/*  var notes = "C1 C1# D1 D1# E1 F1 F1# G1 G1# A1 A1# B1 C2 C2# D2 D2# E2 F2 F2# G2 G2# A2 A2# B2 C3 C3# D3 D3# E3 F3 F3# G3 G3# A3 A3# B3 C4 C4# D4 D4# E4 F4 F4# G4 G4# A4 A4# B4 C5 C5# D5 D5# E5 F5 F5# G5 G5# A5 A5# B5 C6".split(" ") */
  var notes = "C3 C3# D3 D3# E3 F3 F3# G3 G3# A3 A3# B3 C4 C4# D4 D4# E4 F4 F4# G4 G4# A4 A4# B4 C5 C5# D5 D5# E5 F5 F5# G5 G5# A5 A5# B5 C6".split(" ")
  for (var i = 0; i < notes.length; i++) {
    var note = notes[i];
    toolbox += `<block type=\"` + note.toLowerCase() + `\"></block>`;
  }
  toolbox +=
    `</category>`;

  toolbox +=
    `<category name=\"Colours\">`;
  var colours = "black blue green cyan red magenta yellow white".split(" ")
  for (var i = 0; i < colours.length; i++) {
    var colour = colours[i];
    toolbox += `<block type=\"` + colour + `\"></block>`;
  }
  toolbox +=
    `</category>`;

  toolbox +=
      `<category name=\"Conditional Branches and Loops\">
        <block type=\"if_then_else\"></block>
        <block type=\"if_then\"></block>
        <block type=\"while_do\"></block>
        <block type=\"bool_operations\"></block>
        </category>`;

  toolbox +=
      `<category name=\"Procedures\">`
  for (var i = 0; i < proceduresUsage.length; i++) {
    var procedure_name = proceduresUsage[i].split(" ")[0]
    toolbox +=
      `<block type=\"` + procedure_name.toLowerCase() + `\"></block>`
  }
  toolbox +=
      `</category>`

  toolbox +=
      `<category name=\"Functions\">
      <block type=\"assign_array_1\"></block>
      <block type=\"assign_array_2\"></block>
      <block type=\"assign_array_3\"></block>
      `
  for (var i = 0; i < functionsUsage.length; i++) {
    var function_name = functionsUsage[i].split(" ")[0]
    toolbox +=
      `<block type=\"` + function_name.toLowerCase() + `\"></block>`
  }
  toolbox +=
      `</category>`

  toolbox +=
      `<category name=\"Procedure and Function Definition\">
        <block type=\"proc_def\"></block>
        <block type=\"func_def\"></block>
        <block type=\"array_1\"></block>
        <block type=\"array_2\"></block>
        <block type=\"array_3\"></block>
      </category>`;

  toolbox +=
    `</xml>`;

  return toolbox
}
