'use strict';

function getBlocklyHtml () {
  return `
    <html>
      <body>
          <link href="./lib/my-blockly/devArea.css" rel="stylesheet">
            <div id="blocklyDiv"></div>
      </body>
    </html>
  `
}

function updateBlocklyToolbox (proceduresUsage, functionsUsage) {
  var proceduresUsage = proceduresUsage.split(",");
  var functionsUsage = functionsUsage.split(",");
  addBlocklyBlocks(proceduresUsage, functionsUsage);
  addBlocklyStubs(proceduresUsage, functionsUsage);
  var toolbox = initBlocklyToolbox(proceduresUsage, functionsUsage);
  Blockly.mainWorkspace.updateToolbox(toolbox);
}

function initBlockly (flags) {
  $(document).ready(function(){
    initBlocklyBlocks();
    initBlocklyStubs();

    $("#blocklyApp").html($.parseHTML(getBlocklyHtml()));

    var node, app, blocklyDiv,
      workspace, highlevelCode,
      allProceduresUsage, allFunctionsUsage,
      start_xml;


    /* Initialising Blockly */
    var blocklyDiv = document.getElementById('blocklyDiv');
    var toolbox = initBlocklyToolbox([], []);
    Blockly.WorkspaceSvg.prototype.preloadAudio_ = function() {};
    workspace = Blockly.inject(blocklyDiv, {toolbox: toolbox});

    start_xml = Blockly.Xml.textToDom("<xml xmlns=\"http://www.w3.org/1999/xhtml\"><block type=\"start_block\" id=\"rhhd+t8qB84Y,Vy`{8Mp\" x=\"250\" y=\"250\"></block></xml>");
    Blockly.Xml.domToWorkspace(start_xml, workspace);
    initBlocklyVariables();

    highlevelCode = $("#highlevelCode");
    allProceduresUsage = $("#allProceduresUsage");
    allFunctionsUsage = $("#allFunctionsUsage");

    /* Modify the procedure and function blocks given the existing ones */
    $("#grammarBox").on("GRAMMAR_CHANGE", function() {
      updateBlocklyToolbox(allProceduresUsage.text(), allFunctionsUsage.text());
    })

    /* Whenever the workspace is updated, translate the blocks into code */
    function myUpdateFunction(event) {
      var code = Blockly.JavaScript.workspaceToCode(workspace)
      if (code.includes("START")) {
        code = code.split("START\n")[1].split("STOP\n")[0];
        highlevelCode.text(code);
        highlevelCode.trigger("LOADED_CODE")
      }

    }
    workspace.addChangeListener(myUpdateFunction);

    /* Resize the workspace whenever the VM appears or hides */
    $('#bottom').on("POSITION_CHANGE", function() {
      window.dispatchEvent(new Event('resize'));
    })

    $("#blocklyApp").removeClass("hidden");
    window.dispatchEvent(new Event('resize'));
    $("#blocklyApp").addClass("hidden");

  })
}
