'use strict'

function moveUp (block, list) {
  var variableList, previousBlock, currentBlock, options, assignment

  previousBlock = null;
  currentBlock  = block;
  variableList = list;

  if (currentBlock !== null) {
    /* Is this a variable assignment or variable usage of its value */
    assignment = (currentBlock.type === "assign_variable"
                || currentBlock.type === "assign_array_1"
                || currentBlock.type === "assign_array_2"
                || currentBlock.type === "assign_array_3");

    /* while the outer block is not attained yet */
    while (currentBlock !== null) {
        /* Add created variable */
        if (currentBlock.type === "create_variable") {
          variableList = variableList.concat(currentBlock.inputList[0].fieldRow[1].text_.split(" "));
        }
        else if (currentBlock.type === "func_def" || currentBlock.type === "proc_def") {
          /* currentBlock is a surrounding block and not a previous statement */
          /* the initial block is inside a procedure or function definition */
          if (currentBlock == previousBlock.getSurroundParent()) {
            /* if its an assignment, only local variables can be assigned => break; */
            if (assignment) {
              break;
            }
            /* else, add the arguments as allowed */
            variableList = variableList.concat(currentBlock.inputList[0].fieldRow[2].text_.split(" "));
          }

          /* in a return statement, also add the local variables */
          if (currentBlock.type === "func_def" && currentBlock.childBlocks_[1] === previousBlock) {
            variableList = variableList.concat(moveDown(currentBlock.childBlocks_[0]));
          }

        }
        previousBlock = currentBlock;
        currentBlock = currentBlock.parentBlock_;
    }
  }

  return variableList
}

function moveDown(block) {
  var variableList = [];
  if (block.type === "create_variable") {
    variableList = variableList.concat(block.inputList[0].fieldRow[1].text_.split(" "));
  }
  if (block.childBlocks_.length > 0) {
    for (var i = 0; i < block.childBlocks_.length; i++) {
      variableList = variableList.concat(moveDown(block.childBlocks_[i]));
    }
  }
  return variableList
}

function initBlocklyVariables () {
  /* Override the Blockly dropdown creation for variables to implement the
    variable language policy */
  Blockly.FieldVariable.dropdownCreate = function() {
    var variableList, options;

    variableList = [" "];
    variableList = moveUp(this.sourceBlock_, variableList);

    options = [];
    for (var i = 0; i < variableList.length; i++) {
      options[i] = [variableList[i], variableList[i]];
    }

    return options;
  }
}
