'use strict';

function initBlocklyStubs () {
  /* ------- Variables --------- */

  Blockly.JavaScript['create_variable'] = function(block) {
    var text_variable_name = block.getFieldValue('variable_name');
    var code = "variable " + text_variable_name + "\n";
    return code;
  };

  Blockly.JavaScript['assign_variable'] = function(block) {
    var variable_name = block.getFieldValue('NAME');
    var value_variable_name = Blockly.JavaScript.valueToCode(block, 'variable_value', Blockly.JavaScript.ORDER_NONE);
    var code = variable_name + " = " + value_variable_name + "\n";
    return code;
  };

  Blockly.JavaScript['arith_operations'] = function(block) {
    var value_op1 = Blockly.JavaScript.valueToCode(block, 'op1', Blockly.JavaScript.ORDER_NONE);
    var dropdown_op = block.getFieldValue('op');
    var value_op2 = Blockly.JavaScript.valueToCode(block, 'op2', Blockly.JavaScript.ORDER_NONE);
    var code = " ( " + value_op1 + " " + dropdown_op + " " + value_op2 + " ) ";
    return [code, Blockly.JavaScript.ORDER_NONE];
  };

  Blockly.JavaScript['variable'] = function(block) {
    var variable_variable_name = block.getFieldValue('variable_name')
    var code = variable_variable_name;
    return [code, Blockly.JavaScript.ORDER_NONE];
  };

  Blockly.JavaScript['number'] = function(block) {
    var number_name = block.getFieldValue('NAME');
    var code = number_name;
    return [code, Blockly.JavaScript.ORDER_NONE];
  };

  /* ------- Notes --------- */

  var notes = "C1 C1# D1 D1# E1 F1 F1# G1 G1# A1 A1# B1 C2 C2# D2 D2# E2 F2 F2# G2 G2# A2 A2# B2 C3 C3# D3 D3# E3 F3 F3# G3 G3# A3 A3# B3 C4 C4# D4 D4# E4 F4 F4# G4 G4# A4 A4# B4 C5 C5# D5 D5# E5 F5 F5# G5 G5# A5 A5# B5 C6".split(" ")
  for (var i = 0; i < notes.length; i++) {
    var note = notes[i];
    Blockly.JavaScript[note.toLowerCase()] = function(block) {
      var code = this.type.toUpperCase();
      return [code, Blockly.JavaScript.ORDER_NONE];
    };
  }

  /* ------- Colours --------- */

  var colours = "black blue green cyan red magenta yellow white".split(" ")
  for (var i = 0; i < colours.length; i++) {
    var colour = colours[i];
    Blockly.JavaScript[colour.toLowerCase()] = function(block) {
      var code = this.type;
      return [code, Blockly.JavaScript.ORDER_NONE];
    };
  }

  /* --- Conditional Branches and Loops --- */

  Blockly.JavaScript['if_then_else'] = function(block) {
    var value_if_cond = Blockly.JavaScript.valueToCode(block, 'if_cond', Blockly.JavaScript.ORDER_NONE);
    var statements_then_stat = Blockly.JavaScript.statementToCode(block, 'then_stat');
    var statements_else_stat = Blockly.JavaScript.statementToCode(block, 'else_stat');
    var code = "if " + value_if_cond + " then\n" + statements_then_stat + " else\n" + statements_else_stat + " endif\n";
    return code;
  };

  Blockly.JavaScript['if_then'] = function(block) {
    var value_if_cond = Blockly.JavaScript.valueToCode(block, 'if_cond', Blockly.JavaScript.ORDER_NONE);
    var statements_then_stat = Blockly.JavaScript.statementToCode(block, 'then_stat');
    var code = "if " + value_if_cond + " then\n" + statements_then_stat + " endif\n";
    return code;
  };

  Blockly.JavaScript['while_do'] = function(block) {
    var value_while_cond = Blockly.JavaScript.valueToCode(block, 'while_cond', Blockly.JavaScript.ORDER_NONE);
    var statements_do_stat = Blockly.JavaScript.statementToCode(block, 'do_stat');
    var code = "while " + value_while_cond + " do\n" + statements_do_stat + " endwhile\n";
    return code;
  };

  Blockly.JavaScript['bool_operations'] = function(block) {
    var value_op1 = Blockly.JavaScript.valueToCode(block, 'op1', Blockly.JavaScript.ORDER_NONE);
    var dropdown_op = block.getFieldValue('op');
    var value_op2 = Blockly.JavaScript.valueToCode(block, 'op2', Blockly.JavaScript.ORDER_NONE);
    var code = value_op1 + " " + dropdown_op + " " + value_op2;
    return [code, Blockly.JavaScript.ORDER_NONE];
  };

  /* --- Functions --- */
  Blockly.JavaScript['assign_array_1'] = function(block) {
    var value_val1 = block.getFieldValue('val1');
    var value_function_value = Blockly.JavaScript.valueToCode(block, 'function_value', Blockly.JavaScript.ORDER_NONE);
    var code = value_val1 + " = " + value_function_value + "\n" ;
    return code;
  };

  Blockly.JavaScript['assign_array_2'] = function(block) {
    var value_val1 = block.getFieldValue('val1');
    var value_val2 = block.getFieldValue('val2');
    var value_function_value = Blockly.JavaScript.valueToCode(block, 'function_value', Blockly.JavaScript.ORDER_NONE);
    var code = value_val1 + " " + value_val2 + " = " + value_function_value + "\n" ;
    return code;
  };

  Blockly.JavaScript['assign_array_3'] = function(block) {
    var value_val1 = block.getFieldValue('val1');
    var value_val2 = block.getFieldValue('val2');
    var value_val3 = block.getFieldValue('val3');
    var value_function_value = Blockly.JavaScript.valueToCode(block, 'function_value', Blockly.JavaScript.NAME_TYPE);
    var code = value_val1 + " " + value_val2 + " " + value_val3 + " = " + value_function_value + "\n" ;
    return code;
  };

  /* --- Procedure and Function Definition --- */

  Blockly.JavaScript['proc_def'] = function(block) {
    var text_proc_name = block.getFieldValue('proc_name');
    var text_arguments = block.getFieldValue('arguments');
    var statements_proc_stat = Blockly.JavaScript.statementToCode(block, 'proc_stat');
    var code = "define " + text_proc_name + " " + text_arguments + " as\n" + statements_proc_stat + "enddefine\n";
    return code;
  };

  Blockly.JavaScript['func_def'] = function(block) {
    var text_func_name = block.getFieldValue('func_name');
    var text_arguments = block.getFieldValue('arguments');
    var statements_func_stat = Blockly.JavaScript.statementToCode(block, 'func_stat');
    var value_return_values = Blockly.JavaScript.valueToCode(block, 'return_values', Blockly.JavaScript.ORDER_NONE);
    var code = "function " + text_func_name + " " + text_arguments + " as\n" + statements_func_stat + " return " + value_return_values + " endfunction\n";
    return code;
  };

  Blockly.JavaScript['array_1'] = function(block) {
    var value_val1 = Blockly.JavaScript.valueToCode(block, 'val1', Blockly.JavaScript.ORDER_NONE);
    var code = value_val1;
    return [code, Blockly.JavaScript.ORDER_NONE];
  };

  Blockly.JavaScript['array_2'] = function(block) {
    var value_val1 = Blockly.JavaScript.valueToCode(block, 'val1', Blockly.JavaScript.ORDER_NONE);
    var value_val2 = Blockly.JavaScript.valueToCode(block, 'val2', Blockly.JavaScript.ORDER_NONE);
    var code = value_val1 + " " + value_val2;
    return [code, Blockly.JavaScript.ORDER_NONE];
  };

  Blockly.JavaScript['array_3'] = function(block) {
    var value_val1 = Blockly.JavaScript.valueToCode(block, 'val1', Blockly.JavaScript.ORDER_NONE);
    var value_val2 = Blockly.JavaScript.valueToCode(block, 'val2', Blockly.JavaScript.ORDER_NONE);
    var value_val3 = Blockly.JavaScript.valueToCode(block, 'val3', Blockly.JavaScript.ORDER_NONE);
    var code = value_val1 + " " + value_val2 + " " + value_val3;
    return [code, Blockly.JavaScript.ORDER_NONE];
  };

  Blockly.JavaScript['start_block'] = function(block) {
    var statements_name = Blockly.JavaScript.statementToCode(block, 'NAME');
    var code = "START\n" + statements_name + "STOP\n";
    return code;
  };
}

function addBlocklyStubs (procedures, functions) {
  function add_procedure(procedure_usage) {
    var name_args = procedure_usage.split(" ");
    Blockly.JavaScript[name_args[0].toLowerCase()] = function(block) {
      var code = name_args[0];
      for (var i = 1; i < name_args.length; i++) {
        var arg = Blockly.JavaScript.valueToCode(block, "arg" + i, Blockly.JavaScript.ORDER_NONE);
        code += " " + arg + "\n";
      }
      return code;
    }
  }
  function add_function(function_usage) {
    var name_args = function_usage.split(" ");
    Blockly.JavaScript[name_args[0].toLowerCase()] = function(block) {
      var code = name_args[0];
      for (var i = 1; i < name_args.length; i++) {
        var arg = Blockly.JavaScript.valueToCode(block, "arg" + i, Blockly.JavaScript.ORDER_NONE);
        code += " " + arg + "\n";
      }
      return [code, Blockly.JavaScript.ORDER_NONE];
    }
  }

  for (var i = 0; i < procedures.length; i++) {
    add_procedure(procedures[i]);
  }
  for (var i = 0; i < functions.length; i++) {
    add_function(functions[i]);
  }
}
