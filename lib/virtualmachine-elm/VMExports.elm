port module VMExports exposing (..)

import Vm exposing (executeInstruction)
import VMState exposing (..)

import Html exposing (..)
import Array.Hamt as Array

type alias Flags =
  { seed : Int }

main = Html.programWithFlags
 { init = init
 , view = view
 , update = update
 , subscriptions = subscriptions
 }


 -- MODEL

type alias Model =
 { virtualmachine  : VMState }

init: Flags -> (Model, Cmd Msg)
init flags =
  (Model (nullVM flags.seed), Cmd.none)


-- UPDATE

type Msg
  = Load (List String)
  | Run String
  | Reset String
  | Wait Int
  | Sleep Int
  | Tone Int
  | Beep Int Int
  | Rgb Int Int Int
  | Colour Int
  | Flash Int Int
  | Led Int Int
  | Error String
  | Halt String
  | Eoi String

-- port for sending the results to JavaScript
port wait : Int -> Cmd msg
port sleep : Int -> Cmd msg
port tone : Int -> Cmd msg
port beep : (Int, Int) -> Cmd msg
port rgb : (Int, Int, Int) -> Cmd msg
port colour : Int -> Cmd msg
port flash : (Int, Int) -> Cmd msg
port led : (Int, Int) -> Cmd msg
port error : (String, List (List (String, Int)), List String) -> Cmd msg
port halt : String -> Cmd msg
port eoi : (Int, List (List (String, Int)), List String) -> Cmd msg

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Load program ->
      ({ virtualmachine = VMState.load model.virtualmachine (program |> Array.fromList) }, Cmd.none)
    Run str ->
      runStep model
    Reset str ->
      ({ virtualmachine = VMState.reset model.virtualmachine}, Cmd.none)
    Wait tm ->
      ( model, wait tm )
    Sleep tm ->
      ( model, sleep tm )
    Tone freq ->
      ( model, tone freq )
    Beep freq tm ->
      ( model, beep (freq,tm) )
    Rgb r g b ->
      ( model, rgb (r,g,b) )
    Colour col ->
      ( model, colour col )
    Flash col tm ->
      ( model, flash (col,tm) )
    Led pixel col ->
      ( model, led (pixel, col) )
    Error err ->
      ( model, error (err, VMState.printVar model.virtualmachine, model.virtualmachine.callStackNames ) )
    Halt str ->
      ( model, halt str )
    Eoi str ->
      ( model, eoi (model.virtualmachine.lineCounter, VMState.printVar model.virtualmachine, model.virtualmachine.callStackNames) )

runStep : Model -> (Model, Cmd Msg)
runStep model =
  let runvm = VMState.run model.virtualmachine in
  let (vm, command) = executeInstruction runvm in
  {- let dummy = Debug.log "VM State " (toString vm) in -}
  let (hasError, error) = VMState.error vm in
  if hasError then
    (update (Error error) { virtualmachine = vm } )
  else
    case command of
      NONE ->
        if vm.status == RUN then
          update (Run "") { virtualmachine = vm }
        else if vm.status == HALT then
          update (Halt "") { virtualmachine = vm }
        else if vm.status == EOI then
          update (Eoi "") { virtualmachine = vm }
        else
          ( { virtualmachine = vm }, Cmd.none )
      WAIT tm ->
        update (Wait tm) { virtualmachine = vm }
      SLEEP tm ->
        update (Sleep tm) { virtualmachine = vm }
      TONE freq ->
        update (Tone freq){ virtualmachine = vm }
      BEEP freq tm ->
        update (Beep freq tm) { virtualmachine = vm }
      RGB r g b ->
        update (Rgb r g b) { virtualmachine = vm }
      COLOUR col ->
        update (Colour col) { virtualmachine = vm }
      FLASH col tm ->
        update (Flash col tm) { virtualmachine = vm }
      LED pixel col ->
        update (Led pixel col) { virtualmachine = vm }


-- SUBSCRIPTIONS

-- port for listening for calls from JavaScript
port load : (List String -> msg) -> Sub msg
port run : (String -> msg) -> Sub msg
port reset : (String -> msg) -> Sub msg

subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.batch
  [ load Load
  , run Run
  , reset Reset
  ]

-- VIEW

view : Model -> Html Msg
view model =
  div [] []
