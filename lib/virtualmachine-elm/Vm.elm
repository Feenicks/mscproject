module Vm exposing (executeInstruction)

import VMState exposing (..)
import Bitwise

convertToSigned8Bit : Int -> Int
convertToSigned8Bit low =
  if low > 127 then
    low - 256
  else
    low

convertToSigned16Bit : (Int, Int) -> Int
convertToSigned16Bit (low, high) =
  let val = 256 * high + low in
  if val > 32767 then
    val - 65536
  else
    val

getSingleByteArgument : VMState -> (Int, VMState)
getSingleByteArgument vm =
  let (low, newvm) = VMState.readIntAndIncrement vm in
  (convertToSigned8Bit low, newvm)

getDoubleByteArgument : VMState -> (Int, VMState)
getDoubleByteArgument vm =
  let (low, newvm) = VMState.readIntAndIncrement vm in
  let (high, newvm2) = VMState.readIntAndIncrement newvm in
  (convertToSigned16Bit (low, high), newvm2)

saturate : Int -> Int
saturate nb =
  min (2147483647) (max -2147483648 nb)

executeInstruction : VMState -> (VMState, Command)
executeInstruction vm =
  let (instr, newvm) = VMState.readAndIncrement vm in
  {- let dummy = Debug.log "Instruction" instr in -}
  case instr |> String.toInt of
    Ok value ->
      case value of
        0 -> {- Add -}
          let (b, vm2) = VMState.pop newvm in
          let (a, vm3) = VMState.pop vm2 in
          (VMState.push vm3 (saturate (a + b)), NONE)
        1 -> {- Sub -}
          let (b, vm2) = VMState.pop newvm in
          let (a, vm3) = VMState.pop vm2 in
          (VMState.push vm3 (saturate (a - b)), NONE)
        2 -> {- Mul -}
          let (b, vm2) = VMState.pop newvm in
          let (a, vm3) = VMState.pop vm2 in
          (VMState.push vm3 (saturate (a * b)), NONE)
        3 -> {- Div -}
          let (b, vm2) = VMState.pop newvm in
          let (a, vm3) = VMState.pop vm2 in
          if b == 0 then
            ({ vm3 | status = INVALID_OPERAND }, NONE)
          else
            (VMState.push vm3 (saturate (a // b)), NONE)
        4 -> {- Mod -}
          let (b, vm2) = VMState.pop newvm in
          let (a, vm3) = VMState.pop vm2 in
          if b == 0 then
            ({ vm | status = INVALID_OPERAND }, NONE)
          else
            (VMState.push vm3 (saturate (a % b)), NONE)
        5 -> {- Inc -}
          let (a, vm2) = VMState.pop newvm in
          (VMState.push vm2 (saturate (a + 1)), NONE)
        6 -> {- Inc -}
          let (a, vm2) = VMState.pop newvm in
          (VMState.push vm2 (saturate (a - 1)), NONE)
        7 -> {- Max -}
          let (b, vm2) = VMState.pop newvm in
          let (a, vm3) = VMState.pop vm2 in
          (VMState.push vm3 (max a b), NONE)
        8 -> {- Min -}
          let (b, vm2) = VMState.pop newvm in
          let (a, vm3) = VMState.pop vm2 in
          (VMState.push vm3 (min a b), NONE)
        9 -> {- LT -}
          let (b, vm2) = VMState.pop newvm in
          let (a, vm3) = VMState.pop vm2 in
          if a < b then
            (VMState.push vm3 1, NONE)
          else
            (VMState.push vm3 0, NONE)
        10 -> {- LE -}
          let (b, vm2) = VMState.pop newvm in
          let (a, vm3) = VMState.pop vm2 in
          if a <= b then
            (VMState.push vm3 1, NONE)
          else
            (VMState.push vm3 0, NONE)
        11 -> {- EQ -}
          let (b, vm2) = VMState.pop newvm in
          let (a, vm3) = VMState.pop vm2 in
          if a == b then
            (VMState.push vm3 1, NONE)
          else
            (VMState.push vm3 0, NONE)
        12 -> {- GE -}
          let (b, vm2) = VMState.pop newvm in
          let (a, vm3) = VMState.pop vm2 in
          if a >= b then
            (VMState.push vm3 1, NONE)
          else
            (VMState.push vm3 0, NONE)
        13 -> {- GT -}
          let (b, vm2) = VMState.pop newvm in
          let (a, vm3) = VMState.pop vm2 in
          if a > b then
            (VMState.push vm3 1, NONE)
          else
            (VMState.push vm3 0, NONE)
        14 -> {- Drop -}
          let (_, vm2) = VMState.pop newvm in
          (vm2, NONE)
        15 -> {- Dup -}
          (VMState.duplicate newvm 1, NONE)
        16 -> {- Ndup -}
          let (a, vm2) = VMState.pop newvm in
          (VMState.duplicate vm2 a, NONE)
        17 ->  {- swap -}
          (VMState.rotate newvm 2, NONE)
        18 -> {- Rot -}
          (VMState.rotate newvm 3, NONE)
        19 -> {- Nrot -}
          let (a, vm2) = VMState.pop newvm in
          (VMState.rotate vm2 a, NONE)
        20 -> {- Tuck -}
          (VMState.rotate newvm -3, NONE)
        21 -> {- Ntuck -}
          let (a, vm2) = VMState.pop newvm in
          (VMState.rotate vm2 (0 - a), NONE)
        22 -> {- size -}
          (VMState.push newvm (VMState.size newvm), NONE)
        23 -> {- Nrand -}
          let (a, vm2) = VMState.pop newvm in
          let (b, vm3) = VMState.random vm2 a in
          if (a <= 0) then
            ({ vm3 | status = INVALID_OPERAND }, NONE)
          else
            (VMState.push vm3 b, NONE)
        24 -> {- Push8 -}
          let (a, vm2) = getSingleByteArgument newvm in
          (VMState.push vm2 a, NONE)
        25 -> {- Push16 -}
          let (a, vm2) = getDoubleByteArgument newvm in
          (VMState.push vm2 a, NONE)
        26 -> {- Fetch -}
          let (addr, vm2) = VMState.pop newvm in
          let low = VMState.readAddress vm2 addr in
          let high = VMState.readAddress vm2 (addr + 1) in
          (VMState.push vm2 (convertToSigned16Bit (low, high)), NONE)
        27 -> {- Call -}
          let (addr, vm2) = VMState.pop newvm in
          let vm3 = VMState.call vm2 in
          (VMState.jump vm3 addr, NONE)
        28 -> {- Ret -}
          let (addr, vm2) = VMState.ret newvm in
          (VMState.jump vm2 addr, NONE)
        29 -> {- Jmp -}
          let (addr, vm2) = VMState.pop newvm in
          (VMState.jump vm2 addr, NONE)
        30 -> {- Cjmp -}
          let (addr, vm2) = VMState.pop newvm in
          let (cond, vm3) = VMState.pop vm2 in
          if cond /= 0 then
            (VMState.jump vm3 addr, NONE)
          else
            (vm3, NONE)
        31 -> {- Wait -}
          let (tm, vm2) = VMState.pop newvm in
          (vm2, WAIT tm)
        32 -> {- Halt -}
          (VMState.halt newvm, NONE)

        128 -> {- Sleep -}
          let (tm, vm2) = VMState.pop newvm in
          let (_, vm3) = VMState.pop vm2 in
          let (_,vm4) = VMState.readIntAndIncrement vm3 in
          (VMState.reset vm4, SLEEP tm)
        129 -> {- Tone -}
          let (freq, vm2) = VMState.pop newvm in
          let (_,vm3) = VMState.readIntAndIncrement vm2 in
          if (freq < 0 || freq > 32767) then
            ({ vm3 | status = INVALID_OPERAND }, NONE)
          else
            (vm3, TONE freq)
        130 -> {- Beep -}
          let (tm, vm2) = VMState.pop newvm in
          let (freq, vm3) = VMState.pop vm2 in
          let (_,vm4) = VMState.readIntAndIncrement vm3 in
          if (freq < 0 || freq > 32767) then
            ({ vm4 | status = INVALID_OPERAND }, NONE)
          else
            (vm4, BEEP freq tm)
        131 -> {- RGB -}
          let (blue, vm2) = VMState.pop newvm in
          let (green, vm3) = VMState.pop vm2 in
          let (red, vm4) = VMState.pop vm3 in
          let (_,vm5) = VMState.readIntAndIncrement vm4 in
          if (red < 0 || green < 0 || blue < 0 || red > 255 || green > 255 || blue > 255) then
            ({ vm5 | status = INVALID_OPERAND }, NONE)
          else
            (vm5, RGB red green blue)
        132 -> {- COLOUR -}
          let (colour, vm2) = VMState.pop newvm in
          let (_,vm3) = VMState.readIntAndIncrement vm2 in
          if (colour < 0 || colour > 7) then
            ({ vm3 | status = INVALID_OPERAND }, NONE)
          else
            (vm3, COLOUR colour)
        133 -> {- FLASH -}
          let (tm, vm2) = VMState.pop newvm in
          let (colour, vm3) = VMState.pop vm2 in
          let (_,vm4) = VMState.readIntAndIncrement vm3 in
          if (colour < 0 || colour > 7) then
            ({ vm4 | status = INVALID_OPERAND }, NONE)
          else
            (vm4, FLASH colour tm)
        134 -> {- TEMP -}
          let (_,vm2) = VMState.readIntAndIncrement newvm in
          (VMState.push vm2 20, NONE)
        135 -> {- ACCEL -}
          let (accelx, vm2) = VMState.random newvm 200 in
          let (accely, vm3) = VMState.random vm2 200 in
          let (accelz, vm4) = VMState.random vm3 200 in

          let vm5 = VMState.push vm4 (accelx - 100) in
          let vm6 = VMState.push vm5 (accely - 100) in
          let vm7 = VMState.push vm6 (accelz - 100) in

          let (_,vm8) = VMState.readIntAndIncrement vm7 in
          (vm8, NONE)
        136 -> {- LED -}
          let (pixel, vm2) = VMState.pop newvm in
          let (colour, vm3) = VMState.pop vm2 in
          let (_, vm4) = VMState.readIntAndIncrement vm3 in
          if (colour < 0 || colour > 7) then
            ({ vm4 | status = INVALID_OPERAND}, NONE)
          else if (pixel < 1 || pixel > 9) then
            ({ vm4 | status = INVALID_OPERAND}, NONE)
          else
            (vm4, LED pixel colour)

        other ->
          let (code_nb,vm2) = VMState.readIntAndIncrement newvm in
          let popped_values = Bitwise.and code_nb 15 in     {- 0x0f -}
          let pushed_values = (Bitwise.and code_nb 240) |> Bitwise.shiftRightBy 4 in  {- 0xf0 -}

          let vm3 = multiplePop vm2 popped_values in
          let vm4 = multiplePush vm3 pushed_values in

          (vm4, NONE)

    Err _ ->
      {- VM Markers -}
      case instr of
        "#EOI" -> {- EOI -}
          let (line, vm2) = VMState.readIntAndIncrement newvm in
          (VMState.eoi vm2 line, NONE)
        "#VAR" -> {- Variable creation -}
          let (varName, vm2) = VMState.readAndIncrement newvm in
          (VMState.addVar vm2 varName, NONE)
        "#FUNC" -> {- Function call -}
          let (funcName, vm2) = VMState.readAndIncrement newvm in
          let (nbArgs, vm3) = VMState.readIntAndIncrement vm2 in
          (VMState.addFuncCall vm3 funcName nbArgs, NONE)


        other ->
          ({ newvm | status = INVALID_INSTRUCTION }, NONE)
