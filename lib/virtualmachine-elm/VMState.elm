module VMState exposing (..)

import Array.Hamt as Array exposing (Array)
import Random

type alias VMState =
  { program        : Array String
  , status         : Status
  , programCounter : Int
  , callStack      : Array Int
  , operandStack   : Array Int
  , operandSize    : Int
  , seed           : Random.Seed
  , lineCounter    : Int
  , variables      : List (List String)
  , basePointers   : List Int
  , callStackNames : List String
}

nullVM seed =
  VMState Array.empty HALT 0 Array.empty Array.empty 0 (Random.initialSeed seed) 1 [[]] [0] ["main"]
newVM seed =
  VMState Array.empty HALT 0 Array.empty Array.empty 0 seed 1 [[]] [0] ["main"]

type Status =
    RUN
  | HALT
  | EOI
  | INVALID_INSTRUCTION
  | INVALID_OPERAND
  | CALL_STACK_OVERFLOW
  | OPERAND_STACK_OVERFLOW

{-| Verify that none of the stacks overflowed -}
checkState : VMState -> VMState
checkState vm =
  if (vm.status == RUN || vm.status == HALT || vm.status == EOI) then
    if Array.length vm.callStack > 100 then
      { vm | status = CALL_STACK_OVERFLOW }
    else if vm.operandSize > 100 then
      { vm | status = OPERAND_STACK_OVERFLOW }
    else
      vm
  else
    vm

{-| Duplicate nth element on the operand stack -}
duplicate : VMState -> Int -> VMState
duplicate vm n =
  if (vm.status == RUN || vm.status == HALT || vm.status == EOI) then
    let stack = vm.operandStack in
    let value = Maybe.withDefault -1 (Array.get (vm.operandSize - n) stack) in
    let operandStack = Array.push value stack in
    { vm | operandStack = operandStack, operandSize = vm.operandSize + 1 }
    |> checkState
  else
    vm

{-| Rotate nth element on the operand stack -}
rotate : VMState -> Int -> VMState
rotate vm n =
  if (vm.status == RUN || vm.status == HALT || vm.status == EOI) then
    let stack = vm.operandStack in
    let operandStack =
      if n > 1 then
        let value = Maybe.withDefault -1 (Array.get (vm.operandSize - n) stack) in
        Array.append (Array.slice 0 (vm.operandSize - n) stack) (Array.slice (vm.operandSize - n + 1) (vm.operandSize) stack)
        |> Array.push value
      else if n < 1 then
        let value = Maybe.withDefault -1 (Array.get (vm.operandSize - 1) stack) in
        Array.append (Array.slice 0 (vm.operandSize + n) stack |> Array.push value) (Array.slice (vm.operandSize + n) (vm.operandSize - 1) stack)
      else
        stack
    in
    { vm | operandStack = operandStack }
  else
    vm

{-| Pop last element on the operand stack -}
pop : VMState -> (Int, VMState)
pop vm =
  if (vm.status == RUN || vm.status == HALT || vm.status == EOI) then
    let stack = vm.operandStack in
    if Array.length stack == 1 then
      let value = Maybe.withDefault -1 (Array.get 0 stack) in
      (value, { vm | operandStack = Array.empty, operandSize = vm.operandSize - 1 })
    else
      let value = Maybe.withDefault -1 (Array.get (vm.operandSize - 1) stack) in
      let operandStack =
        Array.slice 0 (vm.operandSize - 1) stack
      in
      (value, { vm | operandStack = operandStack, operandSize = vm.operandSize - 1 })
  else
    (-1, vm)

{-| Pop a number of elements on the operand stack -}
multiplePop : VMState -> Int -> VMState
multiplePop vm number =
  if number <= 0 then
    vm
  else
    let (_, newvm) = pop vm in
    multiplePop newvm (number - 1)

{-| Push value to the operand stack -}
push : VMState -> Int -> VMState
push vm value =
  if (vm.status == RUN || vm.status == HALT || vm.status == EOI) then
    { vm | operandStack = (Array.push value vm.operandStack), operandSize = vm.operandSize + 1 }
    |> checkState
  else
    vm

{-| Push a number of zeros on the operand stack -}
multiplePush : VMState -> Int -> VMState
multiplePush vm number =
  if number <= 0 then
    vm
  else
    let newvm = push vm 0 in
    multiplePush newvm (number - 1)

size : VMState -> Int
size vm =
  vm.operandSize

random : VMState -> Int -> (Int, VMState)
random vm n =
  let gen = Random.int 0 (n-1) in
  let (nb, seed) = Random.step gen vm.seed in
  (nb, { vm | seed = seed })

reset : VMState -> VMState
reset vm =
  let vm2 = newVM vm.seed in
  { vm2 | program = vm.program}

load : VMState -> Array String -> VMState
load vm program =
  let vm2 = newVM vm.seed in
  { vm2 | program = program }

{-| Read the value at adress addr in the program -}
readAddress : VMState -> Int -> Int
readAddress vm addr =
  Maybe.withDefault "" (Array.get addr (vm.program)) |> String.toInt |> Result.toMaybe |> Maybe.withDefault -1

{-| Read next value in program and increment counter -}
readAndIncrement : VMState -> (String, VMState)
readAndIncrement vm =
  let value = Maybe.withDefault "" (Array.get vm.programCounter vm.program) in
  (value, { vm | programCounter = vm.programCounter + 1 })

{-| Read next value as Int in the program and increment counter -}
readIntAndIncrement : VMState -> (Int, VMState)
readIntAndIncrement vm =
  let value = Maybe.withDefault "" (Array.get vm.programCounter vm.program) in
  (value |> String.toInt |> Result.toMaybe |> Maybe.withDefault -1, { vm | programCounter = vm.programCounter + 1 })

{-| Change the program counter to new address -}
jump : VMState -> Int -> VMState
jump vm addr =
  { vm | programCounter = addr }

{-| Procedure/function call -}
{- new variable list - new eip on call Stack, new ebp on base pointers stack -}
call : VMState -> VMState
call vm =
  { vm | callStack = Array.push vm.programCounter vm.callStack, variables = [] :: vm.variables, basePointers = vm.operandSize :: vm.basePointers }

{-| Return from call -}
{- get return adress, remove call from call stack, remove the call's local
  variable, remove ebp from base pointers list, remove call name from the list -}
ret : VMState -> (Int, VMState)
ret vm =
  (Maybe.withDefault -1 (Array.get (Array.length vm.callStack - 1) vm.callStack),
  { vm | callStack = Array.slice 0 (Array.length vm.callStack - 1) vm.callStack
       , variables = Maybe.withDefault [[]] (List.tail vm.variables)
       , basePointers = Maybe.withDefault [] (List.tail vm.basePointers)
       , callStackNames = Maybe.withDefault [] (List.tail vm.callStackNames) })

{-| Modify VM State -}
run : VMState -> VMState
run vm =
  if vm.status == HALT || vm.status == EOI then
    { vm | status = RUN }
  else
    vm

halt : VMState -> VMState
halt vm =
  if vm.status == RUN || vm.status == EOI then
    { vm | status = HALT }
  else
    vm

eoi : VMState -> Int -> VMState
eoi vm line =
  if vm.status == RUN then
    { vm | status = EOI, lineCounter = line }
  else
    vm

{-| Add a variable to the current global/local variable list -}
addVar : VMState -> String -> VMState
addVar vm varName =
  if vm.status == RUN then
    let variables =
      case vm.variables of
        [] -> []
        hd :: tl ->
          ( varName :: hd ) :: tl
    in
    { vm | variables = variables }
  else
    vm

{-| Add function name and argument values to call names list -}
addFuncCall : VMState -> String -> Int -> VMState
addFuncCall vm funcName nbArgs =
  if vm.status == RUN then
    let callStackName =
      funcName ++ " " ++ (String.join " " (List.map toString ((Array.slice (Array.length vm.operandStack - nbArgs) (Array.length vm.operandStack) vm.operandStack) |> Array.toList )))
    in
    { vm | callStackNames = (callStackName :: vm.callStackNames) }
  else
    vm

{-| Remove call name from list -}
removeFuncCall : VMState -> VMState
removeFuncCall vm =
  if vm.status == RUN then
    { vm | callStackNames = Maybe.withDefault [] (List.tail vm.callStackNames) }
  else
    vm

{-| Return the element at the index in the list.
  WARNING : RUNTIME error if it doesn't exist -}
getIndex : Int -> List a -> a
getIndex index list =
  if index == 0 then
    case List.head list of
      Just hd ->
        hd
      Nothing ->
        Debug.crash "Tried to get a non-existing index from list"
  else
    case List.tail list of
      Just tl ->
        getIndex (index - 1) tl
      Nothing ->
        Debug.crash "Tried to get a non-existing index from list"

{-| Return the variables stack -}
printVar : VMState -> List (List (String, Int))
printVar vm =
  let printFrame i varNames =
    let base = getIndex i vm.basePointers in
    let numVar = List.length varNames in
    let print idx varName = (varName, Array.get (base + numVar - idx - 1) vm.operandStack |> Maybe.withDefault -1) in
    List.indexedMap print varNames
  in
  List.indexedMap printFrame vm.variables

errormessage : String -> String
errormessage error =
  "<span style=\"font-weight:bold;\">Error: " ++ error ++ "</span>"


error : VMState -> (Bool, String)
error vm =
  case vm.status of
    INVALID_INSTRUCTION ->
      (True, errormessage "Invalid Instruction." )
    INVALID_OPERAND ->
      (True, errormessage "Invalid Operand in last function call." )
    CALL_STACK_OVERFLOW ->
      (True, errormessage "Call Stack Overflow: Too many recursive function calls." )
    OPERAND_STACK_OVERFLOW  ->
      (True, errormessage "Operand Stack Overflow: Too many values stored on the stack." )
    other ->
      (False, "")

type Command =
    NONE
  | WAIT Int
  | SLEEP Int
  | TONE Int
  | BEEP Int Int
  | RGB Int Int Int
  | COLOUR Int
  | FLASH Int Int
  | LED Int Int
