'use strict'

function getVMHtml() {
  return `
  <html>
    <body>
        <link href="./lib/virtualmachine-elm/vm.css" rel="stylesheet">

        <div class="container-fluid" id="topVMBox">

          <div class="col-lg-12 col-md-12 col-sd-12 col-xs-12 column">

            <div class="col-lg-6 col-md-6 col-sd-12 col-xs-12 column">
              <div id="virtualMachine">

                <div style="display:none;" id="elmVirtualMachine"></div>

                <div class="col-lg-4 col-md-4 col-sd-5 col-xs-5 column">
                  <nav id="VMButtons" class="navbar navbar-inverse ">
                    <ul class="nav sidebar-nav">
                        <li><a href="#" id="loadButton">Load</a></li>
                        <li class="divider"> </li>
                        <li><a href="#" id="runButton">Run</a></li>
                        <li class="divider"> </li>
                        <li><a href="#" id="stepButton">Step</a></li>
                        <li class="divider"> </li>
                        <li><a href="#" id="haltButton">Halt</a></li>
                        <li class="divider"> </li>
                        <li><a href="#" id="resetButton">Reset</a></li>
                        <li class="divider"> </li>
                    </ul>
                  </nav>
                </div>

              <div class="col-lg-8 col-md-8 col-sd-7 col-xs-7 column">

                <div id="microbit">
                  <div id="cover">
                    <div id="leftTriangle"></div>
                    <div id="lights">
                      <div id="led1"> </div>
                      <div id="led2"> </div>
                      <div id="led3"> </div>
                      <div id="led4"> </div>
                      <div id="led5"> </div>
                      <div id="led6"> </div>
                      <div id="led7"> </div>
                      <div id="led8"> </div>
                      <div id="led9"> </div>
                    </div>
                    <div id="rightTriangle"></div>
                    <div id="bottomScrewBar">
                    </div>
                  </div>
                  <div id="microbitbottom"></div>
                </div>
              </div>


              <div style="display:none;">
                <div class=infoHead>
                  Tone
                </div>
                <div id="toneFrequency" class="info">
                </div>
              </div>

            </div>
          </div>


            <div class="col-lg-6 col-md-6 col-sd-12 col-xs-12 column">
              <div id="runtimeInfo">
                  <div id="lineCounter"></div>
                  <div id="vmErrorBox"></div>
                  <div id="variables"></div>

              </div>
            </div>

            </div>
            </div>

          </div>
        </div>

    </body>
  <html>
  `
}

function initVM () {
  $(document).ready(function(){
    $("#vmBox").html($.parseHTML(getVMHtml()));

    var node, app,
      halt = true, highlevelCode,
      vmHexCode, loadButton, runButton, stepButton, haltButton, resetButton,
      loopLaunch, forceLoopLaunch, printDebugTable, waittimer, led, sounder,
      showLine = false, lineCounter, variableDiv, vmErrorBox,
      loaded, running, stopped, halted, reset;

    /* Create a sounder maker */
    function Sounder(element) {

        var vca, vco, timer, audioContext, api = this;

        audioContext = new AudioContext();

        vco = audioContext.createOscillator();
        vco.type = 'square';
        vco.frequency.value = 440;

        vca = audioContext.createGain();
        vca.gain.value = 0;

        vco.connect(vca);
        vca.connect(audioContext.destination);

        vco.start();

        api.tone = function (frequency) {
            clearTimeout(timer);
            vco.frequency.setValueAtTime(frequency, audioContext.currentTime);
            vca.gain.setValueAtTime(1, audioContext.currentTime);
            element.text(frequency + ' Hz');
        };

        api.off = function () {
            clearTimeout(timer);
            vca.gain.setValueAtTime(0, audioContext.currentTime);
            element.text('');
        };

        api.beep = function (frequency, duration) {
            vco.frequency.setValueAtTime(frequency, audioContext.currentTime);
            vca.gain.setValueAtTime(1, audioContext.currentTime);
            timer = setTimeout(api.off, duration);
            element.text(frequency + ' Hz');
        };

        return api;

    };

    /* Create a 3x3 LED block */
    function LED() {

        var timer, colours, i, led = [], api = this;

        colours = ['black', 'blue', 'green', 'cyan', 'red', 'magenta', 'yellow', 'white'];
        for (i = 0; i < 9; i ++) {
          led.push($('#led'+(i+1)))
        }

        function rgbToHex(r, g, b) {
            return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
        };

        api.rgb = function (red, green, blue) {
            clearTimeout(timer);
            for (i = 1; i < 10; i ++) {
              led[i-1].css('background-color' , rgbToHex(red, green, blue));
            }
        };

        api.colour = function (colour) {
            clearTimeout(timer);
            for (i = 1; i < 10; i ++) {
              led[i-1].css('background-color' , colours[colour]);
            }
        };

        api.off = function () {
            clearTimeout(timer);
            for (i = 1; i < 10; i ++) {
              led[i-1].css('background-color' , 'black');
            }
        };

        api.flash = function (colour, duration) {
          for (var i = 1; i < 10; i ++) {
            led[i-1].css('background-color', colours[colour]);
          }
          timer = setTimeout(api.off, duration);
        };

        api.led = function (pixel, colour) {
          led[pixel-1].css('background-color', colours[colour]);
        }

        return api;

    };

    /* Importing the elm compiler app */
      node = document.getElementById('elmVirtualMachine');
      app = Elm.VMExports.embed(node, { seed : (new Date).getTime() });

      vmHexCode    = $('#vmHexCode');
      loadButton = $("#loadButton");
      runButton = $("#runButton");
      stepButton = $("#stepButton");
      haltButton = $("#haltButton");
      resetButton = $("#resetButton");

      led = new LED();
      sounder = new Sounder($('#toneFrequency'));

      highlevelCode = $("#highlevelCode");
      lineCounter = $("#lineCounter");
      variableDiv = $("#variables");
      vmErrorBox = $("#vmErrorBox");

      /* Continue execution if VM not halted */
      loopLaunch = function () {
        if (!halt) {
          app.ports.run.send("");
        }
        else {
          stopped();
        }
      };

      /* Continue execution in all cases
         Used if JavaScript calls used (e.g. flash/led) and not EOI */
      forceLoopLaunch = function () {
        app.ports.run.send("");
      }

      loaded = function () {
        loadButton.removeClass("enabled");
        runButton.removeClass("disabled");
        stepButton.removeClass("disabled");
        haltButton.addClass("disabled");
        resetButton.addClass("disabled");
      }

      running = function () {
        loadButton.addClass("disabled");
        runButton.addClass("disabled");
        stepButton.addClass("disabled");
        haltButton.removeClass("disabled");
        resetButton.addClass("disabled");
      }

      stopped =  function() {
        loadButton.removeClass("disabled");
        runButton.removeClass("disabled");
        stepButton.removeClass("disabled");
        haltButton.addClass("disabled");
        resetButton.removeClass("disabled");
      }

      halted = function() {
        loadButton.removeClass("disabled");
        haltButton.addClass("disabled");
        resetButton.removeClass("disabled");
      }

      reset = function () {
        runButton.removeClass("disabled");
        stepButton.removeClass("disabled");
        haltButton.addClass("disabled");
        resetButton.addClass("disabled");
      }

      loadButton.on('click', function() {
        if (!loadButton.hasClass("disabled") && loadButton.hasClass("enabled")) {
          halt = true;
          sounder.off();
          led.off();
          lineCounter.empty();
          variableDiv.empty();
          vmErrorBox.empty();
          if (vmHexCode.text() == "") {
            alert("Nothing to load");
          }
          else {
            var code = vmHexCode.text().split(' ');
            loaded();
            app.ports.load.send(code);
          }
        }
        $(this).blur();
      });

      runButton.on('click', function() {
        if (!runButton.hasClass("disabled")) {
          halt = false;
          showLine = false;
          running();
          app.ports.run.send("");
        }
        $(this).blur();
      });

      stepButton.on('click', function() {
        if (!stepButton.hasClass("disabled")) {
          showLine = true;
          running();
          app.ports.run.send("");
        }
        $(this).blur();
      });

      haltButton.on('click', function(){
        if (!haltButton.hasClass("disabled")) {
          halt = true;
          haltButton.addClass("disabled");
        };
        $(this).blur();
      });

      resetButton.on('click', function() {
        if (!resetButton.hasClass("disabled")) {
          sounder.off();
          led.off();
          lineCounter.empty();
          variableDiv.empty();
          vmErrorBox.empty();
          reset();
          app.ports.reset.send("");
        }
        $(this).blur();
      });

      /* Print table of procedure/function calls and variables */
      printDebugTable = function(variablesList, calls) {
        if (calls.length > variablesList.length) {
          variablesList.unshift([]);
        }
        var table = "<table>";
        for (var i = 0; i < calls.length; i++) {
          var variables = variablesList[i];
          table += "<tr><th colspan=\"2\">" + calls[i] +"</th>";
          for (var j = 0; j < variables.length; j++) {
            var variable = variables[j];
            table += "<tr>";
            table += "<td>"+variable[0]+"</td>";
            table += "<td>"+variable[1]+"</td>";
            table += "</tr>";
          }
        }

        table += "</table>";
        variableDiv.empty();
        variableDiv.append($.parseHTML(table));
      }

      app.ports.wait.subscribe(function(received){
        waittimer = setTimeout(forceLoopLaunch, received);
      });
      app.ports.sleep.subscribe(function(received){
        waittimer = setTimeout(forceLoopLaunch, received);
      });
      app.ports.tone.subscribe(function(received){
        if (received == 0) {
          sounder.off();
        }
        else {
          sounder.tone(received);
        }
        forceLoopLaunch();
      });
      app.ports.beep.subscribe(function(received){
        sounder.beep(received[0], received[1]);
        waittimer = setTimeout(forceLoopLaunch, received[1]);
      });
      app.ports.rgb.subscribe(function(received){
        led.rgb(received[0], received[1], received[2]);
        forceLoopLaunch();
      });
      app.ports.colour.subscribe(function(received){
        led.colour(received);
        forceLoopLaunch();
      });
      app.ports.flash.subscribe(function(received){
        led.flash(received[0], received[1]);
        waittimer = setTimeout(forceLoopLaunch, received[1]);
      });
      app.ports.led.subscribe(function(received){
        led.led(received[0], received[1]);
        forceLoopLaunch();
      });
      app.ports.error.subscribe(function(received){
        halted();
        vmErrorBox.empty();
        vmErrorBox.append($.parseHTML(received[0]));
        printDebugTable(received[1], received[2]);
      });
      app.ports.halt.subscribe(function(received){
        halted();
        halt = true;
        lineCounter.text("End of program");
        variableDiv.empty();
        sounder.off();
        led.off();
      });
      app.ports.eoi.subscribe(function(received){
        if (showLine) {
          lineCounter.text("Line " + received[0] + ": " + highlevelCode.text().split("\n")[received[0] - 1]);
          printDebugTable(received[1], received[2]);
        }
        loopLaunch();
      })

      loadButton.removeClass("disabled");
      runButton.addClass("disabled");
      stepButton.addClass("disabled");
      haltButton.addClass("disabled");
      resetButton.addClass("disabled");

  });

}
