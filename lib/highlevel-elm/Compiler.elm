module Compiler exposing (compiler)

import Types exposing (TokenType(..), Token, BiNodeType(..), TriNodeType(..), ParseTree(..))
import Dict exposing (Dict)

type alias State =
  { ifCount           : Int                            {- number of if labels so far -}
  , whileCount        : Int                            {- number of while labels so far -}
  , procfuncToDefine  : List String                    {- list of procedures and functions to define and append at the end -}
  , frameSize         : Int                            {- used in function calls to keep track of the arguments position -}
  , procedures        : Dict String (String, Int)      {- dict of procedure names, code and number of arguments -}
  , functions         : Dict String (String, Int, Int) {- dict of function names, code, number of arguments and return values -}
  , definedProcedures : Dict String (Int, Int)         {- dict of procedure names, number of arguments and local variables -}
  , definedFunctions  : Dict String (Int, Int, Int)    {- dict of function names, number of arguments, local variables and return values -}
  , isVm              : Bool                           {- whether the code is compiled for the vm or the device -}
  }

initState: Dict String (String, Int) -> Dict String (String, Int, Int) -> Dict String (Int, Int) -> Dict String (Int, Int, Int) -> Bool -> State
initState procedures functions definedProcedures definedFunctions isVm = (State 0 0 [] 0 procedures functions definedProcedures definedFunctions isVm)

createVariable : Int -> String
createVariable index =
  {- VALUE SIZE INDEX SUB NTUCK -}
  "0 SIZE " ++ (toString index) ++ " SUB NTUCK"

createLocalVariable : Int -> Int -> String
createLocalVariable index frameSize =
  {- 0 (FRAMESIZE + 1) INDEX SUB NTUCK -}
  "0 " ++ (toString (frameSize + 1)) ++ " " ++ (toString index) ++ " SUB NTUCK"

getVariable : Int -> String
getVariable index =
  {- SIZE INDEX SUB NDUP -}
  "SIZE " ++ (toString index) ++ " SUB NDUP"

getArgument : Int -> Int -> String
getArgument index frameSize =
  {- FRAMESIZE INDEX SUB NDUP -}
  (toString frameSize) ++ " " ++ (toString index) ++ " SUB NDUP"

setVariable : Int -> String -> String
setVariable index value =
  {- VALUE SIZE INDEX SUB DUP ROT SWAP NTUCK NROT DROP -}
  value ++ " SIZE " ++ (toString index) ++ " SUB DUP ROT SWAP NTUCK NROT DROP"

setLocalVariable : Int -> Int -> String -> String
setLocalVariable index frameSize value =
  {- VALUE FRAMESIZE INDEX SUB DUP ROT SWAP NTUCK NROT DROP -}
  value ++ " " ++ (toString frameSize) ++ " " ++ (toString index) ++ " SUB DUP ROT SWAP NTUCK NROT DROP"

addToFrameSize : Int -> State -> State
addToFrameSize value state =
  let newSize = state.frameSize + value in
  { state | frameSize = newSize }

getNbArgs : State -> String -> Int
getNbArgs state funcname =
  case Dict.get funcname state.procedures of
    Just (_, nbArgs) ->
      nbArgs
    Nothing ->
      case Dict.get funcname state.functions of
        Just (_, nbArgs, _) ->
          nbArgs
        Nothing ->
          case Dict.get funcname state.definedProcedures of
            Just (nbArgs, _) ->
              nbArgs
            Nothing ->
              case Dict.get funcname state.definedFunctions of
                Just (nbArgs, _, _) ->
                  nbArgs
                Nothing ->
                  -1 {- Never happens -}

{-| Get stack difference before and after the procedure/function call -}
getStackDiff : State -> String -> Int
getStackDiff state funcname =
  case Dict.get funcname state.procedures of
    Just (_, nbArgs) ->
      nbArgs
    Nothing ->
      case Dict.get funcname state.functions of
        Just (_, nbArgs, nbReturn) ->
          nbArgs - nbReturn
        Nothing ->
          case Dict.get funcname state.definedProcedures of
            Just (nbArgs, _) ->
              nbArgs
            Nothing ->
              case Dict.get funcname state.definedFunctions of
                Just (nbArgs, _, nbReturn) ->
                  nbArgs - nbReturn
                Nothing ->
                  -1 {- Never happens -}

{-| Numbers supported are between 0 and 0x7fff so transform every number into that -}
splitDecimalInBytes : String -> String
splitDecimalInBytes nbstr =
  case nbstr |> String.toInt of
    Err _ ->
      "" {- Never happens -}
    Ok nb ->
      let aux mynb =
        if mynb < 0 then
          "0 " ++ aux (-mynb) ++ " SUB"
        else if mynb > 32767 then
          "32767 " ++ aux (mynb - 32767) ++ " ADD"
        else
          toString mynb
      in
      aux nb

{-| Add procedure or function from stdlib to the functions to define if not already there -}
addProcFuncDef : String -> State -> State
addProcFuncDef procfuncname state =
  let code =
    case Dict.get procfuncname state.procedures of
      Just (asm, _) ->
        asm
      Nothing ->
        case Dict.get procfuncname state.functions of
          Just (asm, _, _) ->
            asm
          Nothing ->
            ""
  in
  if code == "" then
    state
  else
    let procfunc = procfuncname ++ ":\n" ++ code ++ " ret" in
    if List.member procfunc state.procfuncToDefine then
      state
    else
      { state | procfuncToDefine = ( procfunc :: state.procfuncToDefine )}

{-| Assign the values in 'rightCode' to the variables in 'variableList' one by one -}
compileAssign : State -> List ParseTree -> String -> (State, String)
compileAssign state variableList rightCode =
  case variableList of
    [] ->
      (state, rightCode)
    token :: endList ->
      case token of
        {- Set the variables to the last value on the stack and remove 1 from the size of the stack -}
        Leaf variable ->
          case variable.tokenType of
            VARIABLE ->
              let (newState, assignCode) = (addToFrameSize (-1) state, rightCode ++ " " ++ (setVariable variable.stackPosition "")) in
              compileAssign newState endList assignCode
            LOCALVARIABLE ->
              let (newState, assignCode) = (addToFrameSize (-1) state, rightCode ++ " " ++ (setLocalVariable variable.stackPosition state.frameSize "")) in
              compileAssign newState endList assignCode
            other ->
              (state, "") {- Never happens -}
        other ->
          (state, "") {- Never happens -}

{-| Compiling a list of ParseTree recursively , keeping the order -}
compileTreeList : List ParseTree -> State -> String -> (State, String)
compileTreeList treeList state asmSeen =
  case treeList of
    tree :: nextList ->
      let (newState, asm) = compileTree tree state in
      compileTreeList nextList newState (asmSeen ++ " " ++ asm)
    [] ->
      (state, asmSeen)


compileTree : ParseTree -> State -> (State, String)
compileTree tree state =
  case tree of
    {- Leave assembly as it is -}
    Asm asm ->
      (state, asm)
    Leaf token ->
      case token.tokenType of
        {- Split number into chunks and push onto stack -}
        NUMBER ->
          (addToFrameSize 1 state , splitDecimalInBytes token.lexeme)
        {- Create new variable and signal in the VM code -}
        CREATEVAR ->
          if state.isVm then
            (addToFrameSize 1 state, createVariable token.stackPosition ++ " #VAR " ++ token.lexeme)
          else
            (addToFrameSize 1 state, createVariable token.stackPosition)
        {- Create new local variable and signal in the VM code -}
        CREATELOCALVAR ->
          if state.isVm then
            (addToFrameSize 1 state, createLocalVariable token.stackPosition state.frameSize ++ " #VAR " ++ token.lexeme)
          else
            (addToFrameSize 1 state, createLocalVariable token.stackPosition state.frameSize)
        {- Get variable value -}
        VARIABLE ->
          (addToFrameSize 1 state, getVariable token.stackPosition)
        {- Get local variable value -}
        LOCALVARIABLE ->
          (addToFrameSize 1 state, getArgument token.stackPosition state.frameSize)
        {- Get argument value -}
        ARGUMENT ->
          (addToFrameSize 1 state, getArgument token.stackPosition state.frameSize)
        KEYWORD ->
          (state,"") {- Never happens, this has been preprocessed before -}
        FUNCTION ->
          (state,"") {- Never happens, this has been preprocessed before -}
        PROCEDURE ->
          (state,"") {- Never happens, this has been preprocessed before -}
        DEFINEDPROCEDURE ->
          (state,"") {- Never happens, this has been preprocessed before -}
        DEFINEDFUNCTION ->
          (state, "") {- Never happens, this has been preprocessed before -}
        NAME ->
          (state,"") {- Never happens, this has been preprocessed before -}
    {- Add End Of Instruction (EOI) marker in the VM to signal current line number -}
    EOI linenumber ->
      if state.isVm then
        (state, "#EOI " ++ toString linenumber)
      else
        (state, "")
    {- Compile list of trees -}
    UniNode(treeList) ->
      compileTreeList treeList state ""
    BiNode(operation, leftTree, rightTree) ->
      case operation of
        {- Transform trees into Reverse Polish Notation for the stack -}
        ADDITION ->
          let (newState, leftCode) = compileTree leftTree state in
          let (newState2, rightCode) = compileTree rightTree newState in
          (addToFrameSize (-1) newState2,leftCode ++ " " ++ rightCode ++ " ADD")
        SUBSTRACTION ->
          let (newState, leftCode) = compileTree leftTree state in
          let (newState2, rightCode) = compileTree rightTree newState in
          (addToFrameSize (-1) newState2,leftCode ++ " " ++ rightCode ++ " SUB")
        MULTIPLICATION ->
          let (newState, leftCode) = compileTree leftTree state in
          let (newState2, rightCode) = compileTree rightTree newState in
          (addToFrameSize (-1) newState2,leftCode ++ " " ++ rightCode ++ " MUL")
        DIVISION ->
          let (newState, leftCode) = compileTree leftTree state in
          let (newState2, rightCode) = compileTree rightTree newState in
          (addToFrameSize (-1) newState2,leftCode ++ " " ++ rightCode ++ " DIV")
        ASSIGN ->
          {- Compile the values then assign the variables
            (reverse them because the first value is popped last from the stack) -}
          let (newState2, rightCode) = compileTree rightTree state in
          case leftTree of
            UniNode variableList ->
              compileAssign newState2 (List.reverse variableList) rightCode
            other ->
              (newState2, "") {- Never happens -}
        PENDING_ASSIGN ->
          (state,"") {- Never happens, this has been processed before -}
        {- Transform trees into Reverse Polish Notation for the stack -}
        Types.LT ->
          let (newState, leftCode) = compileTree leftTree state in
          let (newState2, rightCode) = compileTree rightTree newState in
          (addToFrameSize (-1) newState2,leftCode ++ " " ++ rightCode ++ " LT")
        Types.LE ->
          let (newState, leftCode) = compileTree leftTree state in
          let (newState2, rightCode) = compileTree rightTree newState in
          (addToFrameSize (-1) newState2,leftCode ++ " " ++ rightCode ++ " LE")
        Types.GT ->
          let (newState, leftCode) = compileTree leftTree state in
          let (newState2, rightCode) = compileTree rightTree newState in
          (addToFrameSize (-1) newState2,leftCode ++ " " ++ rightCode ++ " GT")
        Types.GE ->
          let (newState, leftCode) = compileTree leftTree state in
          let (newState2, rightCode) = compileTree rightTree newState in
          (addToFrameSize (-1) newState2,leftCode ++ " " ++ rightCode ++ " GE")
        Types.EQ ->
          let (newState, leftCode) = compileTree leftTree state in
          let (newState2, rightCode) = compileTree rightTree newState in
          (addToFrameSize (-1) newState2,leftCode ++ " " ++ rightCode ++ " EQ")
        Types.NE ->
          let (newState, leftCode) = compileTree leftTree state in
          let (newState2, rightCode) = compileTree rightTree newState in
          (addToFrameSize (-1) newState2,leftCode ++ " " ++ rightCode ++ " EQ 0 1 SUB MUL 1 ADD")
        {- Create labels and jumps for the end and conditional branches -}
        {- condition DEC endif_i CJMP conditionalCode endif_i: -}
        IFTHEN ->
          let (newState, condition) = compileTree leftTree state in
          let (newState2, condCode) = compileTree rightTree (addToFrameSize (-1) newState) in
          let ifCount = newState2.ifCount + 1 in
          let strIfCount = toString ifCount in
          ({ newState2 | ifCount = ifCount }, condition ++ " DEC endif_" ++ strIfCount ++ " CJMP " ++ condCode ++ " endif_" ++ strIfCount ++":\n")
        {- Create labels and jumps for the end and conditional branches -}
        {- condition DEC endwhile_i CJMP while_i: whileCode condition while_i CJMP endwhile_i: -}
        WHILE ->
          let (newState, condition) = compileTree leftTree state in
          let (newState2, whileCode) = compileTree rightTree (addToFrameSize (-1) newState) in
          let whileCount = newState2.whileCount + 1 in
          let strWhileCount = toString whileCount in
          ( { newState2 | whileCount = whileCount }, condition ++ " DEC endwhile_" ++ strWhileCount ++ " CJMP while_" ++ strWhileCount ++ ":\n" ++ whileCode ++ " " ++ condition ++ " while_" ++ strWhileCount ++ " CJMP endwhile_" ++ strWhileCount ++ ":")
        {- Get procedure name, Initialize the frame size to the number of arguments, Compile code, Add it to ToDefine list -}
        PROCDEF ->
          let (newState, procname) = compileTree rightTree state in
          let zeroedState = { newState | frameSize = (getNbArgs state procname) } in
          let (newState, procCode) = compileTree leftTree zeroedState in
          let procedure = procname ++ ":\n" ++ procCode in
          ( { newState | procfuncToDefine = ( procedure :: newState.procfuncToDefine )}, "" )
        {- Get function name, Initialize the frame size to the number of arguments, Compile code, Add it to ToDefine list -}
        FUNCDEF ->
          let (newState, funcname) = compileTree rightTree state in
          let zeroedState = { newState | frameSize = (getNbArgs state funcname) } in
          let (newState, funcCode) = compileTree leftTree zeroedState in
          let function = funcname ++ ":\n" ++ funcCode in
          ( { newState | procfuncToDefine = ( function :: newState.procfuncToDefine )}, "" )


    TriNode(operation, leftTree, middleTree, rightTree) ->
      case operation of
        {- condition if_i CJMP elseCode endif_i jmp if_i: ifCode endif_i: -}
        IFELSE ->
          let (newState, condition) = compileTree leftTree state in
          let (newState2, ifCode) = compileTree middleTree (addToFrameSize (-1) newState) in
          let (newState3, elseCode) = compileTree rightTree newState2 in
          let ifCount = newState3.ifCount + 1 in
          let strIfCount = toString ifCount in
          ( { newState3 | ifCount = ifCount}, condition ++ " if_" ++ strIfCount ++ " CJMP " ++ elseCode ++ " endif_" ++ strIfCount ++ " jmp if_" ++ strIfCount ++ ": " ++ ifCode ++ " endif_" ++ strIfCount ++ ":")

        {- Add procedure to ToDefine if not there, Push arguments, Mark procedure call if in VM, Push call -}
        PROC ->
          let (newState, arguments) = compileTree rightTree state in
          let (newState2, procCall) = compileTree middleTree newState in
          let (_, procname)         = compileTree leftTree newState2 in
          let newState3 = addProcFuncDef procname newState2 in
          let stackDiff = getStackDiff newState2 procname in
          if state.isVm then
            let nbArgs = getNbArgs state procname in
            ( addToFrameSize (0 - stackDiff) newState3, arguments ++ " #FUNC " ++ procname ++ " " ++ (toString nbArgs) ++ " " ++ procCall)
          else
            ( addToFrameSize (0 - stackDiff) newState3, arguments ++ " " ++ procCall)

        {- Add function to ToDefine if not there, Push arguments, Mark function call if in VM, Push call -}
        FUNC ->
          let (newState, arguments) = compileTree rightTree state in
          let (newState2, funcCall) = compileTree middleTree newState in
          let (_, funcname)         = compileTree leftTree newState2 in
          let newState3 = addProcFuncDef funcname newState2 in
          let stackDiff = getStackDiff newState2 funcname in
          if state.isVm then
            let nbArgs = getNbArgs state funcname in
            ( addToFrameSize (0 - stackDiff) newState3, arguments ++ " #FUNC " ++ funcname ++ " " ++ (toString nbArgs) ++ " " ++ funcCall)
          else
            ( addToFrameSize (0 - stackDiff) newState3, arguments ++ " " ++ funcCall)

{-| Compile, add 'halt' add the end of the program, add the function labels -}
compile : ParseTree  -> State -> String
compile tree state =
  let (newState, code) = (compileTree tree state) in
  code ++ "\nhalt\n" ++ (String.join "\n" newState.procfuncToDefine)


compiler : Dict String (String, Int) -> Dict String (String, Int, Int) -> Dict String (Int, Int) -> Dict String (Int, Int, Int) -> Bool ->  ParseTree -> String
compiler procedures functions definedProcedures definedFunctions isVm tree =
  compile tree (initState procedures functions definedProcedures definedFunctions isVm)
