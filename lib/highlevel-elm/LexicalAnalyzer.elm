module LexicalAnalyzer exposing (keywords, lexicalAnalyzer)

import Types exposing (TokenType(..), Token)
import Errors exposing (Error, createLexAnaError, printError)
import Tools exposing (findIndex)
import Regex exposing (HowMany(..))
import Dict exposing (Dict)

variable = [ "variable" ]
arith    = [ "+", "-", "*", "/", "=", "(", ")" ]
ifthen   = [ "if", "then", "else" , "endif"]
while    = [ "while", "endwhile" ]
do       = [ "do" ]
bool     = [ "<", ">", "<=", ">=", "==", "!=" ]
define   = [ "define", "as", "enddefine"]
function = [ "function", "return", "endfunction"]

keywords = variable ++ arith ++ ifthen ++ while ++ do ++ bool ++ define ++ function

type alias State =
  { isVariableCreation        : Bool                            {- currently on a variable creation line? -}
  , isFunction                : Bool                            {- in case of procedure/function definition, is it a function? -}
  , isProcFuncHeader          : Bool                            {- in case of procedure/function definition, in the name/arguments declaration? -} {- State 2/3 -}
  , isProcFuncDefinition      : Bool                            {- in case of procedure/function definition, in the core definition? -} {- State 4 -}
  , isFunctionReturn          : Bool                            {- in case of function definition, in the definition of return values? -} {- State 5 -}
  , procFuncName              : String                          {- empty string until name declared -}
  , procFuncArguments         : List String                     {- list of arguments names -}
  , procFuncLocalVariables    : List String                     {- list of local variable names -}
  , variables                 : List String                     {- list of declared variables -}
  , procedures                : Dict String (String, Int)       {- dict of procedure names, code and number of arguments -}
  , functions                 : Dict String (String, Int, Int)  {- dict of function names, code, number of arguments and return values -}
  , guessedProcedures         : List String                     {- guessed defined procedures using regexes -}
  , guessedFunctions          : List String                     {- guessed defined procedures using regexes -}
  , definedProcedures         : Dict String (Int, Int)          {- dict of procedure names, number of arguments and local variables -}
  , definedFunctions          : Dict String (Int, Int, Int)     {- dict of function names, number of arguments, local variables and zero (unknown number of return values) -}
  , definedProceduresUsage    : List String                     {- procedure and arguments names -}
  , definedFunctionsUsage     : List String                     {- function and arguments names -}
  , ch                        : Int                             {- current column position -}
  }

initState : Dict String (String, Int) -> Dict String (String, Int, Int) -> State
initState procNames functionNames = (State False False False False False "" [] [] [] procNames functionNames [] [] Dict.empty Dict.empty [] [] 0)

{-| Switch from State 4 to State 1 and add new procedure -}
switchState1P : Dict String (Int, Int) -> State -> State
switchState1P definedProcedures state =
  let newProcedureUsage = state.procFuncName ++ " " ++ String.join " " state.procFuncArguments in
  let definedProceduresUsage = newProcedureUsage :: state.definedProceduresUsage in
  { state | isProcFuncDefinition = False, definedProcedures = definedProcedures, procFuncName = "", definedProceduresUsage = definedProceduresUsage }

{-| Switch from State 5 to State 1 and add new function -}
switchState1F : Dict String (Int, Int, Int) -> State -> State
switchState1F definedFunctions state =
  let newFunctionUsage = state.procFuncName ++ " " ++ String.join " " state.procFuncArguments in
  let definedFunctionsUsage = newFunctionUsage :: state.definedFunctionsUsage in
  { state | isFunctionReturn = False, definedFunctions = definedFunctions, procFuncName = "", definedFunctionsUsage = definedFunctionsUsage }

{-| Switch to State 2 for procedure definition -}
switchState2P : State -> State
switchState2P state =
  { state | isFunction = False, isProcFuncHeader = True, procFuncArguments = [], procFuncLocalVariables = [] }

{-| Switch to State 2 for function definition -}
switchState2F : State -> State
switchState2F state =
  { state | isFunction = True, isProcFuncHeader = True, procFuncArguments = [], procFuncLocalVariables = [] }

{-| Switch to State 3 after adding procedure/function name -}
switchState3 : String -> State -> State
switchState3 procFuncName state =
  { state | procFuncName = procFuncName }

{-| Switch to State 4 after adding arguments -}
switchState4 : List String -> State -> State
switchState4 procFuncArguments state =
  { state | isProcFuncHeader = False, isProcFuncDefinition = True, procFuncArguments = procFuncArguments }

{-| Switch to State 5 for return definition -}
switchState5 : State -> State
switchState5 state =
  { state | isProcFuncDefinition = False, isFunctionReturn = True }

{-| Add new argument to argument list -}
addArgument : String -> State -> State
addArgument argument state =
  { state | procFuncArguments = ( argument :: state.procFuncArguments )}

{-| Add new local variable to list -}
addLocalVariable : String -> State -> State
addLocalVariable variable state =
   let procFuncLocalVariables = variable :: state.procFuncLocalVariables in
   { state | procFuncLocalVariables = procFuncLocalVariables }

{-| Add new global variable to list -}
addGlobalVariable : String -> State -> State
addGlobalVariable variable state =
  let variables = variable :: state.variables in
  { state | variables = variables }

{-| Increase the columns -}
increaseChBy : String -> State -> State
increaseChBy word state =
  let ch = state.ch + (String.length word + 1) in
  { state | ch = ch }

variableCreationOn : State -> State
variableCreationOn state =
  { state | isVariableCreation = True }

variableCreationOff : State -> State
variableCreationOff state =
  { state | isVariableCreation = False, ch = 0 }

{-| Return a list of defined names: variables, keywords, procedure and function names plus current procedure name, arguments and variables -}
getDefinedNames : State -> List String
getDefinedNames state =
  let names =
  [ variable
  , ifthen
  , while
  , do
  , define
  , function
  , Dict.keys state.procedures
  , Dict.keys state.functions
  , Dict.keys state.definedProcedures
  , Dict.keys state.definedFunctions
  , state.variables
  ]
  |> List.concat
  in
  if state.procFuncName == "" then
    names
  else
    [ state.procFuncName :: names
    , state.procFuncLocalVariables
    , state.procFuncArguments ]
    |> List.concat


replaceColourMacro: String -> String
replaceColourMacro colour =
  case colour of
    "black"   -> "0"
    "blue"    -> "1"
    "green"   -> "2"
    "cyan"    -> "3"
    "red"     -> "4"
    "magenta" -> "5"
    "yellow"  -> "6"
    "white"   -> "7"
    other     -> "-1"

replaceNoteMacro : String -> String
replaceNoteMacro note =
  (case note of
    "C1"  -> 33
    "C1#" -> 35
    "D1"  -> 37
    "D1#" -> 39
    "E1"  -> 41
    "F1"  -> 44
    "F1#" -> 46
    "G1"  -> 49
    "G1#" -> 52
    "A1"  -> 55
    "A1#" -> 58
    "B1"  -> 62
    "C2"  -> 65
    "C2#" -> 69
    "D2"  -> 73
    "D2#" -> 78
    "E2"  -> 82
    "F2"  -> 87
    "F2#" -> 92
    "G2"  -> 98
    "G2#" -> 104
    "A2"  -> 110
    "A2#" -> 117
    "B2"  -> 123
    "C3"  -> 131
    "C3#" -> 139
    "D3"  -> 147
    "D3#" -> 156
    "E3"  -> 165
    "F3"  -> 175
    "F3#" -> 185
    "G3"  -> 196
    "G3#" -> 208
    "A3"  -> 220
    "A3#" -> 233
    "B3"  -> 247
    "C4"  -> 262
    "C4#" -> 277
    "D4"  -> 294
    "D4#" -> 311
    "E4"  -> 330
    "F4"  -> 349
    "F4#" -> 370
    "G4"  -> 392
    "G4#" -> 415
    "A4"  -> 440
    "A4#" -> 446
    "B4"  -> 494
    "C5"  -> 523
    "C5#" -> 554
    "D5"  -> 587
    "D5#" -> 622
    "E5"  -> 659
    "F5"  -> 698
    "F5#" -> 698
    "G5"  -> 784
    "G5#" -> 831
    "A5"  -> 880
    "A5#" -> 932
    "B5"  -> 988
    "C6"  -> 1046
    other -> -1)
  |> toString



{- Err lexeme   -- unmatched lexeme
   Ok Err token -- error in token
   Ok Ok token  -- matched token
-}


{- #########################################
             State 1 | Main code.
   ######################################### -}

forbiddenKeywords1 = define ++ function

isKeywordState1 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isKeywordState1 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      if List.member lexeme keywords then
        let token = { tokenType = KEYWORD, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
        if state.isVariableCreation then
          (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 101) ), state)
        else if lexeme == "variable" then
          (Ok (Ok Nothing), variableCreationOn state)
        else if lexeme == "define" then
          (Ok (Ok (Just token)), switchState2P state)
        else if lexeme == "function" then
          (Ok (Ok (Just token)), switchState2F state)
        else if List.member lexeme forbiddenKeywords1 then
          (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 110) ), state)
        else
          (Ok (Ok (Just token)), state)
      else
        (word, state)

isProcOrFunctionState1 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isProcOrFunctionState1 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      if state.isVariableCreation then
        if Dict.member lexeme state.procedures || Dict.member lexeme state.functions || Dict.member lexeme state.definedProcedures || Dict.member lexeme state.definedFunctions then
          (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 102) ), state)
        else
          (word, state)
      else
        if Dict.member lexeme state.procedures then
          (Ok (Ok (Just { tokenType = PROCEDURE, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 })), state)
        else if Dict.member lexeme state.functions then
          (Ok (Ok (Just { tokenType = FUNCTION, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 })), state)
        else if (Dict.member lexeme state.definedProcedures || List.member lexeme state.guessedProcedures) then
          (Ok (Ok (Just { tokenType = DEFINEDPROCEDURE, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 })), state)
        else if (Dict.member lexeme state.definedFunctions || List.member lexeme state.guessedFunctions) then
          (Ok (Ok (Just { tokenType = DEFINEDFUNCTION, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 })), state)
        else (word, state)

isNumberState1 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isNumberState1 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      {- check is the lexeme is a number -}
      if Regex.contains (Regex.regex "^-?[0-9]+$") lexeme then
        if state.isVariableCreation then
          (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 103) ), state)
        else
          let token = { tokenType = NUMBER, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
          (Ok (Ok (Just token)), state)
      else if replaceNoteMacro lexeme /= "-1" then
          if state.isVariableCreation then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 105) ), state)
          else
            let token = { tokenType = NUMBER, lexeme = (replaceNoteMacro lexeme), ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
            (Ok (Ok (Just token)), state)
      else if replaceColourMacro lexeme /= "-1" then
        if state.isVariableCreation then
          (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 106) ), state)
        else
          let token = { tokenType = NUMBER, lexeme = (replaceColourMacro lexeme), ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
          (Ok (Ok (Just token)), state)
      else
        (word, state)

isVariableState1 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isVariableState1 lineNumber (word, state) =
  case word of
    Ok (Ok token) ->
      (Ok (Ok token), state)
    Ok (Err e) ->
      (Ok (Err e), state)
    Err lexeme ->
      if Regex.contains (Regex.regex "^[a-zA-Z][a-zA-Z0-9_]*$") lexeme then

        if state.isVariableCreation then
          if List.member lexeme state.variables then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 104) ), state)
          else
            let token = { tokenType = CREATEVAR, lexeme  = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = List.length state.variables } in
            (Ok (Ok (Just token)), addGlobalVariable lexeme state)

        else
          if List.member lexeme state.variables then
            (Ok (Ok (Just { tokenType = VARIABLE, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = (List.length state.variables - 1 - (findIndex lexeme state.variables)) })), state)
          else
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme (Just (getDefinedNames state)) 111) ), state)
      else
        (Err lexeme, state)



{- #########################################
    State 2 | Procedure or Function declaration. Waiting for name.
   ######################################### -}

isKeywordState2 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isKeywordState2 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      if List.member lexeme keywords then
        (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 120) ), state)
      else
        (word, state)

isProcOrFunctionState2 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isProcOrFunctionState2 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      if Dict.member lexeme state.procedures || Dict.member lexeme state.functions || Dict.member lexeme state.definedProcedures || Dict.member lexeme state.definedFunctions then
        (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 121) ), state)
      else
        (word, state)

isNumberState2 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isNumberState2 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      {- check is the lexeme is a number -}
      if Regex.contains (Regex.regex "^-?[0-9]+$") lexeme then
        (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 122) ), state)
      else if replaceNoteMacro lexeme /= "-1" then
        (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 124) ), state)
      else if replaceColourMacro lexeme /= "-1" then
        (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 125) ), state)
      else
        (word, state)

isVariableState2 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isVariableState2 lineNumber (word, state) =
  case word of
    Ok (Ok token) ->
      (Ok (Ok token), state)
    Ok (Err e) ->
      (Ok (Err e), state)
    Err lexeme ->
      if Regex.contains (Regex.regex "^[a-zA-Z][a-zA-Z0-9_]*$") lexeme then
          if List.member lexeme state.variables then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 123) ), state)
          else if Regex.contains (Regex.regex "^if_[0-9]*$") lexeme || Regex.contains (Regex.regex "^endif_[0-9]*$") lexeme || Regex.contains (Regex.regex "^while_[0-9]*$") lexeme || Regex.contains (Regex.regex "^endwhile_[0-9]*$") lexeme then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 126) ), state)
          else
            let token = { tokenType = NAME, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
            (Ok (Ok ( Just token )), switchState3 lexeme state )
      else
        (Err lexeme, state)


{- #########################################
    State 3 | Procedure or Function declaration. Waiting for arguments.
   ######################################### -}


isKeywordState3 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isKeywordState3 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      if lexeme == "as" then
        let token = { tokenType = KEYWORD, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
        let procFuncArguments = List.reverse state.procFuncArguments in
        (Ok (Ok (Just token)), switchState4 procFuncArguments state)
      else if List.member lexeme keywords then
        (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 130) ), state)
      else
        (word, state)

isProcOrFunctionState3 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isProcOrFunctionState3 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      if Dict.member lexeme state.procedures || Dict.member lexeme state.functions || Dict.member lexeme state.definedProcedures || Dict.member lexeme state.definedFunctions then
        (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 131) ), state)
      else
        (word, state)

isNumberState3 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isNumberState3 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      {- check is the lexeme is a number -}
      if Regex.contains (Regex.regex "^-?[0-9]+$") lexeme then
        (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 132) ), state)
      else if replaceNoteMacro lexeme /= "-1" then
        (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 136) ), state)
      else if replaceColourMacro lexeme /= "-1" then
        (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 137) ), state)
      else
        (word, state)

isVariableState3 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isVariableState3 lineNumber (word, state) =
  case word of
    Ok (Ok token) ->
      (Ok (Ok token), state)
    Ok (Err e) ->
      (Ok (Err e), state)
    Err lexeme ->
      if Regex.contains (Regex.regex "^[a-zA-Z][a-zA-Z0-9_]*$") lexeme then
          if List.member lexeme state.variables then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 133) ), state)
          else if List.member lexeme state.procFuncArguments then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 134) ), state)
          else if lexeme == state.procFuncName then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 135) ), state)
          else
            (Ok (Ok Nothing), addArgument lexeme state )
      else
        (Err lexeme, state)


{- #########################################
    State 4 | Procedure or Function definition.
   ######################################### -}

forbiddenKeywords4 = define ++ function

isKeywordState4 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isKeywordState4 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      if List.member lexeme keywords then
        let token = { tokenType = KEYWORD, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
        if state.isVariableCreation then
          (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 101) ), state)
        else if lexeme == "variable" then
          (Ok (Ok Nothing), variableCreationOn state)
        else if lexeme == "enddefine" && not state.isFunction then
          let definedProcedures = (Dict.insert state.procFuncName (List.length state.procFuncArguments, List.length state.procFuncLocalVariables) state.definedProcedures) in
          (Ok (Ok (Just token)), switchState1P definedProcedures state)
        else if lexeme == "endfunction" then
          (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 142) ), state)
        else if lexeme == "return" && state.isFunction then
          (Ok (Ok (Just token)), switchState5 state)
        else if List.member lexeme forbiddenKeywords4 then
          if state.isFunction then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 141) ), state)
          else
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 140) ), state)
        else
          (Ok (Ok (Just token)), state)
      else
        (word, state)

isProcOrFunctionState4 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isProcOrFunctionState4 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      if state.isVariableCreation then
        if Dict.member lexeme state.procedures || Dict.member lexeme state.functions || Dict.member lexeme state.definedProcedures || Dict.member lexeme state.definedFunctions || lexeme == state.procFuncName then
          (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 102) ), state)
        else
          (word, state)
      else
        if Dict.member lexeme state.procedures then
          (Ok (Ok (Just { tokenType = PROCEDURE, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 })), state)
        else if Dict.member lexeme state.functions then
          (Ok (Ok (Just { tokenType = FUNCTION, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 })), state)
        else if Dict.member lexeme state.definedProcedures || (lexeme == state.procFuncName && not state.isFunction) then
          (Ok (Ok (Just { tokenType = DEFINEDPROCEDURE, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 })), state)
        else if Dict.member lexeme state.definedFunctions || (lexeme == state.procFuncName && state.isFunction) then
          (Ok (Ok (Just { tokenType = DEFINEDFUNCTION, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 })), state)
        else (word, state)

isNumberState4 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isNumberState4 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      {- check is the lexeme is a number -}
      if Regex.contains (Regex.regex "^-?[0-9]+$") lexeme then
        if state.isVariableCreation then
          (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 103) ), state)
        else
          let token = { tokenType = NUMBER, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
          (Ok (Ok (Just token)), state)
      else if replaceNoteMacro lexeme /= "-1" then
          if state.isVariableCreation then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 105) ), state)
          else
            let token = { tokenType = NUMBER, lexeme = (replaceNoteMacro lexeme), ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
            (Ok (Ok (Just token)), state)
      else if replaceColourMacro lexeme /= "-1" then
          if state.isVariableCreation then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 106) ), state)
          else
            let token = { tokenType = NUMBER, lexeme = (replaceColourMacro lexeme), ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
            (Ok (Ok (Just token)), state)
      else
        (word, state)

isVariableState4 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isVariableState4 lineNumber (word, state) =
  case word of
    Ok (Ok token) ->
      (Ok (Ok token), state)
    Ok (Err e) ->
      (Ok (Err e), state)
    Err lexeme ->
      if Regex.contains (Regex.regex "^[a-zA-Z][a-zA-Z0-9_]*$") lexeme then

        if state.isVariableCreation then
          if List.member lexeme state.variables then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 104) ), state)
          else if List.member lexeme state.procFuncLocalVariables then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 143) ), state)
          else if List.member lexeme state.procFuncArguments then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 144) ), state)
          else
            let token = { tokenType = CREATELOCALVAR, lexeme  = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = (List.length state.procFuncArguments + List.length state.procFuncLocalVariables) } in
            (Ok (Ok ( Just token)) , addLocalVariable lexeme state)

        else
          if List.member lexeme state.variables then
            (Ok (Ok (Just { tokenType = VARIABLE, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = (List.length state.variables - 1 - (findIndex lexeme state.variables)) })), state)
          else if List.member lexeme state.procFuncLocalVariables then
            (Ok (Ok (Just { tokenType = LOCALVARIABLE, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = (List.length state.procFuncArguments + List.length state.procFuncLocalVariables - 1 - (findIndex lexeme state.procFuncLocalVariables)) })), state)
          else if List.member lexeme state.procFuncArguments then
            (Ok (Ok (Just { tokenType = ARGUMENT, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = (findIndex lexeme state.procFuncArguments)})), state)
          else
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme (Just (getDefinedNames state)) 145) ), state)

      else
        (Err lexeme, state)



{- #########################################
    State 5 | Function Return.
   ######################################### -}

forbiddenKeywords5 = variable ++ ifthen ++ while ++ do ++ define ++ function

isKeywordState5 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isKeywordState5 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      let token = { tokenType = KEYWORD, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
      if lexeme == "endfunction" then
        let definedFunctions = (Dict.insert state.procFuncName (List.length state.procFuncArguments, List.length state.procFuncLocalVariables, 0) state.definedFunctions) in
        (Ok (Ok (Just token)), switchState1F definedFunctions state)
      else if List.member lexeme forbiddenKeywords5 then
        (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 150) ), state)
      else if List.member lexeme keywords then
        (Ok (Ok (Just token)), state)
      else
        (word, state)

isProcOrFunctionState5 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isProcOrFunctionState5 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      if Dict.member lexeme state.procedures || Dict.member lexeme state.functions || Dict.member lexeme state.definedProcedures || Dict.member lexeme state.definedFunctions || lexeme == state.procFuncName then
        (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 151) ), state)
      else
        (word, state)

isNumberState5 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isNumberState5 lineNumber (word, state) =
  case word of
    Ok _ ->
      (word, state)
    Err lexeme ->
      {- check is the lexeme is a number -}
      if Regex.contains (Regex.regex "^-?[0-9]+$") lexeme then
        let token = { tokenType = NUMBER, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
        (Ok (Ok (Just token)), state)
      else if replaceNoteMacro lexeme /= "-1" then
          if state.isVariableCreation then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 105) ), state)
          else
            let token = { tokenType = NUMBER, lexeme = (replaceNoteMacro lexeme), ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
            (Ok (Ok (Just token)), state)
      else if replaceColourMacro lexeme /= "-1" then
          if state.isVariableCreation then
            (Ok (Err (createLexAnaError lineNumber state.ch lexeme Nothing 106) ), state)
          else
            let token = { tokenType = NUMBER, lexeme = (replaceColourMacro lexeme), ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = 0 } in
            (Ok (Ok (Just token)), state)
      else
        (word, state)

isVariableState5 : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result String (Result Error (Maybe Token)), State)
isVariableState5 lineNumber (word, state) =
  case word of
    Ok (Ok token) ->
      (Ok (Ok token), state)
    Ok (Err e) ->
      (Ok (Err e), state)
    Err lexeme ->
      if Regex.contains (Regex.regex "^[a-zA-Z][a-zA-Z0-9_]*$") lexeme then
        if List.member lexeme state.variables then
          (Ok (Ok (Just { tokenType = VARIABLE, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = (List.length state.variables - 1 - (findIndex lexeme state.variables)) })), state)
        else if List.member lexeme state.procFuncLocalVariables then
          (Ok (Ok (Just { tokenType = LOCALVARIABLE, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = (List.length state.procFuncArguments + List.length state.procFuncLocalVariables - 1 - (findIndex lexeme state.procFuncLocalVariables)) })), state)
        else if List.member lexeme state.procFuncArguments then
          (Ok (Ok (Just { tokenType = ARGUMENT, lexeme = lexeme, ch = state.ch, line = lineNumber, length = (String.length lexeme), stackPosition = (findIndex lexeme state.procFuncArguments)})), state)
        else
          (Ok (Err (createLexAnaError lineNumber state.ch lexeme (Just (getDefinedNames state)) 145) ), state)

      else
        (Err lexeme, state)


hasFailed : Int -> (Result String (Result Error (Maybe Token)), State) -> (Result Error (Maybe Token), State)
hasFailed lineNumber (word, state) =
  case word of
    {- lexeme already matched -}
    Ok (Ok token) ->
      (Ok token, state)
    Ok (Err e) ->
      (Err e, state)
    Err lexeme ->
      (Err (createLexAnaError lineNumber state.ch lexeme Nothing 100) , state)


getToken : Int -> State -> String -> (Result Error (Maybe Token), State)
getToken lineNumber state lexeme =
  if state.isFunctionReturn then
    (Err lexeme, state)
    |> isKeywordState5 lineNumber
    |> isProcOrFunctionState5 lineNumber
    |> isNumberState5 lineNumber
    |> isVariableState5 lineNumber
    |> hasFailed lineNumber
  else if state.isProcFuncDefinition then
    (Err lexeme, state)
    |> isKeywordState4 lineNumber
    |> isProcOrFunctionState4 lineNumber
    |> isNumberState4 lineNumber
    |> isVariableState4 lineNumber
    |> hasFailed lineNumber
  else if state.isProcFuncHeader && state.procFuncName /= "" then
    (Err lexeme, state)
    |> isKeywordState3 lineNumber
    |> isProcOrFunctionState3 lineNumber
    |> isNumberState3 lineNumber
    |> isVariableState3 lineNumber
    |> hasFailed lineNumber
  else if state.isProcFuncHeader && state.procFuncName == "" then
    (Err lexeme, state)
    |> isKeywordState2 lineNumber
    |> isProcOrFunctionState2 lineNumber
    |> isNumberState2 lineNumber
    |> isVariableState2 lineNumber
    |> hasFailed lineNumber
  else
    (Err lexeme, state)
    |> isKeywordState1 lineNumber
    |> isProcOrFunctionState1 lineNumber
    |> isNumberState1 lineNumber
    |> isVariableState1 lineNumber
    |> hasFailed lineNumber


lexicalAnalyzerWord : Int -> List Token -> State -> List String -> (Result Error (List Token), State)
lexicalAnalyzerWord lineNumber tokenList state wordList =
  case wordList of
    word :: rest ->
      if word == "" then
        (Ok tokenList, state)
      else
        case getToken lineNumber state word of
          (Ok (Just token), newState) ->
            let state = increaseChBy word newState in
            lexicalAnalyzerWord lineNumber (token :: tokenList) state rest
          (Ok Nothing, newState) ->
            let state = increaseChBy word newState in
            lexicalAnalyzerWord lineNumber tokenList state rest
          (Err e, newState) ->
            (Err e, newState)
    [] ->
      (Ok tokenList, state)


lexicalAnalyzerLine : Int -> State -> String -> (Result Error (List Token), State)
lexicalAnalyzerLine lineNumber state line =
  if line == "" then
    (Ok [], state)
  else
    String.words line
    |> lexicalAnalyzerWord lineNumber [] state


lexicalAnalyzerByLine : List Token -> Int -> State -> List String -> Result Error (List Token, List String, Dict String (Int, Int), Dict String (Int, Int, Int), List String, List String)
lexicalAnalyzerByLine tokenList lineNumber state inputLines  =
  case inputLines of
    line :: rest ->
      case lexicalAnalyzerLine lineNumber state line of
        (Ok token, newState) ->
          let zeroedState = variableCreationOff newState in
          lexicalAnalyzerByLine (List.append token tokenList) (lineNumber + 1) zeroedState rest
        (Err e, _) ->
          Err e
    [] -> Ok (List.reverse tokenList, List.reverse state.variables, state.definedProcedures, state.definedFunctions, List.reverse state.definedProceduresUsage, List.reverse state.definedFunctionsUsage)


guessProcFuncNames : String -> State -> State
guessProcFuncNames input state =
  let regex_proc = Regex.regex "define\\s+([a-zA-Z][a-zA-Z0-9_]+)*\\s" in
  let regex_func = Regex.regex "function\\s+([a-zA-Z][a-zA-Z0-9_]+)*\\s" in
  let guessedProcedures =
    (Regex.find All regex_proc input)
    |> List.map .submatches
    |> List.map List.head
    |> List.map (Maybe.withDefault Nothing)
    |> List.map (Maybe.withDefault "")
  in
  let guessedFunctions =
    (Regex.find All regex_func input)
    |> List.map .submatches
    |> List.map List.head
    |> List.map (Maybe.withDefault Nothing)
    |> List.map (Maybe.withDefault "")
  in
  { state | guessedProcedures = guessedProcedures, guessedFunctions = guessedFunctions }

lexicalAnalyzer : String -> Dict String (String, Int) -> Dict String (String, Int, Int) -> Result Error (List Token, List String, Dict String (Int, Int), Dict String (Int, Int, Int), List String, List String)
lexicalAnalyzer input procNames functionNames =
  let state = guessProcFuncNames input (initState procNames functionNames) in
  String.lines input
  |> lexicalAnalyzerByLine [] 1 state
