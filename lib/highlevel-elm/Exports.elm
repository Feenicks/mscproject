port module Exports exposing (..)

import Types exposing (Token, ParseTree)
import Errors exposing (Error, printError, printErrorStyle, printGrammarHelp)
import Formatter exposing  (formatter, formatterBlocks)
import LexicalAnalyzer exposing (lexicalAnalyzer)
import Parser exposing (parser)
import Compiler exposing (compiler)
import Colourer exposing (colourer)

import Html exposing (..)
import Dict exposing (Dict)

{- StdLib -}
type alias Flags =
  { procedures      : List (String, (String, Int))      {- procedure name - Output - Nb Args -}
  , functions       : List (String, (String, Int, Int)) {- function name - Output - Nb In Args - Nb Out Values -}
  , proceduresUsage : List String                       {- procedure and arguments names -}
  , functionsUsage  : List String                       {- function and arguments names -}
  }

main =
  Html.programWithFlags
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }


-- MODEL

type alias Model =
  { procedures               : Dict String (String, Int)      {- dict of procedure names, code and number of arguments -}
  , functions                : Dict String (String, Int, Int) {- dict of function names, code, number of arguments and return values -}
  , definedProcedures        : Dict String (Int, Int)         {- dict of procedure names, number of arguments and local variables -}
  , definedFunctions         : Dict String (Int, Int, Int)    {- dict of function names, number of arguments, local variables and return values -}
  , proceduresUsage          : List String                    {- procedure and arguments names -}
  , functionsUsage           : List String                    {- function and arguments names -}
  , definedProceduresUsage   : List String                    {- procedure and arguments names -}
  , definedFunctionsUsage    : List String                    {- function and arguments names -}
  , code                     : String
  , line                     : Int                            {- cursor position line -}
  , column                   : Int                            {- cursor position column -}
  , formattingMode           : Int                            {- 0 off - 1 on -}
  , stackCode                : String                         {- stack code to compile -}
  , vmCode                   : String                         {- stack code for the VM -}
  , errors                   : Maybe Error
  , compiledOnce             : Bool
  }

init : Flags -> (Model, Cmd Msg)
init flags =
  (Model (Dict.fromList flags.procedures) (Dict.fromList flags.functions) Dict.empty Dict.empty flags.proceduresUsage flags.functionsUsage [] [] "" 0 0 1 "" "" Nothing False, Cmd.none)



-- UPDATE

type Msg
  = Compile (String, Int, Int, Int)
  | Result


-- port for sending the compiling result out to JavaScript
{-
  formatted code
  cursor position (line, column)
  (compiled stack code, stack code for the vm)
  Maybe (error message)
  error style
  grammar panel
  colouring for the code in Code Mirror
  procedure and function usages
-}
port result : (String, (Int, Int), (String, String), Maybe String, String, String, List (Int, Int, Int, Int, String), (List String, List String)) -> Cmd msg

{-| Add the number of returns values in the function usage displayed -}
addReturnValuesToFunctionUsage : Dict String (a, Int, Int) -> List String -> List String
addReturnValuesToFunctionUsage functions functionsUsage =
  let addSingle functionUsage =
    let functionName = List.head (String.split " " functionUsage) |> Maybe.withDefault "" in
    let (_, _, returnValue) =
      case Dict.get functionName functions of
        Just a -> a
        Nothing -> Debug.crash "Exports -- This never happens 1"
    in
    functionUsage ++ " -- returns " ++ (toString returnValue) ++ " values"
  in
  List.map addSingle functionsUsage

{-| Update the model -}
update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Compile (newCode, line, column, formattingMode) ->
      codeChange newCode line column formattingMode model
      |> formatCode
      |> compileCode
      |> update Result

    Result ->
      ( model, result ( model.code
                      , (model.line, model.column)
                      , (model.stackCode, model.vmCode)
                      , (Maybe.map (printError model.code) model.errors)
                      , printErrorStyle model.errors
                      , printGrammarHelp (List.append model.proceduresUsage model.definedProceduresUsage) (List.append
                                              (addReturnValuesToFunctionUsage model.functions model.functionsUsage)
                                              (addReturnValuesToFunctionUsage model.definedFunctions model.definedFunctionsUsage)) model.errors
                      , colourer model.code (List.concat [Dict.keys model.procedures, Dict.keys model.functions, Dict.keys model.definedProcedures, Dict.keys model.definedFunctions])
                      , (List.append model.proceduresUsage model.definedProceduresUsage, List.append
                                              (addReturnValuesToFunctionUsage model.functions model.functionsUsage)
                                              (addReturnValuesToFunctionUsage model.definedFunctions model.definedFunctionsUsage) )))

{-| Reinitialise the model on code modification received -}
codeChange : String -> Int -> Int -> Int -> Model -> Model
codeChange newCode line column formattingMode model  =
  { model | code = newCode, line = line, column = column, compiledOnce = False, formattingMode = formattingMode }

{-| Format the code using Regex rules -}
formatCode : Model -> Model
formatCode model =
  if model.formattingMode == 1 then
    let (code, line, column) = (formatter model.line model.column model.code) in
    { model | code = code, line = line, column = column}
  else
    model

{-| Format the code into blocks of what the compiler sees -}
formatBlocks : List (Int, Int) -> List (Int, Int) -> Maybe Int -> Model -> Model
formatBlocks endlines beglines endLine model =
  if model.formattingMode == 1 then
    let (code, line, column) = (formatterBlocks model.line model.column endlines beglines endLine model.code) in
    { model | code = code, line = line, column = column }
  else
    model


{-| Call for Lexical Analysis -}
compileCode : Model -> Model
compileCode model =
  case lexicalAnalyzer (model.code) model.procedures model.functions of
    Err e ->
      { model | errors = Just e }
    Ok (tokenList, variables, definedProcedures, definedFunctions, definedProceduresUsage, definedFunctionsUsage) ->
      { model | stackCode = (toString tokenList), errors = Nothing , definedProcedures = definedProcedures, definedFunctions = definedFunctions, definedProceduresUsage = definedProceduresUsage, definedFunctionsUsage = definedFunctionsUsage }
      |> tokenChange tokenList

{-| Call for the Parser -}
tokenChange : List Token -> Model -> Model
tokenChange tokenList model =
  {- Uses the compiledOnce trick because the code needs to be parser once to know what block formatting to used and
     then the code needs to be parsed with that formatting so that the line numbers are correct in the tokens -}
  case parser model.procedures model.functions model.definedProcedures model.definedFunctions tokenList of
    (Err e, _, parserEndlines, parserNewlines) ->
      let newModel =
        { model | errors = Just e }
        |> formatBlocks parserEndlines parserNewlines e.line
      in
        if newModel.compiledOnce then
          newModel
        else
          { newModel | compiledOnce = True }
          |> compileCode
    (Ok parseTree, definedFunctions, parserEndlines, parserNewlines) ->
      let newModel = { model | stackCode = (toString parseTree), errors = Nothing, definedFunctions = definedFunctions } in
      if newModel.compiledOnce then
        newModel
        |> parseTreeChange parseTree
      else
        { newModel | compiledOnce = True }
        |> formatBlocks parserEndlines parserNewlines Nothing
        |> compileCode

{-| Call the compiler on the parsed tree -}
parseTreeChange : ParseTree -> Model -> Model
parseTreeChange parseTree model =
  { model | stackCode = (compiler model.procedures model.functions model.definedProcedures model.definedFunctions False parseTree)
          , vmCode    = (compiler model.procedures model.functions model.definedProcedures model.definedFunctions True parseTree)}


-- SUBSCRIPTIONS

-- port for listening for suggestions from JavaScript
{-
  code
  cursor line
  cursor column
-}
port elmcompiler : ((String, Int, Int, Int) -> msg) -> Sub msg

subscriptions : Model -> Sub Msg
subscriptions model =
  elmcompiler Compile


-- VIEW

view : Model -> Html Msg
view model =
  div [] []
