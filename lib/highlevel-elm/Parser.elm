module Parser exposing (parser)

import Types exposing (TokenType(..), Token, BiNodeType(..), TriNodeType(..), ParseTree(..), ParsePosition(..))
import Errors exposing (Error, createParserError, updateParserLineNumber, addEOI, updateParserToken, updateParserPosition, replaceErrorNumber, printError)
import Dict exposing (Dict)

type alias State =
  { procedures        : Dict String (String, Int)          {- dict of procedure names, code and number of arguments -}
  , functions         : Dict String (String, Int, Int)     {- dict of function names, code, number of arguments and return values -}
  , definedProcedures : Dict String (Int, Int)             {- dict of procedure names, number of arguments and local variables -}
  , definedFunctions  : Dict String (Int, Int, Int)        {- dict of function names, number of arguments, local variables and return values (0 until seen) -}
  , lastToken         : (Int, Int)                         {- position of last token seen -}
  , endlines          : List (Int, Int)                    {- list of end of lines to insert in block formatting -}
  , beglines          : List (Int, Int)                    {- list of begininng of lines to insert in block formatting -}
  }

isType : TokenType -> Token -> Bool
isType token_type token =
  token.tokenType == token_type

isKeyword : String -> Token -> Bool
isKeyword keyword token =
  token.lexeme == keyword

readKeyword : String -> Int -> Int -> (State, List Token, ParseTree) -> Result Error (State, List Token, ParseTree)
readKeyword keyword missing_number wrong_number (state, token_list, parse_tree) =
  case token_list of
    [] ->
      emptyError state missing_number

    token :: end_list ->
      if (isType KEYWORD token) && (isKeyword keyword token) then
        Ok (state |> addTokenPosition token, end_list, parse_tree)

      else
        tokenError state token wrong_number

appendToList : List ParseTree -> (State, List Token, ParseTree) -> Result Error (State, List Token, List ParseTree)
appendToList tree_list (state, token_list, tree) =
  Ok (state, token_list, tree :: tree_list)

emptyError : State -> Int -> Result Error a
emptyError state number =
  Err (createParserError Nothing Nothing Nothing state.endlines state.beglines number)

tokenError : State -> Token -> Int -> Result Error a
tokenError state token number =
  Err (createParserError (Just token.line) Nothing (Just token) state.endlines state.beglines number)

{-| Add the token position as a newline to add to block formatting -}
addBegTokenPosition : Token -> State -> State
addBegTokenPosition token state =
  let beglines = (token.line, token.ch) :: state.beglines in
  let endlines = state.lastToken :: state.endlines in
  { state | beglines = beglines, endlines = endlines }

{-| Add the current token position as last seen -}
addTokenPosition : Token -> State -> State
addTokenPosition token state =
  { state | lastToken = (token.line, token.ch + token.length) }


parseStart : (State, List Token, (List ParseTree)) -> Result Error (State, List Token, (List ParseTree))
parseStart (state, token_list, tree_list) =
  (case token_list of
    [] ->
      Ok (state, token_list, List.reverse tree_list)
    _ ->
      Ok (state, token_list)
      |> Result.andThen parseStatement
      |> Result.andThen (appendToList tree_list)
      |> Result.andThen parseStart)
    |> updateParserPosition MAIN


parseStatement : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseStatement (state, token_list) =
  case token_list of
    [] ->
      emptyError state 201

    token :: end_list ->
      if isType CREATEVAR token then
        let new_state = state |> addTokenPosition token in
        Ok ( new_state, end_list, Leaf token)
        |> addEOI token.line

      else if (isType KEYWORD token) && (isKeyword "define" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        ( new_state, end_list )
        |> parseProcedureDefinition
        |> updateParserLineNumber token.line

      else if (isType KEYWORD token) && (isKeyword "function" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        ( new_state, end_list )
        |> parseFunctionDefinition
        |> updateParserLineNumber token.line

      else
        ( state, token_list )
        |> parseInsideStatement
        |> updateParserLineNumber token.line


parseInsideStatement : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseInsideStatement (state, token_list) =
  case token_list of
    [] ->
      emptyError state 202

    token :: end_list ->
      if isType VARIABLE token then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        ( new_state, [Leaf token], end_list)
        |> parseGlobalAssignment
        |> updateParserLineNumber token.line
        |> addEOI token.line

      else if (isType KEYWORD token) && (isKeyword "if" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        ( new_state, end_list )
        |> parseIfStatement
        |> updateParserLineNumber token.line
        |> addEOI token.line

      else if (isType KEYWORD token) && (isKeyword "while" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        ( new_state, end_list )
        |> parseWhileStatement
        |> updateParserLineNumber token.line
        |> addEOI token.line

      else if (isType PROCEDURE token) || (isType DEFINEDPROCEDURE token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        ( new_state, token_list )
        |> parseProcFunc
        |> updateParserLineNumber token.line
        |> addEOI token.line

      else if isType CREATEVAR token then
        tokenError state token 203

      else
        let new_state = { state | beglines = (token.line, token.ch) :: state.beglines } in
        tokenError new_state token 204


parseIfStatement : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseIfStatement (state, token_list) =
  (case (state, token_list) |> parseBooleanCondition |> Result.andThen (readKeyword "then" 219 220) of
    Err error ->
      Err error
    Ok (state, token_list, bool_tree) ->
      case (state, token_list) |> parseInsideStatement of
        Err error ->
          Err error
        Ok (state, token_list, stmtTree) ->
          (state, bool_tree, [stmtTree], token_list)
          |> parseElseStatement)
  |> updateParserPosition CONDITIONAL


parseElseStatement : (State, ParseTree, List ParseTree, List Token) -> Result Error (State, List Token, ParseTree)
parseElseStatement (state, bool_tree, if_stmt_tree_list, token_list) =
  case token_list of
    [] ->
      emptyError state 221

    token :: end_list ->
      if (isType KEYWORD token) && (isKeyword "else" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        case (new_state, end_list) |> parseInsideStatement of
          Err error ->
            Err error
          Ok (state, token_list, else_stmt_tree) ->
            (state, bool_tree, if_stmt_tree_list, [else_stmt_tree], token_list)
            |> parseEndIfStatement

      else if (isType KEYWORD token) && (isKeyword "endif" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        Ok (new_state, end_list, (BiNode(IFTHEN, bool_tree, UniNode (List.reverse if_stmt_tree_list))))

      else
        case (state, token_list) |> parseInsideStatement of
          Err error ->
            Err (replaceErrorNumber 204 221 error)
          Ok (state, token_list, stmt_tree) ->
            (state, bool_tree, (stmt_tree :: if_stmt_tree_list), token_list)
            |> parseElseStatement


parseEndIfStatement : (State, ParseTree, List ParseTree, List ParseTree, List Token) -> Result Error (State, List Token, ParseTree)
parseEndIfStatement (state, bool_tree, if_stmt_tree_list, else_stmt_tree_list, token_list) =
  case token_list of
    [] ->
      emptyError state 222

    token :: end_list ->
      if (isType KEYWORD token) && (isKeyword "endif" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        Ok (new_state, end_list, (TriNode(IFELSE, bool_tree, UniNode (List.reverse if_stmt_tree_list), UniNode (List.reverse else_stmt_tree_list))))

      else
        case (state, token_list) |> parseInsideStatement of
          Err error ->
            Err (replaceErrorNumber 204 222 error)
          Ok (state, token_list, stmt_tree) ->
            (state, bool_tree, if_stmt_tree_list, (stmt_tree :: else_stmt_tree_list), token_list)
            |> parseEndIfStatement


parseWhileStatement : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseWhileStatement (state, token_list) =
  (case (state, token_list) |> parseBooleanCondition |> Result.andThen (readKeyword "do" 223 224) of
    Err error ->
      Err error
    Ok (state, token_list, bool_tree) ->
      case (state, token_list) |> parseInsideStatement of
        Err error ->
          Err error
        Ok (state, token_list, stmtTree) ->
          (state, bool_tree, [stmtTree], token_list)
          |> parseEndWhileStatement)
  |> updateParserPosition LOOP


parseEndWhileStatement : (State, ParseTree, List ParseTree, List Token) -> Result Error (State, List Token, ParseTree)
parseEndWhileStatement (state, bool_tree, stmt_tree_list, token_list) =
  case token_list of
    [] ->
      emptyError state 225

    token :: end_list ->
      if (isType KEYWORD token) && (isKeyword "endwhile" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        Ok (new_state, end_list, BiNode(WHILE, bool_tree, UniNode (List.reverse stmt_tree_list)))

      else
        case (state, token_list) |> parseInsideStatement of
          Err error ->
            Err (replaceErrorNumber 204 225 error)
          Ok (state, token_list, stmt_tree) ->
            (state, bool_tree, (stmt_tree :: stmt_tree_list), token_list)
            |> parseEndWhileStatement


parseBooleanCondition : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseBooleanCondition (state, token_list) =
  case token_list of
    [] ->
      emptyError state 216

    _ ->
      case (state, token_list) |> parseFactor of
        Err error ->
          Err (replaceErrorNumber 213 216 error)
        Ok (state, token_list, factor_tree_1) ->
          case token_list of
            [] ->
              emptyError state 217

            operator :: end_list ->
              let operator_type =
                if (isType KEYWORD operator) && (isKeyword "<" operator) then
                  Ok Types.LT
                else if (isType KEYWORD operator) && (isKeyword "<=" operator) then
                  Ok Types.LE
                else if (isType KEYWORD operator) && (isKeyword ">" operator) then
                  Ok Types.GT
                else if (isType KEYWORD operator) && (isKeyword ">=" operator) then
                  Ok Types.GE
                else if (isType KEYWORD operator) && (isKeyword "==" operator) then
                  Ok Types.EQ
                else if (isType KEYWORD operator) && (isKeyword "!=" operator) then
                  Ok Types.NE
                else
                  tokenError state operator 218
              in
              case operator_type of
                Err error ->
                  Err error
                Ok type_operator ->

                  let new_state = state |> addTokenPosition operator in
                  case (new_state, end_list) |> parseFactor of
                    Err error ->
                      Err (replaceErrorNumber 213 216 error)
                    Ok (state, token_list, factor_tree_2) ->
                      Ok (state, token_list, (BiNode(type_operator, factor_tree_1, factor_tree_2)))


parseExpression : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseExpression (state, token_list) =
  (state, token_list)
  |> parseTerm
  |> Result.andThen parseSubExpression


parseSubExpression : (State, List Token, ParseTree) -> Result Error (State, List Token, ParseTree)
parseSubExpression (state, token_list, parse_tree) =
  case token_list of
    [] ->
      Ok (state, token_list, parse_tree)

    operation :: end_list ->
      let operation_type =
        if (isType KEYWORD operation) && (isKeyword "+" operation) then
          Ok ADDITION
        else if (isType KEYWORD operation) && (isKeyword "-" operation) then
          Ok SUBSTRACTION
        else
          Err ""
      in
      case operation_type of
        Err _ ->
          Ok (state, token_list, parse_tree)
        Ok type_operation ->

          let new_state = state |> addTokenPosition operation in
          case (new_state, end_list) |> parseTerm of
            Err error ->
              Err error
            Ok (state, token_list, term_tree) ->
              (state, token_list, (BiNode(type_operation, parse_tree, term_tree)))
              |> parseSubExpression


parseTerm : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseTerm (state, token_list) =
  (state, token_list)
  |> parseFactor
  |> Result.andThen parseSubTerm


parseSubTerm : (State, List Token, ParseTree) -> Result Error (State, List Token, ParseTree)
parseSubTerm (state, token_list, parse_tree) =
  case token_list of
    [] ->
      Ok (state, token_list, parse_tree)

    operation :: end_list ->
      let operation_type =
        if (isType KEYWORD operation) && (isKeyword "*" operation) then
          Ok MULTIPLICATION
        else if (isType KEYWORD operation) && (isKeyword "/" operation) then
          Ok DIVISION
        else
          Err ""
      in
      case operation_type of
        Err _ ->
          Ok (state, token_list, parse_tree)
        Ok type_operation ->

          let new_state = state |> addTokenPosition operation in
          case (new_state, end_list) |> parseFactor of
            Err error ->
              Err error
            Ok (state, token_list, factor_tree) ->
              (state, token_list, (BiNode(type_operation, parse_tree, factor_tree)))
              |> parseSubExpression


parseFactor : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseFactor (state, token_list) =
  case token_list of
    [] ->
      emptyError state 214

    token :: end_list ->
      let new_state = state |> addTokenPosition token in
      if (isType VARIABLE token) || (isType LOCALVARIABLE token) || (isType ARGUMENT token) || (isType NUMBER token)  then
        Ok (new_state, end_list, (Leaf token))

      else if (isType KEYWORD token) && (isKeyword "(" token) then
          case (new_state, end_list) |> parseExpression |> updateParserLineNumber token.line of
            Err error ->
              Err error
            Ok (state, token_list, expression_tree) ->

              case token_list of
                [] ->
                  Err (createParserError Nothing Nothing (Just token) state.endlines state.beglines 212)
                token2 :: end_list ->

                  if (isType KEYWORD token2) && (isKeyword ")" token2) then
                    Ok (state |> addTokenPosition token2, end_list, expression_tree)
                  else
                    Err (createParserError (Just token2.line) (Just token2.lexeme) (Just token) state.endlines state.beglines 236)

      else
        emptyError state 214

parseFullExpression : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseFullExpression (state, token_list) =
  case token_list of
    [] ->
      emptyError state 215

    token :: _ ->
      if (isType FUNCTION token) || (isType DEFINEDFUNCTION token) then
        (state, token_list) |> parseProcFunc |> updateParserLineNumber token.line
      else
        (state, token_list) |> parseExpression |> updateParserLineNumber token.line


parseGlobalAssignment : (State, List ParseTree, List Token) -> Result Error (State, List Token, ParseTree)
parseGlobalAssignment (state, variable_trees, token_list) =
  case token_list of
    [] ->
      emptyError state 210

    token :: end_list ->
      if (isType VARIABLE token) then
        (state |> addTokenPosition token, (Leaf token) :: variable_trees, end_list)
        |> parseGlobalAssignment
        |> updateParserLineNumber token.line

      else if (isType KEYWORD token) && (isKeyword "=" token) then
        (state |> addTokenPosition token, variable_trees, end_list)
        |> parseAssignment
        |> updateParserLineNumber token.line

      else
        tokenError state token 207


parseLocalAssignment : (State, List ParseTree, List Token) -> Result Error (State, List Token, ParseTree)
parseLocalAssignment (state, variable_trees, token_list) =
  case token_list of
    [] ->
      emptyError state 210

    token :: end_list ->
      if (isType LOCALVARIABLE token) then
        (state |> addTokenPosition token, (Leaf token) :: variable_trees, end_list)
        |> parseLocalAssignment
        |> updateParserLineNumber token.line

      else if (isType KEYWORD token) && (isKeyword "=" token) then
        (state |> addTokenPosition token, variable_trees, end_list)
        |> parseAssignment
        |> updateParserLineNumber token.line

      else if (isType VARIABLE token) then
        tokenError state token 211

      else
        tokenError state token 207


parseAssignment : (State, List ParseTree, List Token) -> Result Error (State, List Token, ParseTree)
parseAssignment (state, variable_trees, token_list) =
  case (state, token_list) |> parseFullExpression of
    Err error ->
      Err error
    Ok (state, token_list, tree) ->
      case tree of
        TriNode(FUNC, _, _, _) ->
          Ok (state, token_list, (BiNode(PENDING_ASSIGN, UniNode (List.reverse variable_trees), tree)))
        other ->
            if List.length variable_trees > 1 then
              emptyError state 209
            else
              Ok (state, token_list, (BiNode(ASSIGN, UniNode (List.reverse variable_trees), tree)))


parseProcFunc : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseProcFunc (state, token_list) =
  case token_list of
    [] ->
      emptyError state 995
    token :: end_list ->
      let makeNode1 (new_state, new_list, argument_tree) =
        (new_state, new_list, (TriNode(PROC, Asm(token.lexeme), Asm(token.lexeme ++ " call"), argument_tree)))
      in
      let makeNode2 (new_state, new_list, argument_tree) =
        (new_state, new_list, (TriNode(FUNC, Asm(token.lexeme), Asm(token.lexeme ++ " call"), argument_tree)))
      in
      case Dict.get token.lexeme state.procedures of
        Just (_, number_arguments) ->
          (state |> addTokenPosition token, number_arguments, [], end_list)
          |> parseArguments
          |> updateParserLineNumber token.line
          |> updateParserToken token
          |> Result.map makeNode1
        Nothing ->
          case Dict.get token.lexeme state.functions of
            Just (_, number_arguments, _) ->
              (state |> addTokenPosition token, number_arguments, [], end_list)
              |> parseArguments
              |> updateParserLineNumber token.line
              |> updateParserToken token
              |> Result.map makeNode2
            Nothing ->
              case Dict.get token.lexeme state.definedProcedures of
                Just (number_arguments, _) ->
                  (state |> addTokenPosition token, number_arguments, [], end_list)
                  |> parseArguments
                  |> updateParserLineNumber token.line
                  |> updateParserToken token
                  |> Result.map makeNode1
                Nothing ->
                  case Dict.get token.lexeme state.definedFunctions of
                    Just (number_arguments, _, _) ->
                      (state |> addTokenPosition token, number_arguments, [], end_list)
                      |> parseArguments
                      |> updateParserLineNumber token.line
                      |> updateParserToken token
                      |> Result.map makeNode2
                    Nothing ->
                      tokenError state token 226


parseArguments : (State, Int, List ParseTree, List Token) -> Result Error (State, List Token, ParseTree)
parseArguments (state, number_arguments, arguments_read, token_list) =
  if number_arguments == 0 then
    Ok (state, token_list, (UniNode (List.reverse arguments_read)))
  else
    case token_list of
      [] ->
        Err (createParserError Nothing (Just (toString number_arguments)) Nothing state.endlines state.beglines 227)
      token :: _ ->
        case (state, token_list) |> parseFactor |> updateParserLineNumber token.line of
          Err error ->
            Err error
          Ok (state, token_list, argument_tree) ->
            (state, number_arguments - 1, argument_tree :: arguments_read, token_list)
            |> parseArguments
            |> updateParserLineNumber token.line


parseProcedureDefinition : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseProcedureDefinition (state, token_list) =
  (case token_list of
    [] ->
      emptyError state 228

    token :: end_list ->
      if (isType NAME token) then
        let procedure_name = token.lexeme in
        case readKeyword "as" 229 230 (state, end_list, Asm "") of
          Err error ->
            Err error
          Ok (new_state, new_list, _) ->
            let state = new_state |> addTokenPosition token in
            (state, procedure_name, [], new_list)
            |> parseProcedureStatement
            |> updateParserLineNumber token.line
      else
        tokenError state token 994
  )
  |> updateParserPosition INPROC


parseProcedureStatement : (State, String, List ParseTree, List Token) -> Result Error (State, List Token, ParseTree)
parseProcedureStatement (state, procedure_name, statement_trees, token_list) =
  case token_list of
    [] ->
      emptyError state 231

    token :: end_list ->
      if (isType KEYWORD token) && (isKeyword "enddefine" token) then
        let (number_arguments, number_local_variables) = Maybe.withDefault (0,0) (Dict.get procedure_name state.definedProcedures) in
        let drop_arguments = String.repeat (number_arguments + number_local_variables) "DROP " in
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        Ok (new_state, end_list, (BiNode(PROCDEF, UniNode(List.reverse (Asm(drop_arguments ++ "ret") :: statement_trees)), Asm(procedure_name))) )

      else
        case (state, token_list) |> parseProcFuncStatement |> updateParserLineNumber token.line of
          Err error ->
            Err (replaceErrorNumber 204 231 error)
          Ok (state, token_list, statement_tree) ->
            (state, procedure_name, statement_tree :: statement_trees, token_list)
            |> parseProcedureStatement
            |> updateParserLineNumber token.line


parseFunctionDefinition : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseFunctionDefinition (state, token_list) =
  (case token_list of
    [] ->
      emptyError state 232

    token :: end_list ->
      if (isType NAME token) then
        let function_name = token.lexeme in
        case readKeyword "as" 229 230 (state, end_list, Asm "") of
          Err error ->
            Err error
          Ok (new_state, new_list, _) ->
            let state = new_state |> addTokenPosition token in
            (state, function_name, [], new_list)
            |> parseFunctionStatement
            |> updateParserLineNumber token.line
      else
        tokenError state token 993
  )
  |> updateParserPosition INFUNC


parseFunctionStatement : (State, String, List ParseTree, List Token) -> Result Error (State, List Token, ParseTree)
parseFunctionStatement (state, function_name, statement_trees, token_list) =
  case token_list of
    [] ->
      emptyError state 233

    token :: end_list ->
      if (isType KEYWORD token) && (isKeyword "return" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        (new_state, function_name, statement_trees, [], end_list)
        |> parseReturnStatement
        |> updateParserLineNumber token.line
      else
        case (state, token_list) |> parseProcFuncStatement |> updateParserLineNumber token.line of
          Err error ->
            Err (replaceErrorNumber 204 233 error)
          Ok (state, token_list, statement_tree) ->
            (state, function_name, statement_tree :: statement_trees, token_list)
            |> parseFunctionStatement
            |> updateParserLineNumber token.line


parseReturnStatement : (State, String, List ParseTree, List ParseTree, List Token) -> Result Error (State, List Token, ParseTree)
parseReturnStatement (state, function_name, statement_trees, return_trees, token_list) =
  case token_list of
    [] ->
      if List.isEmpty return_trees then
        emptyError state 239
      else
        emptyError state 234

    token :: end_list ->
      if (isType KEYWORD token) && (isKeyword "endfunction" token) then
        if return_trees == [] then
          tokenError state token 238
        else
          let (number_arguments, number_local_variables, _) = Maybe.withDefault (0,0,0) (Dict.get function_name state.definedFunctions) in
          let number_return_values = List.length return_trees in
          let newDefinedFunctions = Dict.remove function_name state.definedFunctions in
          let definedFunctions = Dict.insert function_name (number_arguments, number_local_variables, number_return_values) newDefinedFunctions in
          let dropArgs = String.repeat (number_arguments + number_local_variables) ((toString (number_return_values + 1)) ++ " NROT DROP ") in
          let statement_trees_2 = List.append return_trees statement_trees in
          let new_state = { state | definedFunctions = definedFunctions } |> addBegTokenPosition token |> addTokenPosition token in
          Ok ( new_state, end_list, (BiNode(FUNCDEF, UniNode(List.reverse (Asm(dropArgs ++ "ret") :: statement_trees_2)), Asm(function_name))) )

      else
        case (state, token_list) |> parseFactor |> updateParserLineNumber token.line of
          Err error ->
            Err (replaceErrorNumber 204 234 error)
          Ok (state, token_list, return_tree) ->
            (state, function_name, statement_trees, return_tree :: return_trees, token_list)
            |> parseReturnStatement
            |> updateParserLineNumber token.line



parseProcFuncStatement : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseProcFuncStatement (state, token_list) =
  case token_list of
    [] ->
      emptyError state 201

    token :: end_list ->
      if isType CREATELOCALVAR token then
        let new_state = state |> addTokenPosition token in
        Ok ( new_state, end_list, Leaf token)
        |> addEOI token.line

      else
        ( state, token_list )
        |> parseInsideProcFuncStatement
        |> updateParserLineNumber token.line


parseInsideProcFuncStatement : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseInsideProcFuncStatement (state, token_list) =
  case token_list of
    [] ->
      emptyError state 202

    token :: end_list ->
      if isType LOCALVARIABLE token then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        ( new_state, [Leaf token], end_list)
        |> parseLocalAssignment
        |> updateParserLineNumber token.line
        |> addEOI token.line

      else if isType VARIABLE token then
        tokenError state token 206

      else if (isType KEYWORD token) && (isKeyword "if" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        ( new_state, end_list )
        |> parseIfProcFuncStatement
        |> updateParserLineNumber token.line
        |> addEOI token.line

      else if (isType KEYWORD token) && (isKeyword "while" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        ( new_state, end_list )
        |> parseWhileProcFuncStatement
        |> updateParserLineNumber token.line
        |> addEOI token.line

      else if (isType PROCEDURE token) || (isType DEFINEDPROCEDURE token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        ( new_state, token_list )
        |> parseProcFunc
        |> updateParserLineNumber token.line
        |> addEOI token.line

      else if isType CREATEVAR token then
        tokenError state token 203

      else
        let new_state = { state | beglines = (token.line, token.ch) :: state.beglines } in
        tokenError new_state token 204


parseIfProcFuncStatement : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseIfProcFuncStatement (state, token_list) =
  (case (state, token_list) |> parseBooleanCondition |> Result.andThen (readKeyword "then" 219 220) of
    Err error ->
      Err error
    Ok (state, token_list, bool_tree) ->
      case (state, token_list) |> parseInsideProcFuncStatement of
        Err error ->
          Err error
        Ok (state, token_list, stmtTree) ->
          (state, bool_tree, [stmtTree], token_list)
          |> parseElseProcFuncStatement)
  |> updateParserPosition CONDITIONALINPROCFUNC


parseElseProcFuncStatement : (State, ParseTree, List ParseTree, List Token) -> Result Error (State, List Token, ParseTree)
parseElseProcFuncStatement (state, bool_tree, if_stmt_tree_list, token_list) =
  case token_list of
    [] ->
      emptyError state 221

    token :: end_list ->
      if (isType KEYWORD token) && (isKeyword "else" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        case (new_state, end_list) |> parseInsideProcFuncStatement of
          Err error ->
            Err error
          Ok (state, token_list, else_stmt_tree) ->
            (state, bool_tree, if_stmt_tree_list, [else_stmt_tree], token_list)
            |> parseEndIfProcFuncStatement

      else if (isType KEYWORD token) && (isKeyword "endif" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        Ok (new_state, end_list, (BiNode(IFTHEN, bool_tree, UniNode (List.reverse if_stmt_tree_list))))

      else
        case (state, token_list) |> parseInsideProcFuncStatement of
          Err error ->
            Err (replaceErrorNumber 204 221 error)
          Ok (state, token_list, stmt_tree) ->
            (state, bool_tree, (stmt_tree :: if_stmt_tree_list), token_list)
            |> parseElseProcFuncStatement


parseEndIfProcFuncStatement : (State, ParseTree, List ParseTree, List ParseTree, List Token) -> Result Error (State, List Token, ParseTree)
parseEndIfProcFuncStatement (state, bool_tree, if_stmt_tree_list, else_stmt_tree_list, token_list) =
  case token_list of
    [] ->
      emptyError state 222

    token :: end_list ->
      if (isType KEYWORD token) && (isKeyword "endif" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        Ok (new_state, end_list, (TriNode(IFELSE, bool_tree, UniNode (List.reverse if_stmt_tree_list), UniNode (List.reverse else_stmt_tree_list))))

      else
        case (state, token_list) |> parseInsideProcFuncStatement of
          Err error ->
            Err (replaceErrorNumber 204 222 error)
          Ok (state, token_list, stmt_tree) ->
            (state, bool_tree, if_stmt_tree_list, (stmt_tree :: else_stmt_tree_list), token_list)
            |> parseEndIfProcFuncStatement


parseWhileProcFuncStatement : (State, List Token) -> Result Error (State, List Token, ParseTree)
parseWhileProcFuncStatement (state, token_list) =
  (case (state, token_list) |> parseBooleanCondition |> Result.andThen (readKeyword "do" 223 224) of
    Err error ->
      Err error
    Ok (state, token_list, bool_tree) ->
      case (state, token_list) |> parseInsideProcFuncStatement of
        Err error ->
          Err error
        Ok (state, token_list, stmtTree) ->
          (state, bool_tree, [stmtTree], token_list)
          |> parseEndWhileProcFuncStatement)
  |> updateParserPosition LOOPINPROCFUNC


parseEndWhileProcFuncStatement : (State, ParseTree, List ParseTree, List Token) -> Result Error (State, List Token, ParseTree)
parseEndWhileProcFuncStatement (state, bool_tree, stmt_tree_list, token_list) =
  case token_list of
    [] ->
      emptyError state 225

    token :: end_list ->
      if (isType KEYWORD token) && (isKeyword "endwhile" token) then
        let new_state = state |> addBegTokenPosition token |> addTokenPosition token in
        Ok (new_state, end_list, BiNode(WHILE, bool_tree, UniNode (List.reverse stmt_tree_list)))

      else
        case (state, token_list) |> parseInsideProcFuncStatement of
          Err error ->
            Err (replaceErrorNumber 204 225 error)
          Ok (state, token_list, stmt_tree) ->
            (state, bool_tree, (stmt_tree :: stmt_tree_list), token_list)
            |> parseEndWhileProcFuncStatement


{- In case of assignments where the right-hand side is a function, check that
   the number of return values is the same as the number of variables assigned -}
checkNumberOfVariables : State -> ParseTree -> Result Error ParseTree
checkNumberOfVariables state tree =
  case tree of
    BiNode(PENDING_ASSIGN, UniNode (variables), TriNode(FUNC, Asm(funcname), func, args)) ->
      case Dict.get funcname state.functions of
        Just (_, _, nbReturn) ->
          let nbVariables = List.length variables in
          if nbReturn /= nbVariables then
            Err (createParserError Nothing (Just (funcname ++ " " ++ (toString nbReturn) ++ " " ++ (toString nbVariables))) Nothing state.endlines state.beglines 208)
          else
            Ok (BiNode(ASSIGN, UniNode (variables), TriNode(FUNC, Asm(funcname), func, args)))
        Nothing ->
          case Dict.get funcname state.definedFunctions of
            Just (_, _, nbReturn) ->
              let nbVariables = List.length variables in
              if nbReturn /= nbVariables then
                Err (createParserError Nothing (Just (funcname ++ " " ++ (toString nbReturn) ++ " " ++ (toString nbVariables))) Nothing state.endlines state.beglines 208)
              else
                Ok (BiNode(ASSIGN, UniNode (variables), TriNode(FUNC, Asm(funcname), func, args)))
            Nothing ->
                Err (createParserError Nothing Nothing Nothing state.endlines state.beglines 999)
    BiNode(nodetype, leftTree, rightTree) ->
      case (checkNumberOfVariables state leftTree, checkNumberOfVariables state rightTree) of
        (Err e, _) -> Err e
        (Ok _, Err e) -> Err e
        (Ok leftTree, Ok rightTree) -> Ok (BiNode(nodetype, leftTree, rightTree))
    UniNode(listTree) ->
      let aux tree acc =
        case acc of
          Err e -> Err e
          Ok list ->
          case checkNumberOfVariables state tree of
            Err e -> Err e
            Ok tree -> Ok (tree :: list)
      in
      case (List.foldr aux (Ok []) listTree) of
        Err e -> Err e
        Ok listTree -> Ok (UniNode(listTree))

    TriNode(nodetype, leftTree, middleTree, rightTree) ->
      case (checkNumberOfVariables state leftTree, checkNumberOfVariables state middleTree, checkNumberOfVariables state rightTree) of
        (Err e, _, _) -> Err e
        (_, Err e, _) -> Err e
        (_, _, Err e) -> Err e
        (Ok leftTree, Ok middleTree, Ok rightTree) -> Ok (TriNode(nodetype, leftTree, middleTree, rightTree))
    other ->
      Ok tree


parse : State -> List Token -> (Result Error ParseTree, Dict String (Int, Int, Int), List (Int, Int), List (Int, Int))
parse state token_list =
  case parseStart (state, token_list, [])  of
    Err error ->
      (Err error, state.definedFunctions, Maybe.withDefault [] error.endlines, Maybe.withDefault [] error.beglines)
    Ok (newState, restList, parseTree) ->
      case restList of
        _ :: _ ->
          (Err (createParserError Nothing Nothing Nothing newState.endlines newState.beglines 992), state.definedFunctions, newState.endlines, newState.beglines)
        [] ->
          let verifiedParseTree = checkNumberOfVariables newState (UniNode parseTree) in
          (verifiedParseTree, newState.definedFunctions, newState.endlines, newState.beglines)



parser : Dict String (String, Int) -> Dict String (String, Int, Int) -> Dict String (Int, Int) -> Dict String (Int, Int, Int) -> List Token -> (Result Error ParseTree, Dict String (Int, Int, Int), List (Int, Int), List (Int, Int))
parser procedures functions definedProcedures definedFunctions token_list =
  parse { procedures = procedures, functions = functions, definedProcedures = definedProcedures, definedFunctions = definedFunctions, lastToken = (0,0), endlines = [], beglines = [] } token_list
