module Types exposing (TokenType(..), Token, nullToken, BiNodeType(..), TriNodeType(..), ParseTree(..), ParsePosition(..))

type TokenType = KEYWORD | PROCEDURE | FUNCTION | DEFINEDPROCEDURE | DEFINEDFUNCTION | CREATEVAR | CREATELOCALVAR | VARIABLE | LOCALVARIABLE | ARGUMENT | NAME | NUMBER

type alias Token =
  { tokenType     : TokenType
  , lexeme        : String
  , line          : Int
  , ch            : Int
  , length        : Int
  , stackPosition : Int {- Real stack position for Variables // Negative stack position for Arguments in the call frame -}
  }

nullToken = Token KEYWORD "" 0 0 0 0

type BiNodeType = PENDING_ASSIGN | ASSIGN | ADDITION | SUBSTRACTION | MULTIPLICATION | DIVISION | LT | LE | GT | GE | EQ | NE | IFTHEN | WHILE | PROCDEF | FUNCDEF
type TriNodeType = IFELSE | PROC | FUNC
type ParseTree = Asm String | Leaf Token | EOI Int | UniNode (List ParseTree) | BiNode (BiNodeType, ParseTree, ParseTree) | TriNode (TriNodeType, ParseTree, ParseTree, ParseTree)

type ParsePosition = MAIN | CONDITIONAL | LOOP | INPROC | INFUNC | CONDITIONALINPROCFUNC | LOOPINPROCFUNC
