module Colourer exposing (colourer)

import LexicalAnalyzer exposing (keywords)
import Regex exposing (regex, HowMany(..), find, Match)

notemacros = ["C1", "C1#", "D1", "D1#", "E1", "F1", "F1#", "G1", "G1#", "A1", "A1#", "B1", "C2", "C2#", "D2", "D2#", "E2", "F2", "F2#", "G2", "G2#", "A2", "A2#", "B2", "C3", "C3#", "D3", "D3#", "E3", "F3", "F3#", "G3", "G3#", "A3", "A3#", "B3", "C4", "C4#", "D4", "D4#", "E4", "F4", "F4#", "G4", "G4#", "A4", "A4#", "B4", "C5", "C5#", "D5", "D5#", "E5", "F5", "F5#", "G5", "G5#", "A5", "A5#", "B5", "C6"]
colourmacros = ["black", "blue", "green", "cyan", "red", "magenta", "yellow", "white"]

{-| Add colour for one word occurence in a line -}
addColour : Int -> String -> Match -> List (Int, Int, Int, Int, String) -> List (Int, Int, Int, Int, String)
addColour lineNumber class match colouring =
  let ch = .index match in
  let word = .match match in
  (lineNumber, ch, lineNumber, (ch + String.length word), class) :: colouring

{-| Add colour for all word occurences in a line -}
addColours : Int -> String -> String -> String -> List (Int, Int, Int, Int, String) -> List (Int, Int, Int, Int, String)
addColours lineNumber line class word colouring =
  if List.member word ["(", ")", "+", "-", "*", "/"] then
    let matches = find All (regex ("(^|\\n| )\\" ++ word ++ "($|\\n| )")) line in
    List.foldr (addColour lineNumber class) colouring matches
  else
    let matches = find All (regex ("(^|\\n| )" ++ word ++ "($|\\n| )")) line in
    List.foldr (addColour lineNumber class) colouring matches


{-| Add colour for all keywords occurences in a line -}
colourerKeywords : Int -> String -> List (Int, Int, Int, Int, String) -> List (Int, Int, Int, Int, String)
colourerKeywords lineNumber line pastColouring =
  List.foldr (addColours lineNumber line "cm-my-keyword") pastColouring keywords

{-| Add colour for all procfuncname occurences in a line -}
colourerProcFunc : List String -> Int -> String -> List (Int, Int, Int, Int, String) -> List (Int, Int, Int, Int, String)
colourerProcFunc procfuncnames lineNumber line pastColouring =
  List.foldr (addColours lineNumber line "cm-procfunc") pastColouring procfuncnames

{-| Add colour for all note macros occurences in a line -}
colourerMacro : Int -> String -> List (Int, Int, Int, Int, String) -> List (Int, Int, Int, Int, String)
colourerMacro lineNumber line pastColouring =
  List.foldr (addColours lineNumber line "cm-macro") pastColouring (List.append notemacros colourmacros)

indexedFold : Int -> List String -> ( Int -> String -> List (Int, Int, Int, Int, String) -> List (Int, Int, Int, Int, String) ) -> List (Int, Int, Int, Int, String) -> List (Int, Int, Int, Int, String)
indexedFold lineNumber lines func pastColouring =
  case lines of
    [] -> pastColouring
    hd :: rest ->
      func lineNumber hd pastColouring
      |> indexedFold (lineNumber + 1) rest func

colourer : String -> List String -> List (Int, Int, Int, Int, String)
colourer code procfuncnames =
  let lines = code |> String.lines in
  []
  |> indexedFold 0 lines (colourerKeywords)
  |> indexedFold 0 lines (colourerProcFunc procfuncnames)
  |> indexedFold 0 lines (colourerMacro)
