module Formatter exposing (formatter, formatterBlocks)

import Regex exposing (HowMany(..), regex, replace)

{-| Inserting marker in cursor place before the formatting -}
addCursor : Int -> Int -> String -> Result String String
addCursor cursor_line cursor_column code =
  if String.contains "$" code then
    Err code
  else
    let insertCursor line_number current_line =
      if line_number == cursor_line then
        String.dropRight (String.length current_line - cursor_column) current_line
        ++ "$"
        ++ (String.dropLeft cursor_column current_line)
      else
        current_line
    in
    String.lines code
    |> List.indexedMap insertCursor
    |> String.join "\n"
    |> Ok

{-| Removing the cursor and retrieving its position -}
removeCursor : String -> (String, Int, Int)
removeCursor code =
  let retrieveCursor line_number line =
    if String.contains "$" line then
      let column = Maybe.withDefault 0 (List.head (String.indices "$" line)) in
      let new_line =
        String.dropRight (String.length line - column) line
        ++ String.dropLeft (column + 1) line
      in
      (new_line, (line_number, column))
    else
      (line, (-1,-1))
  in
  let pass = String.lines code |> List.indexedMap retrieveCursor in
  let new_code = List.map Tuple.first pass |> String.join "\n" in
  let (cursor_line, cursor_column) =
    pass
    |> List.map Tuple.second
    |> List.filter ((/=) (-1,-1))
    |> List.head
    |> Maybe.withDefault (0,0)
  in
  (new_code, cursor_line, cursor_column)

{-| Add newline before variable -}
addNewlineVariable : String -> String
addNewlineVariable line =
  let reg_variable_1 = regex " variable " in
  let reg_variable_2 = regex " variable\\n" in
  line
  |> replace All reg_variable_1 (\_ -> "\nvariable ")
  |> replace All reg_variable_2 (\_ -> "\nvariable\n")

{-| Add spaces around the operators except '-' to allow negative numbers -}
addOperatorSpaces : String -> String
addOperatorSpaces code =
  let addLeft operator expr =
    replace All (regex expr) (\{match} -> (String.join (" " ++ operator) (String.split operator match)))
  in
  let addRight operator expr =
    replace All (regex expr) (\{match} -> (String.join (operator ++ " ") (String.split operator match)))
  in
  code
  |> addLeft "(" "[^ |\\$]\\("
  |> addLeft ")" "[^ |\\$]\\)"
  |> addLeft "<=" "[^ |\\$]<="
  |> addLeft ">=" "[^ |\\$]>="
  |> addLeft "==" "[^ |\\$]=="
  |> addLeft "!=" "[^ |\\$]!="
  |> addLeft "=" "[^ |<|>|=|!|\\$]="
  |> addLeft "<" "[^ |\\$]<[^=|\\$]"
  |> addLeft ">" "[^ |\\$]>[^=|\\$]"
  |> addLeft "+" "[^ |\\$]\\+"
  |> addLeft "-" "[^ |\\$]\\-"
  |> addLeft "*" "[^ |\\$]\\*"
  |> addLeft "/" "[^ |\\$]/"
  |> addRight "(" "\\([^ |\\$]"
  |> addRight ")" "\\)[^ |\\$]"
  |> addRight "<=" "<=[^ |\\$]"
  |> addRight ">=" ">=[^ |\\$]"
  |> addRight "==" "==[^ |\\$]"
  |> addRight "!=" "!=[^ |\\$]"
  |> addRight "=" "[^<|>|!|=]=[^ |=|\\$]"
  |> addRight "<" "<[^ |=|\\$]"
  |> addRight ">" ">[^ |=|\\$]"
  |> addRight "+" "\\+[^ |\\$]"
  |> addRight "*" "\\*[^ |\\$]"
  |> addRight "/" "/[^ |\\$]"

{-  let reg_1 = regex "\\(|\\)|<=|>=|==|!=" in
  let reg_2 = regex "([^<|>|=|!|$])=([^=|$])" in
  let reg_3 = regex "<([^=|$])" in
  let reg_4 = regex ">([^=|$])" in
  let reg_5 = regex "\\+|\\*|/" in
  let reg_6 = regex "\\-" in
  code
  |> replace All reg_1 (\{match} -> " " ++ match ++ " ")
  |> replace All reg_2 (\{match} -> (String.join " = " (String.split "=" match)))
  |> replace All reg_3 (\{match} -> (String.join " < " (String.split "<" match)))
  |> replace All reg_4 (\{match} -> (String.join " > " (String.split ">" match)))
  |> replace All reg_5 (\{match} -> " " ++ match ++ " ")
  |> replace All reg_6 (\{match} -> " " ++ match) -}

{-| Add block-like indentation due to p rocedure/function definitions, conditionals or loops -}
addIndentation : String -> String
addIndentation code =
  let lines = String.lines code in
  let indent lines pastLines indentCount =
    case lines of
      realLine :: nextLines ->
        if Regex.contains (regex "(^if)|(^while)|(^define)|(^function)") realLine then
          indent nextLines (((String.repeat indentCount " ") ++ realLine) :: pastLines) (indentCount + 2)
        else if Regex.contains (regex "^else") realLine then
          indent nextLines (((String.repeat (indentCount - 2) " ") ++ realLine) :: pastLines) indentCount
        else if Regex.contains (regex "(^endif)|(^endwhile)|(^enddefine)|(^endfunction)") realLine then
          indent nextLines (((String.repeat (indentCount - 2) " ") ++ realLine) :: pastLines) (indentCount - 2)
        else
          indent nextLines (((String.repeat indentCount " ") ++ realLine) :: pastLines) indentCount
      [] ->
        List.reverse pastLines
  in
  indent lines [] 0
  |> String.join "\n"


{-| Guess the formatting before any parsing -}
formatter : Int -> Int -> String -> (String, Int, Int)
formatter cursor_line cursor_column code =
  code
  |> addCursor cursor_line cursor_column
  |> Result.map addOperatorSpaces
  |> Result.map (String.lines)
  |> Result.map (List.map String.trimLeft)
  |> Result.map (List.map addNewlineVariable)
  |> Result.map (String.join "\n")
  |> Result.map removeCursor
  |> Result.toMaybe
  |> Maybe.withDefault (code, cursor_line, cursor_column)

{-| Insert '~' marker for the newlines / end of instructions as seen by the compiler -}
insertNewlines : String -> Int -> Int -> List (Int, Int) -> List String -> List String
insertNewlines sep cursor_line cursor_column newlines lines =
  let filter linenb tup =
    let line = Tuple.first tup in
    line == linenb
  in
  let insertLine lineidx line =
    let addCursor chrList =
      if lineidx /= cursor_line then
        chrList
      else
        let addOne nb =
          if nb >= cursor_column then
            nb + 1
          else
            nb
        in
        List.map addOne chrList
      in
    let positions =
      List.filter (filter (lineidx + 1)) newlines
      |> List.map Tuple.second
      |> List.sort
      |> addCursor
    in
    let insert lastPos posList aux =
      case posList of
        [] ->
          aux ++ (String.dropLeft lastPos line)
        pos :: rest ->
          insert pos rest (aux ++ (String.slice lastPos pos line) ++ sep)
    in
    insert 0 positions ""
  in
  List.indexedMap insertLine lines


{-| Remove the newlines which are in the middle of statements -}
removeSelectedNewlines : String -> String
removeSelectedNewlines code =
  let lines = code |> String.split "~" in
  let remove line =
    let emptyFilter char =
      List.member char [' ', '\n', '$']
      |> not
    in
    if String.filter emptyFilter line == "" then
      line
    else
      if String.contains "$" line then
        line
      else if String.contains "variable" line then
        line
      else
        Regex.replace All (regex "\n") (\_ -> " ") line
        |> replace All (regex " +") (\_ -> " ")
  in
  let oddMap index line =
    if (index // 2) * 2 == index then
      line
    else
      "~" ++ line
  in
  List.map remove lines
  |> List.indexedMap oddMap
  |> String.join ""


{-| Formats according to what the parser saw -}
formatterBlocks : Int -> Int -> List (Int, Int) -> List (Int, Int) -> Maybe Int -> String -> (String, Int, Int)
formatterBlocks cursor_line cursor_column endlines beglines endline code =
  let lines =
    code
    |> addCursor cursor_line cursor_column
    |> Result.map (String.lines)
    |> Result.map (List.map String.trimLeft)
  in
  case lines of
    Err _ ->
      (code, cursor_line, cursor_column)
    Ok lines ->
      let fixed_lines =
        case endline of
          Nothing ->
            lines
            |> insertNewlines "~" cursor_line cursor_column (List.append beglines endlines)
            |> String.join "\n"
            |> removeSelectedNewlines
            |> Regex.replace All (regex "~[ |\\$]*\\n") (\{match} -> (String.dropLeft 1 match))
            |> Regex.replace All (regex "\\n[ |\\$]*~") (\{match} -> (String.dropRight 1 match))
            |> Regex.replace All (regex "~") (\_ -> "\n")
          Just endline ->
            let unseenlines =
              lines
              |> (List.drop (endline))
              |> (String.join "\n")
            in
            let seenlines =
              lines
              |> List.take (endline)
              |> insertNewlines "~" cursor_line cursor_column (List.append beglines endlines)
              |> String.join "\n"
              |> removeSelectedNewlines
              |> Regex.replace All (regex "~[ |\\$]*\\n") (\{match} -> (String.dropLeft 1 match))
              |> Regex.replace All (regex "\\n[ |\\$]*~") (\{match} -> (String.dropRight 1 match))
              |> Regex.replace All (regex "~$") (\{match} -> "")
              |> Regex.replace All (regex "~") (\_ -> "\n")
           in
           seenlines ++ "\n" ++ unseenlines
     in
     fixed_lines
     |> String.lines
     |> List.map addNewlineVariable
     |> List.map String.trimLeft
     |> String.join "\n"
     |> addIndentation
     |> removeCursor
