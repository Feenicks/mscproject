module Tools exposing (findIndex, getIndex)

{-| Returns the index of the element in the list, -1 if it was not found -}
findIndex : a -> List a -> Int
findIndex element list =
  let findIndexRec element list index =
    case list of
      hd :: tl ->
        if element == hd then
          index
        else
          findIndexRec element tl (index+1)
      [] ->
        -1 {- Never happens -}
  in
  findIndexRec element list 0

{-| Returns the element at the index in the list.
  WARNING : RUNTIME error if it doesn't exist -}
getIndex : Int -> List a -> a
getIndex index list =
  if index == 0 then
    case List.head list of
      Just hd ->
        hd
      Nothing ->
        Debug.crash "Tried to get a non-existing index from list"
  else
    case List.tail list of
      Just tl ->
        getIndex (index - 1) tl
      Nothing ->
        Debug.crash "Tried to get a non-existing index from list"
