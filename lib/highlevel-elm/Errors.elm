module Errors exposing (Error, createLexAnaError, createParserError, updateParserLineNumber, addEOI, updateParserToken, updateParserPosition, replaceErrorNumber, printError, printErrorStyle, printGrammarHelp)

import Types exposing (Token, nullToken, ParseTree(..), ParsePosition(..))
import Tools exposing (getIndex)
import Regex exposing (HowMany(..))
import Array

type alias Error =
  { line               : Maybe Int
  , ch                 : Maybe Int
  , errorLexeme        : Maybe String
  , existingNames      : Maybe (List String)      {- existing names for close name search -}
  , errorParsePosition : Maybe ParsePosition      {- part of which structure is the error token -}
  , errorToken         : Maybe Token
  , endlines           : Maybe (List (Int, Int))  {- where should the end of lines be inserted -}
  , beglines           : Maybe (List (Int, Int))  {- where should the beginning of lines be inserted -}
  , errorNumber        : Int
  }

{-| Create an error during the Lexical Analysis -}
createLexAnaError : Int -> Int -> String -> Maybe (List String) -> Int -> Error
createLexAnaError line ch lexeme existingNames number =
  Error (Just line) (Just ch) (Just lexeme) existingNames Nothing Nothing Nothing Nothing number

{-| Create an error during the Parsing -}
createParserError : Maybe Int -> Maybe String -> Maybe Token -> List (Int, Int) -> List (Int, Int) -> Int -> Error
createParserError line lexeme token endlines beglines number =
  Error line Nothing lexeme Nothing Nothing token (Just endlines) (Just beglines) number

{-| Add line number to error record if it wasn't recorded yet -}
updateParserLineNumber : Int -> Result Error (a, b, c) -> Result Error (a, b, c)
updateParserLineNumber line result =
  let changeLine nb error =
    case error.line of
      Nothing ->
        { error | line = (Just nb) }
      Just _ ->
        error
  in
  Result.mapError (changeLine line) result

{-| Add EOI if no error -}
addEOI : Int -> Result Error (a, b, ParseTree) -> Result Error (a, b, ParseTree)
addEOI line result =
  let add (a, b, tree) =
    (a, b, UniNode [EOI line, tree])
  in
  Result.map add result

{-| Add token if it wasn't recorded yet -}
updateParserToken : Token -> Result Error (a, b, c) -> Result Error (a, b, c)
updateParserToken token result =
  let changeToken tok error =
    case error.errorToken of
      Nothing ->
        { error | errorToken = (Just tok) }
      Just _ ->
        error
  in
  Result.mapError (changeToken token) result

{-| Add parsing position if it wasn't recorded yet -}
updateParserPosition : ParsePosition -> Result Error (a, b, c) -> Result Error (a, b, c)
updateParserPosition position result =
  let changePosition position error =
    case error.errorParsePosition of
      Nothing ->
        { error | errorParsePosition = (Just position)}
      Just _ ->
        error
  in
  Result.mapError (changePosition position) result

{-| Replace the Error number if the old one is equal to something -}
replaceErrorNumber : Int -> Int -> Error -> Error
replaceErrorNumber old new error =
  if error.errorNumber == old then
    { error | errorNumber = new }
  else
    error

{-| Levenshtein distance between two string. Warning: slow for long string -}
levenshteinDistance : String -> String -> Float
levenshteinDistance a b =
  let aux a b lena lenb =
    if lena == 0 then
      lenb |> toFloat
    else if lenb == 0 then
      lena |> toFloat
    else
      let cost =
        if Array.get (lena - 1) a == Array.get (lenb - 1) b then
          0
        else
          1
      in
      min
        ( min
          ( aux a b (lena - 1) lenb + 1 )
          ( aux a b lena (lenb - 1) + 1 )
        )
        ( aux a b (lena - 1) (lenb - 1) + cost )
  in
  aux (Array.fromList (String.toList a)) (Array.fromList (String.toList b)) (String.length a) (String.length b)

{-| Number of common characters in two strings -}
commonChars : String -> String -> Float
commonChars a b =
  let isIn str chr =
    String.any ((==) chr) str
  in
  ( toFloat ( String.filter (isIn b) a
  |> String.length)
  + toFloat (String.filter (isIn b) a
  |> String.length ))
  / 2

{-| Approximate string matching -}
closeNames : List String -> String -> List String
closeNames nameList name =
  let filterName trialName =
    let len = ((String.length name) + (String.length trialName) |> toFloat) / 2 in
    let commonCharPourcentage = (commonChars trialName name) / len in
    if commonCharPourcentage < 0.5 then
      False
    else
      {- Don't compute the distance for long string -}
      let distancePourcentage =
        if len > 6 then
          1
        else
          (levenshteinDistance trialName name) / len
      in
      (distancePourcentage <= 0.5 || commonCharPourcentage >= 0.8)
  in
  List.filter filterName nameList


{-
####### ERROR MESSAGE FORMATTING #######
-}

{-| Print the line number -}
addLineNumber : Error -> String -> String
addLineNumber error message =
  case error.line of
    Nothing ->
      message
    Just line ->
      message ++ "Line " ++ (toString line) ++ ": "

addErrorType : Error -> String -> String
addErrorType error message =
  let errorNumber = error.errorNumber in
  let errorMessage = "<span style=\"font-weight:bold;\">Error [" ++ toString (errorNumber) ++ "]: " in
  let errorMessage2 =
    if List.member errorNumber [0] then
      errorMessage ++ "Empty Code"
      {- Error during Lexical Analysis -}
    else if List.member errorNumber [100] then
      errorMessage ++ "Parsing Error"
    else if List.member errorNumber [101, 102, 103, 104, 105, 106, 143, 144] then
      errorMessage ++ "Issue with the name of the variable"
    else if List.member errorNumber [110, 140, 141, 142] then
      errorMessage ++ "Keyword misuse"
    else if List.member errorNumber [111, 145] then
      errorMessage ++ "Undefined name"
    else if List.member errorNumber [120, 121, 122, 123, 124, 125, 126] then
      errorMessage ++ "Issue with the name of the procedure/function"
    else if List.member errorNumber [130, 131, 132, 133, 134, 135, 136, 137] then
      errorMessage ++ "Issue with the name of the argument"
    else if List.member errorNumber [151] then
      errorMessage ++ "Procedure or function misuse"

    {- Error during Parsing -}
    else if List.member errorNumber [201, 202, 205, 210, 212, 214, 215, 216, 217, 219, 221, 222, 223, 225, 229, 231, 233, 234, 235, 236, 238, 239] then
      errorMessage ++ "Something is missing"
    else if List.member errorNumber [203, 206, 209, 211] then
      errorMessage ++ "Operation not allowed"
    else if List.member errorNumber [204, 207, 213, 218, 220, 224, 230] then
      errorMessage ++ "Unexpected statement"
    else if List.member errorNumber [208] then
      errorMessage ++ "Mismatch in number of variables"
    else if List.member errorNumber [226] then
      errorMessage ++ "Unfinished procedure/function declaration"
    else if List.member errorNumber [227] then
      errorMessage ++ "Missing arguments"
    else if List.member errorNumber [228, 232] then
      errorMessage ++ "Missing name"
    else if List.member errorNumber [992, 993, 994, 995, 996, 997, 998, 999] then
      errorMessage ++ "Sofware bug" {- These cases should never happen -}

    else
      errorMessage ++ "Unknown error number"
  in
    errorMessage2 ++ "</span>\n"


addErrorMessage : Error -> String -> String
addErrorMessage error message =
  let lexeme = Maybe.withDefault "" error.errorLexeme in
  let token  = Maybe.withDefault nullToken error.errorToken in
  let position = Maybe.withDefault MAIN error.errorParsePosition in
  case error.errorNumber of
    0 ->
      message ++ "Empty Code"

    {- Error during Lexical Analysis -}
    {- Error common to several States -}
    100 ->
      message ++ "Parsing Error.\n  Could not parse '" ++ lexeme ++ "'. Allowed characters are: a-z, A-Z, 0-9, _"
    101 ->
      message ++ "Wrong variable name in creation.\n  Cannot use keyword '" ++ lexeme ++ "' as a variable name."
    102 ->
      message ++ "Wrong variable name in creation.\n  Cannot use function or procedure '" ++ lexeme ++ "' as a variable name."
    103 ->
      message ++ "Wrong variable name in creation.\n  A variable name has to start with a letter. '" ++ lexeme ++ "' doesn't."
    104 ->
      message ++ "Wrong variable name in creation.\n  '" ++ lexeme ++ "' is already the name of a global variable."
    105 ->
      message ++ "Wrong variable name in creation.\n  '" ++ lexeme ++ "' is already the name of a note."
    106 ->
      message ++ "Wrong variable name in creation.\n  '" ++ lexeme ++ "' is already the name of a colour."

    {- Error in State 1 -}
    110 ->
      message ++ "Keyword misuse.\n  Cannot use keyword '" ++ lexeme ++ "' here."
    111 ->
      message ++ "Unknown name.\n  Variable, procedure or function '" ++ lexeme ++ "' is not defined."

    {- Error in State 2 -}
    120 ->
      message ++ "Wrong name for a procedure or function.\n  Cannot use keyword '" ++ lexeme ++ "' as a name."
    121 ->
      message ++ "Wrong name for a procedure or function.\n  Cannot use already existing procedure or function name '" ++ lexeme ++ "' as a name."
    122 ->
      message ++ "Wrong name for a procedure or function.\n  A procedure or function name has to start with a letter. '" ++ lexeme ++ "' doesn't."
    123 ->
      message ++ "Wrong name for a procedure or function.\n  Cannot use global variable name '" ++ lexeme ++ "' as a name."
    124 ->
      message ++ "Wrong name for a procedure or function.\n  Cannot use note name '" ++ lexeme ++ "' as a name."
    125 ->
      message ++ "Wrong name for a procedure or function.\n Cannot use colour name '" ++ lexeme ++ "' as a name."
    126 ->
      message ++ "Wrong name for a procedure of function.\n Cannot use names like 'if_[0-9]', 'endif_[0-9]', 'while_[0-9]' or 'endwhile_[0-9]'."

    {- Error in State 3 -}
    130 ->
      message ++ "Wrong argument name in procedure or function creation.\n  Cannot use keyword '" ++ lexeme ++ "' as a name."
    131 ->
      message ++ "Wrong argument name in procedure or function creation.\n  Cannot use already existing procedure or function name '" ++ lexeme ++ "' as a name."
    132 ->
      message ++ "Wrong argument name in procedure or function creation.\n  A procedure or function argument has to start with a letter. '" ++ lexeme ++ "' doesn't."
    133 ->
      message ++ "Wrong argument name in procedure or function creation.\n  Cannot use global variable name '" ++ lexeme ++ "' as a name."
    134 ->
      message ++ "Wrong argument name in procedure or function creation.\n  Cannot use same name '" ++ lexeme ++ "' for several arguments."
    135 ->
      message ++ "Wrong argument name in procedure or function creation.\n  Cannot use procedure or function name '" ++ lexeme ++ "' as a name."
    136 ->
      message ++ "Wrong argument name in procedure or function creation.\n  Cannot use note name '" ++ lexeme ++ "' as a name."
    137 ->
      message ++ "Wrong argument name in procedure or function creation.\n  Cannot use colour name '" ++ lexeme ++ "' as a name."

    {- Error in State 4 -}
    140 ->
      message ++ "Keyword misuse.\n  Cannot use keyword '" ++ lexeme ++ "' inside a procedure definition."
    141 ->
      message ++ "Keyword misuse.\n  Cannot use keyword '" ++ lexeme ++ "' inside a function definition."
    142 ->
      message ++ "Keyword misuse.\n  A 'return' statement is expected before using the keyword '" ++ lexeme ++ "' inside a function definition."
    143 ->
      message ++ "Wrong variable name in creation.\n  '" ++ lexeme ++ "' is already the name of a local variable."
    144 ->
      message ++ "Wrong variable name in creation.\n  '" ++ lexeme ++ "' is already the name of an argument."
    145 ->
      message ++ "Unknown name.\n  Variable, argument, procedure or function '" ++ lexeme ++ "' is not defined."

    {- Error in State 5 -}
    150 ->
      message ++ "Keyword misuse.\n  Cannot use keyword '" ++ lexeme ++ "' inside a function return statement, or, in place of the endfunction keyword."
    151 ->
      message ++ "Procedure or function misuse.\n  Cannot call procedure or function '" ++ lexeme ++ "' inside a function return statement"

    {- Error during Parsing -}
    201 ->
      message ++ "Missing statement.\n See grammar completion box for help."
    202 ->
      message ++ "Missing statement.\n See grammar completion box for help."
    203 ->
      message ++ "Cannot create variable '" ++ token.lexeme ++ "' inside a loop (while) or a conditional (if)"
    204 ->
      message ++ "Expected statement but got '" ++ token.lexeme ++ "' instead.\n  See grammar completion box for help."
    205 ->
      message ++ "Missing statement.\n See grammar completion box for help."
    206 ->
      message ++ "Cannot modify global variable '" ++ token.lexeme ++ "' inside a procedure or a function."
    207 ->
      message ++ "Expected '=' sign for the assignment of variable (or another variable name for multiple assignment) but got '" ++ token.lexeme ++ "'."
    208 ->
      let argList = String.split " " lexeme in
      message ++ "Function " ++ (getIndex 0 argList) ++ " returns " ++ (getIndex 1 argList) ++ " values but only " ++ (getIndex 2 argList) ++ " variables are assigned."
    209 ->
      message ++ "Except when a function returns several values, you may only assign one value at a time."
    210 ->
      message ++ "Expected '=' sign for the assignment of variable (or another variable name for multiple assignment)'."
    211 ->
      message ++ "Cannot assign global variable '" ++ token.lexeme ++ "' inside a procedure or function. Only local variables may be modified."
    212 ->
      message ++ "Expected closing bracket for opening bracket line " ++ (toString token.line) ++ "."
    213 ->
      if List.member position [MAIN, CONDITIONAL, LOOP] then
        message ++ "Expected variable or number but got '" ++ token.lexeme ++ "' instead."
      else
        message ++ "Expected variable, argument or number but got '" ++ token.lexeme ++ "' instead."
    214 ->
      if List.member position [MAIN, CONDITIONAL, LOOP] then
        message ++ "Expected variable,argument or number."
      else
        message ++ "Expected variable or number."
    215 ->
      if List.member position [MAIN, CONDITIONAL, LOOP] then
        message ++ "Expected variable or number."
      else
        message ++ "Expected variable, argument or number."
    216 ->
      if token /= nullToken then
        if List.member position [MAIN, CONDITIONAL, LOOP] then
          message ++ "Expected a number or variable for the boolean condition but got '" ++ token.lexeme ++ "' instead."
        else
          message ++ "Expected a number, variable or argument for the boolean condition but got '" ++ token.lexeme ++ "' instead."
      else
        message ++ "Expected boolean condition"
    217 ->
      message ++ "Missing <, >, <=, >=, ==, != sign in condition.\n See grammar completion box for help."
    218 ->
      message ++ "Expected <, >, <=, >=, ==, != sign in condition but got '" ++ token.lexeme ++ "' instead."
    219 ->
      message ++ "Missing 'then' keyword after boolean condition.\n See grammar completion box for help."
    220 ->
      message ++ "Expected 'then' keyword after boolean condition but got '" ++ token.lexeme ++ "' instead."
    221 ->
      if token /= nullToken then
        message ++ "Missing 'else' or 'endif' keyword, or, statement, got '" ++ token.lexeme ++ "' instead.\n See grammar completion box for help."
      else
        message ++ "Missing 'else' or 'endif' keyword, or, statement.\n See grammar completion box for help."
    222 ->
      if token /= nullToken then
        message ++ "Missing 'endif' keyword, or, statement, got '" ++ token.lexeme ++ "' instead.\n See grammar completion box for help."
      else
        message ++ "Missing 'endif' keyword, or, statement.\n See grammar completion box for help."
    223 ->
      message ++ "Missing 'do' keyword after boolean condition.\n See grammar completion box for help."
    224 ->
      message ++ "Expected 'do' keyword after boolean condition but got '" ++ token.lexeme ++ "' instead."
    225 ->
      if token /= nullToken then
        message ++ "Missing 'endwhile' keyword, or, statement, got '" ++ token.lexeme ++ "' instead.\n See grammar completion box for help."
      else
        message ++ "Missing 'endwhile' keyword, or, statement.\n See grammar completion box for help."
    226 ->
      message ++ "Procedure or function '" ++ token.lexeme ++ "' not finished to define"
    227 ->
      message ++ "Procedure or function '" ++ token.lexeme ++ "' is missing " ++ lexeme ++ " arguments."
    228 ->
      message ++ "Missing name for procedure.\n See grammar completion box for help."
    229 ->
      message ++ "Missing 'as' keyword, or, arguments.\n See grammar completion box for help."
    230 ->
      message ++ "Expected 'as' keyword but got '" ++ token.lexeme ++ "' instead."
    231 ->
      if token /= nullToken then
        message ++ "Missing 'enddefine' keyword, or, statement, got '" ++ token.lexeme ++ "' instead.\n See grammar completion box for help."
      else
        message ++ "Missing 'enddefine' keyword, or, statement.\n See grammar completion box for help."
    232 ->
      message ++ "Missing name for function."
    233 ->
      if token /= nullToken then
        message ++ "Missing 'return' keyword, or, statement, got '" ++ token.lexeme ++ "' instead.\n See grammar completion box for help."
      else
        message ++ "Missing 'return' keyword, or, statement.\n See grammar completion box for help."
    234 ->
      if token /= nullToken then
        message ++ "Missing 'endfunction' keyword, or, return value, got '" ++ token.lexeme ++ "' instead.\n See grammar completion box for help."
      else
        message ++ "Missing 'endfunction' keyword, or, return value.\n See grammar completion box for help."
    235 ->
      message ++ "Missing statement.\n See grammar completion box for help."
    236 ->
      message ++ "Expected closing bracket for opening bracket line " ++ (toString token.line) ++ " but got '" ++ lexeme ++ "' instead."
    237 ->
      message ++ "Function ending line " ++ (toString token.line) ++ " returns more than the 3 allowed values."
    238 ->
      message ++ "Function ending line " ++ (toString token.line) ++ " needs to return at least 1 value."
    239 ->
      message ++ "A function needs to return at least 1 value."

    992 ->
      message ++ "Code 992 : This should never happen there is a bug in the program."
    993 ->
      message ++ "Code 993 : This should never happen there is a bug in the program."
    994 ->
      message ++ "Code 994 : This should never happen there is a bug in the program."
    995 ->
      message ++ "Code 995 : This should never happen there is a bug in the program."
    996 ->
      message ++ "Code 996 : This should never happen there is a bug in the program."
    997 ->
      message ++ "Code 997 : This should never happen there is a bug in the program."
    998 ->
      message ++ "Code 998 : This should never happen there is a bug in the program."
    999 ->
      message ++ "Code 999 : This should never happen there is a bug in the program."
    other ->
      "Unknown code error: " ++ (toString error.errorNumber)

countIndentation : String -> Int
countIndentation line =
  case Regex.find (AtMost 1) (Regex.regex "^ +") line of
    [] -> 0
    hd :: _ -> String.length hd.match

{-| Print the code line with ^^^ under the error -}
addErrorCode : Error -> String -> String -> String
addErrorCode error code message =
  let errorNumber = error.errorNumber in
  if not (List.member errorNumber [0, 201, 202, 205, 210, 212, 214, 215, 216, 217, 219, 221, 222, 223, 225, 227, 228, 229, 231, 232, 233, 234, 235, 236, 992, 993, 994, 995, 996, 997, 998, 999]) then
    case error.line of
      Nothing ->
        message
      Just line ->
        let myLine = getIndex (line - 1) (String.lines code) in
        let myIndent = countIndentation myLine in
        let messageLine = message ++ "\n" ++ toString line ++ ": " ++ myLine in
        if errorNumber >= 100 && errorNumber < 200 then
          let mySpace = Maybe.withDefault 0 error.ch + (String.length (toString line)) + 2 + myIndent in
          let myLength = String.length (Maybe.withDefault "" error.errorLexeme) in
          messageLine ++ "\n" ++ "<span style=\"color:#7A0000;\">" ++ String.repeat mySpace " " ++ String.repeat myLength "^" ++ "</span>\n"
        else if List.member errorNumber [203, 204, 206, 207, 213, 218, 220, 224, 226, 227, 230] then
          let token = Maybe.withDefault nullToken error.errorToken in
          let mySpace = token.ch + (String.length (toString line)) + 2 + myIndent in
          let myLength = String.length token.lexeme in
          messageLine ++ "\n" ++ "<span style=\"color:#7A0000;\">" ++ String.repeat mySpace " " ++ String.repeat myLength "^" ++ "</span>\n"
        else if List.member errorNumber [208, 212, 236] then
          let mySpace = (String.length (toString line)) + 2 + myIndent in
          let myLength = String.length myLine in
          messageLine ++ "\n" ++ "<span style=\"color:#7A0000;\">" ++ String.repeat mySpace " " ++ String.repeat myLength "^" ++ "</span>\n"
        else
          messageLine
  else
    message

{- Print close existing names in case of an unknown name -}
addCloseNames : Error -> String -> String
addCloseNames error message =
  if error.errorNumber == 111 || error.errorNumber == 145 then
    let lexeme = Maybe.withDefault "" error.errorLexeme in
    let nameList = Maybe.withDefault [] error.existingNames in
    let helpNames = closeNames nameList lexeme in
    if List.isEmpty helpNames then
      message
    else
      message ++ "\n" ++ "Instead of '" ++ lexeme ++ "' did you mean one of the similar names:\n    - " ++ String.join "\n    - " helpNames ++ "\n ?\n"
  else
    message

{-| Transforming the error into a text -}
printError : String -> Error-> String
printError code error  =
  ""
  |> addErrorType error
  |> addLineNumber error
  |> addErrorMessage error
  |> addErrorCode error code
  |> addCloseNames error

{-| CSS Style for the error -}
printErrorStyle : Maybe Error -> String
printErrorStyle error =
  case error of
    Just error ->
      let errorNumber = error.errorNumber in
      if List.member errorNumber [0, 201, 202, 205, 210, 212, 214, 215, 216, 217, 219, 221, 222, 223, 225, 227, 228, 229, 231, 232, 233, 234, 235, 236] then
        "warning"
      else
        "error"
    Nothing ->
      "ok"

functionGrammar =
  "<span style=\"font-weight:bold;\">Fonction Definition</span>\n"
   ++ "  <span id=\"keyword\">function</span> <span style=\"font-style:italic;\">functionname arguments</span> <span id=\"keyword\">as</span>\n"
   ++ "    statements\n"
   ++ "    <span id=\"keyword\">return</span> <span style=\"font-style:italic;\">expressions\n</span>"
   ++ "  <span id=\"keyword\">endfunction</span>\n"

procedureGrammar =
  "<span style=\"font-weight:bold;\">Procedure Definition</span>\n"
   ++ "  <span id=\"keyword\">define</span> <span style=\"font-style:italic;\">procedurename arguments</span> <span id=\"keyword\">as</span>\n"
   ++ "    statements\n"
   ++ "  <span id=\"keyword\">enddefine</span>\n"

variableCreation =
  "<span style=\"font-weight:bold;\">Variable Creation</span>\n"
   ++ "  <span id=\"keyword\">variable</span> <span style=\"font-style:italic;\">variablenames</span>\n"

variableAssignment =
  "<span style=\"font-weight:bold;\">Variable Assignment</span>\n"
   ++ "  <span style=\"font-style:italic;\">variablename</span> = expression\n"
   ++ "  <span style=\"font-style:italic;\">variablenames</span> = function\n"

conditional =
  "<span style=\"font-weight:bold;\">Conditional Branch(es)</span>\n"
   ++ "  <span id=\"keyword\">if</span> booleancondition <span id=\"keyword\">then</span>\n"
   ++ "    statements\n"
   ++ "  <span id=\"keyword\">else</span>\n"
   ++ "    statements\n"
   ++ "  <span id=\"keyword\">endif</span>\n\n"
   ++ "  <span id=\"keyword\">if</span> booleancondition <span id=\"keyword\">then</span>\n"
   ++ "    statements\n"
   ++ "  <span id=\"keyword\">endif</span>\n"

loop =
  "<span style=\"font-weight:bold;\">Loop</span>\n"
   ++ "  <span id=\"keyword\">while</span> booleancondition <span id=\"keyword\">do</span>\n"
   ++ "    statements\n"
   ++ "  <span id=\"keyword\">endwhile</span>\n"

condition =
  "<span style=\"font-weight:bold;\">Boolean Condition</span>\n"
   ++ "  expression <span id=\"keyword\">&lt;|&gt;|==|!=|&gt;=|&lt;=</span> expression\n"

expression =
  "<span style=\"font-weight:bold;\">Expression</span>\n"
   ++ "  factor <span id=\"keyword\">+|-|*|/</span> expresion\n"

factor =
  "<span style=\"font-weight:bold;\">Factor</span>\n"
   ++ "  number | variable | ( expression )\n"

statementsinprocfunc =
  "<span style=\"font-weight:bold;\">Inside procedure/function definition</span>\n"
   ++ "  Local Variable Assignment\n"

statementsoutprocfunc =
 "<span style=\"font-weight:bold;\">Outside of procedure/function definition</span>\n"
  ++ "  Global Variable Assignment\n"

statementsoutcondloop =
  "<span style=\"font-weight:bold;\">Outside of condition branches or loops</span>\n"
   ++ "  Variable Creation\n"

allstatements =
  "<span style=\"font-weight:bold;\">Anywhere</span>\n"
  ++ "  Conditional Branch(es)\n"
  ++ "  Loop\n"
  ++ "  Procedure Call\n"

{-| In procedure / function usages, put the id 'funcname' around the name and the arguments in italic -}
prettifyUsage : List String -> String
prettifyUsage usages =
  let prettifySingle usage =
    let cut = Regex.split (AtMost 1) (Regex.regex " ") usage in
    case cut of
      [name, arguments] -> "<span id=\"funcname\">" ++ name ++ "</span>" ++ " <span style=\"font-style:italic\">" ++ arguments ++ "</span>"
      other -> "<span id=\"funcname\">" ++ usage ++ "</span>"
  in
  usages |> List.map prettifySingle |> String.join "\n"

{-| Returns the Grammar Panel -}
menu : List String -> List String -> String -> String
menu proceduresUsage functionsUsage errorPanel =
  """ <div class="panel-group" id="accordion">
        <div class="panel">
            <div class="panel-heading">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Variables</a>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">"""
                  ++ variableCreation ++ variableAssignment ++ expression ++ factor ++
                """</div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Notes</a>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">"""
                ++ "C3 C3# D3 D3# E3 F3 F3# G3 G3# A3 A3# B3\nC4 C4# D4 D4# E4 F4 F4# G4 G4# A4 A4# B4\nC5 C5# D5 D5# E5 F5 F5# G5 G5# A5 A5# B5\nC6"++
          {-       ++ "C1 C1# D1 D1# E1 F1 F1# G1 G1# A1 A1# B1\nC2 C2# D2 D2# E2 F2 F2# G2 G2# A2 A2# B2\nC3 C3# D3 D3# E3 F3 F3# G3 G3# A3 A3# B3\nC4 C4# D4 D4# E4 F4 F4# G4 G4# A4 A4# B4\nC5 C5# D5 D5# E5 F5 F5# G5 G5# A5 A5# B5\nC6"++ -}
                """</div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Colours</a>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">"""
                ++ "black (or 0)\nblue (or 1)\ngreen (or 2)\ncyan (or 3)\nred (or 4)\nmagenta (or 5)\nyellow (or 6)\nwhite (or 7)\n" ++
                """</div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Conditional Branches and Loops</a>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">"""
                  ++ conditional ++ loop ++ condition ++
                """</div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Procedures</a>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">"""
                  ++ prettifyUsage proceduresUsage ++ "\n" ++
                """</div>
            </div>
        </div>
         <div class="panel">
            <div class="panel-heading">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Functions</a>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="panel-body">"""
                ++ prettifyUsage functionsUsage ++ "\n" ++
                """</div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Procedure and Function Definition</a>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse">
                <div class="panel-body">"""
                  ++ procedureGrammar ++ functionGrammar ++
                """</div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">Statements</a>
            </div>
            <div id="collapseEight" class="panel-collapse collapse">
                <div class="panel-body">"""
                  ++ allstatements ++ statementsoutcondloop ++ statementsoutprocfunc ++ statementsinprocfunc ++
                """</div>
            </div>
        </div>"""
        ++ errorPanel ++
      """</div>"""

{-| Add panel depending on what structure the error token was in -}
warningPanel : ParsePosition -> String
warningPanel position =
  let help =
    case position of
      MAIN ->
        "<span style=\"font-weight:bold;\">Things you can do:</span>\n"
        ++ "  Global Variable Creation\n"
        ++ "  Global Variable Assignment\n"
        ++ "  Conditional Branch(es)\n"
        ++ "  Loop\n"
        ++ "  Procedure Call\n"

      CONDITIONAL ->
        "<span style=\"font-weight:bold;\">Structure:</span>\n"
        ++ "  <span id=\"keyword\">if</span> booleancondition <span id=\"keyword\">then</span>\n"
        ++ "    statements\n"
        ++ "  <span id=\"keyword\">else</span>\n"
        ++ "    statements\n"
        ++ "  <span id=\"keyword\">endif</span>\n\n"
        ++ "  <span id=\"keyword\">if</span> condition <span id=\"keyword\">then</span>\n"
        ++ "    statements\n"
        ++ "  <span id=\"keyword\">endif</span>\n"
        ++ "\n"
        ++ "<span style=\"font-weight:bold;\">Things you can do:</span>\n"
        ++ "  Global Variable Assignment\n"
        ++ "  Conditional Branch(es)\n"
        ++ "  Loop\n"
        ++ "  Procedure Call\n"

      LOOP ->
        "<span style=\"font-weight:bold;\">Structure:</span>\n"
        ++ "  <span id=\"keyword\">while</span> booleancondition <span id=\"keyword\">do</span>\n"
        ++ "    statements\n"
        ++ "  <span id=\"keyword\">endwhile</span>\n"
        ++ "\n"
        ++ "<span style=\"font-weight:bold;\">Things you can do:</span>\n"
        ++ "  Global Variable Assignment\n"
        ++ "  Conditional Branch(es)\n"
        ++ "  Loop\n"
        ++ "  Procedure Call\n"

      INPROC ->
        "<span style=\"font-weight:bold;\">Structure:</span>\n"
        ++ "  <span id=\"keyword\">define</span> <span style=\"font-style:italic;\">procedurename arguments</span> <span id=\"keyword\">as</span>\n"
        ++ "    statements\n"
        ++ "  <span id=\"keyword\">enddefine</span>\n"
        ++ "\n"
        ++ "<span style=\"font-weight:bold;\">Things you can do:</span>\n"
        ++ "  Local Variable Creation\n"
        ++ "  Local Variable Assignment\n"
        ++ "  Conditional Branch(es)\n"
        ++ "  Loop\n"
        ++ "  Procedure Call\n"

      INFUNC ->
        "<span style=\"font-weight:bold;\">Structure:</span>\n"
        ++ "  <span id=\"keyword\">function</span> <span style=\"font-style:italic;\">functionname arguments</span> <span id=\"keyword\">as</span>\n"
        ++ "    statements\n"
        ++ "    <span id=\"keyword\">return</span> <span style=\"font-style:italic;\">expressions\n</span>"
        ++ "  <span id=\"keyword\">endfunction</span>\n"
        ++ "\n"
        ++ "<span style=\"font-weight:bold;\">Things you can do:</span>\n"
        ++ "  Local Variable Creation\n"
        ++ "  Local Variable Assignment\n"
        ++ "  Conditional Branch(es)\n"
        ++ "  Loop\n"
        ++ "  Procedure Call\n"

      CONDITIONALINPROCFUNC ->
        "<span style=\"font-weight:bold;\">Structure:</span>\n"
        ++ "  <span id=\"keyword\">if</span> booleancondition <span id=\"keyword\">then</span>\n"
        ++ "    statements\n"
        ++ "  <span id=\"keyword\">else</span>\n"
        ++ "    statements\n"
        ++ "  <span id=\"keyword\">endif</span>\n\n"
        ++ "  <span id=\"keyword\">if</span> condition <span id=\"keyword\">then</span>\n"
        ++ "    statements\n"
        ++ "  <span id=\"keyword\">endif</span>\n"
        ++ "\n"
        ++ "<span style=\"font-weight:bold;\">Things you can do:</span>\n"
        ++ "  Local Variable Assignment\n"
        ++ "  Conditional Branch(es)\n"
        ++ "  Loop\n"
        ++ "  Procedure Call\n"

      LOOPINPROCFUNC ->
        "<span style=\"font-weight:bold;\">Structure:</span>\n"
        ++ "  <span id=\"keyword\">while</span> booleancondition <span id=\"keyword\">do</span>\n"
        ++ "    statements\n"
        ++ "  <span id=\"keyword\">endwhile</span>\n"
        ++ "\n"
        ++ "<span style=\"font-weight:bold;\">Things you can do:</span>\n"
        ++ "  Local Variable Assignment\n"
        ++ "  Conditional Branch(es)\n"
        ++ "  Loop\n"
        ++ "  Procedure Call\n"

  in
  """<div class="panel">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseNine">Completion Help</a>
      </div>
      <div id="collapseNine" class="panel-collapse collapse in">
          <div class="panel-body">"""
            ++ help ++
          """</div>
      </div>
    </div>"""

{-| Returns the complete Grammar Panel -}
printGrammarHelp : List String -> List String -> Maybe Error -> String
printGrammarHelp proceduresUsage functionsUsage error =
  case error of
    Nothing ->
      menu proceduresUsage functionsUsage ""
    Just error ->
      case error.errorParsePosition of
        Nothing ->
          menu proceduresUsage functionsUsage ""
        Just position ->
          let errorNumber = error.errorNumber in
          if List.member errorNumber [201, 202, 205, 210, 212, 214, 215, 216, 217, 219, 221, 222, 223, 225, 228, 229, 231, 232, 233, 234, 235, 236, 237, 238, 239] then
            menu proceduresUsage functionsUsage (warningPanel position)
          else
            menu proceduresUsage functionsUsage ""
