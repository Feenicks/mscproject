port module StackExports exposing (..)

import StackAssembler exposing (assembler)

import Html exposing (..)

type alias Flags =
  {}

main =
  Html.programWithFlags
   { init = init
   , view = view
   , update = update
   , subscriptions = subscriptions
   }


-- MODEL

type alias Model =
  { hexCode : String, vmCode : String }

init: Flags -> (Model, Cmd Msg)
init flags =
  (Model "" "", Cmd.none)

-- UPDATE

type Msg
  = Compile (String, String)
  | Result


-- port for sending the compiling result out to JavaScript
port result : (String, String) -> Cmd msg

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Compile (newCode, newVmCode) ->
      { hexCode = (assembler newCode), vmCode = (assembler newVmCode) }
      |> update Result

    Result ->
      ( model, result (model.hexCode, model.vmCode) )


-- SUBSCRIPTIONS

-- port for listening for calls from JavaScript
port stackassembler : ((String, String) -> msg) -> Sub msg

subscriptions : Model -> Sub Msg
subscriptions model =
  stackassembler Compile


-- VIEW

view : Model -> Html Msg
view model =
  div [] []
