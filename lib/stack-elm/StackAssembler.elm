module StackAssembler exposing (assembler)

import Regex exposing (regex, HowMany(..))
import Dict exposing (Dict)

type alias State =
  { currentAddress : Int
  , inRawCode      : Bool
  , labels         : Dict String String
  , readIntArg     : Bool
  , readStringArg  : Bool
  , readFuncName   : Bool
  }

nullState = State 0 False Dict.empty False False False

mod16 : Int -> String
mod16 nb =
  case nb % 16 of
    10 -> "A"
    11 -> "B"
    12 -> "C"
    13 -> "D"
    14 -> "E"
    15 -> "F"
    other ->
      toString (nb % 16)

intToHexBytes : Int -> String
intToHexBytes int =
  let aux nb str =
    if nb == 0 then
      if str == "" then
        "0x00"
      else
        str
    else
      if str == "" then
        aux ((nb//16)//16) ("0x" ++ (mod16 (nb//16)) ++ (mod16 nb))
      else
        aux ((nb//16)//16) (str ++ " 0x" ++ (mod16 (nb//16)) ++ (mod16 nb))
  in
  if (int >= 128) && (int < 256) then
    (aux int "") ++ " 0x00"
  else
    aux int ""

{- Add current address as label position into the dictionary -}
addLabel : State -> String -> State
addLabel state label =
  let hexnb = intToHexBytes state.currentAddress in
  let count = List.length( Regex.find (All) (regex "x") hexnb) in
  if count == 1 then
    { state | labels = (Dict.insert label ("0x19 " ++ hexnb ++ " 0x00") state.labels) }
  else
    { state | labels = (Dict.insert label ("0x19 " ++ hexnb) state.labels) }

{- Increament the current address in compiled code -}
incr : State -> Int -> State
incr state nb =
  { state | currentAddress = state.currentAddress + nb }

{- If number, push it on stack -}
number : String -> Maybe (String, Int)
number inst =
  case String.toInt inst of
    Err _ -> Nothing
    Ok nb ->
      let hexnb = intToHexBytes nb in
      let count = List.length( Regex.find (All) (regex "x") hexnb) in
      if count == 1 then
        Just ("0x18 " ++ hexnb, 2)
      else
        Just ("0x19 " ++ hexnb, 3)

{- Transform instructions -}
instruction : State -> String -> Maybe (String, Int, State)
instruction state inst =
  case inst |> String.toUpper of
    "ADD"   -> Just ("0x00",1,state)
    "SUB"   -> Just ("0x01",1,state)
    "MUL"   -> Just ("0x02",1,state)
    "DIV"   -> Just ("0x03",1,state)
    "MOD"   -> Just ("0x04",1,state)
    "INC"   -> Just ("0x05",1,state)
    "DEC"   -> Just ("0x06",1,state)
    "MAX"   -> Just ("0x07",1,state)
    "MIN"   -> Just ("0x08",1,state)
    "LT"    -> Just ("0x09",1,state)
    "LE"    -> Just ("0x0A",1,state)
    "EQ"    -> Just ("0x0B",1,state)
    "GE"    -> Just ("0x0C",1,state)
    "GT"    -> Just ("0x0D",1,state)
    "DROP"  -> Just ("0x0E",1,state)
    "DUP"   -> Just ("0x0F",1,state)
    "NDUP"  -> Just ("0x10",1,state)
    "SWAP"  -> Just ("0x11",1,state)
    "ROT"   -> Just ("0x12",1,state)
    "NROT"  -> Just ("0x13",1,state)
    "TUCK"  -> Just ("0x14",1,state)
    "NTUCK" -> Just ("0x15",1,state)
    "SIZE"  -> Just ("0x16",1,state)
    "NRND"  -> Just ("0x17",1,state)
    "PUSH8" -> Just ("0x18",1,state)
    "PUSH"  -> Just ("0x19",1,state)
    "FETCH" -> Just ("0x1A",1,state)
    "CALL"  -> Just ("0x1B",1,state)
    "RET"   -> Just ("0x1C",1,state)
    "JMP"   -> Just ("0x1D",1,state)
    "CJMP"  -> Just ("0x1E",1,state)
    "HALT"  -> Just ("0x20",1,state)

    {- VM markers -}
    "#EOI"   -> Just ("#EOI",1, { state | readIntArg = True } )
    "#VAR"   -> Just ("#VAR",1, { state | readStringArg = True })
    "#FUNC"  -> Just ("#FUNC",1, { state | readFuncName = True })

    other -> Nothing


parse : (State, String) -> List String -> (State, String)
parse (state,compiledCode) tokenList =
  case tokenList of
    [] -> (state, compiledCode)
    token :: endList ->
      {- Raw code to translate. Example : [0x80 0x01] -}
      if state.inRawCode then
        if token == "]" then
          parse ({ state | inRawCode = False }, compiledCode) endList
        else
          parse ((incr state 1), (compiledCode ++ " " ++ token)) endList
      else
        if token == "[" then
          parse ({ state | inRawCode = True }, compiledCode) endList
        else
          {- Reading extra informations for the VM Markers -}
          if state.readIntArg then
            parse ((incr { state | readIntArg = False } 1), compiledCode ++ " " ++ token) endList
          else if state.readStringArg then
            parse ((incr { state | readStringArg = False } 1), compiledCode ++ " " ++ token) endList
          else if state.readFuncName then
            parse ((incr { state | readFuncName = False, readIntArg = True} 1), compiledCode ++ " " ++ token) endList
          {- Actual Stack Code -}
          else
            {- Real instruction ? -}
            case (instruction state token) of
              Just (token, nb, state) ->
                parse ((incr state nb), (compiledCode ++ " " ++ token)) endList
              Nothing ->
                {- Nunber ? -}
                case (number token) of
                  Just (token, nb) ->
                    parse ((incr state nb), (compiledCode ++ " " ++ token)) endList
                  Nothing ->
                    if token /= "" then
                      {- Label Definition ? -}
                      if (String.right 1 token) == ":" then
                        parse ((addLabel state ("l_" ++ String.dropRight 1 token)), compiledCode) endList
                      {- Label Usage ? -}
                      else
                        parse (incr state 3, compiledCode ++ " l_" ++ token) endList
                    else
                      parse (state, compiledCode) endList

{- replace the label markers (example: l_if_1)  with the addresses recorded -}
replaceLabels : (State, String) -> String
replaceLabels (state,compiledCode) =
  let replaceLabel label value code =
    Regex.replace (All) (regex ("(^| )" ++ label ++ "( |$)")) (\_ -> " " ++ value ++ " ") code
  in
  Dict.foldl replaceLabel compiledCode state.labels

assembler : String -> String
assembler code =
  code
  |> Regex.replace (All) (regex "\\[") (\_ -> "[ ")
  |> Regex.replace (All) (regex "\\]") (\_ -> " ]")
  |> String.words
  |> parse (nullState, "")
  |> replaceLabels
  |> String.trim
  |> Regex.replace (All) (regex "\\s+") (\_ -> " ")
