'use strict'

function getCodeEditorHtml() {
  return `
  <html>
    <body>

      <link href="./lib/my-code-editor/devArea.css" rel="stylesheet">

      <div id="editorDiv" class="container-fluid">

        <div class="col-lg-5 col-md-5 col-sd-6 col-xs-6 column">
          <div id="grammarBox"></div>
          <div id="allProceduresUsage" class="hidden"></div>
          <div id="allFunctionsUsage" class="hidden"></div>
        </div>
        <div class="col-lg-7 col-md-7 col-sd-6 col-xs-6 column">
          <div id="codeEditor"></div>
        </div>

      </div>


    </body>
  </html>
  `
}

function initEditor (stdlib) {
  $(document).ready(function(){
      $("#codingApp").html($.parseHTML(getCodeEditorHtml()));
      $("#elmCompiler").ready(function(){
        var node, app,
        syntaxBox, syntaxInfo, grammarBox, highlevelCode, stackCodeDiv, vmCodeDiv,
        codeEditor, allProceduresUsageDiv, allFunctionsUsageDiv,
        lastKeyStroke, lastAddedSpace, busyCompiler = false, waitingData = false;

        /* Importing the elm compiler app */
        node = document.getElementById('elmCompiler');
        app = Elm.Exports.embed(node, stdlib);

        syntaxBox = $('#syntaxBox');
        syntaxInfo = $("#syntaxInfo");
        grammarBox = $("#grammarBox");
        highlevelCode = $("#highlevelCode");
        stackCodeDiv = $("#stackCode");
        vmCodeDiv = $("#vmCode");
        allProceduresUsageDiv = $("#allProceduresUsage");
        allFunctionsUsageDiv = $("#allFunctionsUsage");

        /* Creating the CodeMirror code editor */
        codeEditor = new CodeMirror(document.getElementById('codeEditor'), {
            tabSize: 2,
            lineNumbers: true,
            smartIndent: false,
            lineWrapping: true
        });

        codeEditor.setOption("extraKeys", {
          Tab: function(cm) {
            var spaces = Array(cm.getOption("indentUnit") + 1).join(" ");
            cm.replaceSelection(spaces);
          }
        });

        /* returns True if the only changes where added spaces or newlines */
        function emptyChange (changeList) {
          var result, i;
          result = true
          for (i = 0; i < changeList.length; i++) {
            result = result && (changeList[i] === "" || changeList[i] === " ")
          }
          return result
        };

        /* code change event listener */
        codeEditor.on('change', function (instance, changeObject) {

          /* if real user change + not "empty change" */
          if (changeObject.origin !== "setValue" && (!emptyChange(changeObject.text) || !(emptyChange(changeObject.removed)))) {

            waitingData = true;

            syntaxBox.removeClass(syntaxBox.attr('class').split(' ').pop());
            syntaxBox.addClass("waiting");

            syntaxInfo.empty();
            syntaxInfo.append($.parseHTML("<b>Waiting for Compiler...</b>"));

            /* (re)launch timer before compiling */
            if (lastKeyStroke === undefined || lastKeyStroke === null) {
              lastKeyStroke = window.setTimeout(function(){launchCompiler()}, 750);
              lastAddedSpace = null;
            }
            else {
              window.clearTimeout(lastKeyStroke)
              lastKeyStroke = window.setTimeout(function(){launchCompiler()}, 750);
              lastAddedSpace = null;
            }
          }

          /* Longer timer if only thing added was spaces */
          else if (changeObject.origin !== "setValue") {

            syntaxBox.removeClass(syntaxBox.attr('class').split(' ').pop());
            syntaxBox.addClass("waiting");

            syntaxInfo.empty();
            syntaxInfo.append($.parseHTML("<b>Waiting for Compiler...</b>"));

            if (lastKeyStroke === undefined || lastKeyStroke === null) {
              if (lastAddedSpace === undefined || lastAddedSpace === null) {
                lastAddedSpace = window.setTimeout(function(){launchCompiler()}, 2000);
              }
              else {
                window.clearTimeout(lastAddedSpace)
                lastAddedSpace = window.setTimeout(function(){launchCompiler()}, 2000);
              }
            }
          }
        });

        function launchCompiler () {

            var input, line, col;

            lastKeyStroke = null;
            lastAddedSpace = null;

            /* if not already compiling something */
            if (!busyCompiler) {
                /* Run the scanner over the editor content */
                busyCompiler = true;
                waitingData = false;
                input = codeEditor.getValue();
                line = codeEditor.getCursor().line;
                col = codeEditor.getCursor().ch;
                app.ports.elmcompiler.send([input, line, col, parseInt($("#formattingMode").text())]);

            }
        };

        app.ports.result.subscribe(function(received) {

          var formattedCode, stackCode, vmCode, errors, line, col, errorStyle, grammarHelp,
            colouring, count, li1, ch1, li2, ch2, className, allProceduresUsage, allFunctionsUsage;

          formattedCode = received[0]
          line          = received[1][0]
          col           = received[1][1]
          stackCode     = received[2][0]
          vmCode        = received[2][1]
          errors        = received[3]
          errorStyle    = received[4]
          grammarHelp   = received[5]
          colouring     = received[6]
          allProceduresUsage = received[7][0]
          allFunctionsUsage = received[7][1]

          busyCompiler = false;

          /* No data arrived while processing */
          if (!waitingData) {
            /* Parse errors */
            codeEditor.setValue(formattedCode);
            codeEditor.setCursor({line:line, ch:col});
            grammarBox.empty();
            grammarBox.append($.parseHTML(grammarHelp));
            allProceduresUsageDiv.text(allProceduresUsage)
            allFunctionsUsageDiv.text(allFunctionsUsage)
            grammarBox.trigger("GRAMMAR_CHANGE")

            highlevelCode.text(formattedCode);

            /* Colour the CodeMirror code */
            count = colouring.length;
            for (var i = 0; i < count; i++) {
              li1 = colouring[i][0];
              ch1 = colouring[i][1];
              li2 = colouring[i][2];
              ch2 = colouring[i][3];
              className = colouring[i][4];
              codeEditor.markText({line:li1, ch:ch1}, {line:li2, ch:ch2}, { className: className });
            }

            /* Unsuccessful compiling */
            if (errors != null) {

                syntaxBox.removeClass(syntaxBox.attr('class').split(' ').pop());
                syntaxBox.addClass(errorStyle);

                syntaxInfo.empty();
                syntaxInfo.append($.parseHTML(errors));

                vmCodeDiv.empty();

                stackCodeDiv.empty();
                stackCodeDiv.trigger("NEW_CODE")

            } else {
              /* Successful compiling */

                syntaxBox.removeClass(syntaxBox.attr('class').split(' ').pop());
                syntaxBox.addClass(errorStyle);

                syntaxInfo.empty();
                syntaxInfo.append($.parseHTML("<b>Successfully compiled !</b>"));

                vmCodeDiv.empty();
                vmCodeDiv.text(vmCode);

                stackCodeDiv.text(stackCode);
                stackCodeDiv.trigger("NEW_CODE")

            }
        }

        });

        /* If code loaded (Load Button or Blockly) */
        highlevelCode.on('LOADED_CODE', function () {
          var code;

          code = highlevelCode.text();
          codeEditor.setValue(code);
          launchCompiler();
        })

        launchCompiler();

        });

})

}
