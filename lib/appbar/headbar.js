'use strict';

function getAppbarHtml() {
  return `
  <link href="./lib/appbar/headbar.css" rel="stylesheet">

  <div class="container-fluid">
    <div id="buttonBar">
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <ul class="nav navbar-nav">
          <li><a href="#" id="loadFileButton">Open</a></li>
          <li class="divider"> </li>
          <li><a href="#" id="saveFileButton">Save</a></li>
          <li class="divider"> </li>
          <li><a href="#" id="flashMicrobitButton">Flash</a></li>
          <li class="divider"> </li>
          <li><a href="#" id="switchEnvironmentButton">Switch Environment</a></li>
          <li class="divider"> </li>
          <li><a href="#" id="clearButton">Clear Code</a></li>
          <li class="divider"> </li>
          <li id="formattingMenu">
            <a href="#" class="dropdown-toggle" type="button" data-toggle="dropdown"> Formatting mode
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li><a href="#" id="noFormatting">No Formatting</a></li>
              <li><a href="#" class="disabled" id="autoFormatting">Auto-Formatting</a></li>
            </ul>
            <div class="hidden" id="formattingMode">1</div>
          </li>
        </ul>

          <div id="brightnessBar">
            <ul>
              <li><a href="#" class="brightnessButton" id="brightnessButtonMinus"> - </a></li>
              <li class="bar" id="brightnessBar1"></li>
              <li class="bar" id="brightnessBar2"></li>
              <li class="bar" id="brightnessBar3"></li>
              <li><a href="#" class="brightnessButton" id="brightnessButtonPlus"> + </a></li>
            </ul>
            <div class="hidden" id="brightnessLevel"></div>
          </div>
      </nav>
    </div>

  </div>
  `
}

function initAppbar() {

  var drivelist = require('drivelist')
  var fs = require('fs')
  var path = require('path')
  var dialog = require('electron').remote.dialog


  function menuConfirm() {
    if (confirm("By continuing, you will lose all unsaved code.\n Are you sure you want to continue?")) {
      window.location.href="../index.html";
    }
    $(this).blur();
  };

  function flashMicrobit() {
    var microbitProgrammer, hexCode, hexFile, found = false, drive, brightness;

    microbitProgrammer = new MicrobitProgrammer();
    hexCode = document.getElementById('hexCode').childNodes[0].nodeValue.split(' ').map(function(item) {return parseInt(item,16)});
    brightness = Math.min(3, Math.max(parseInt($("#brightnessLevel").text()),0));
    hexFile = microbitProgrammer.generateHexFile(hexCode, 3 - brightness);

    if (hexFile === null) {
      alert("Can't flash an empty file");
    }
    else {
      drivelist.list( function (errors, drives) {
        drives.forEach( function (drive) {
          if (!found && !drive.system && ( fs.existsSync(path.join(drive.mountpoints[0].path, "MICROBIT.HTM") ))) {
            found = true;
            /* The file extension has to be '.hex' to work on Windows */
            fs.writeFile( path.join(drive.mountpoints[0].path, 'stack.hex'), hexFile, function (error) {
              if (error) {
                alert('There was an error flashing the Micro:bit: ' + error)
              }
            });
          }
        });
        if (!found) {
          alert('Could not find the Micro:bit');
        }
      });
    }
    $(this).blur();
  }

  function loadCode() {
    if (confirm("By continuing, you will lose all unsaved code.\n Are you sure you want to continue?")) {

      var environment = $("#environment"),
        defaultPath, filters, name, extension;

      /* Text Editor file .sck extension */
      if (environment.text() === "my-code-editor") {
        extension = "sck";
        name = "." + extension;
      }
      /* Block Editor file .bck extension */
      else {
        extension = "bck";
        name = "." + extension;
      }

      defaultPath = require('electron').remote.app.getAppPath();
      filters = [ { name: name, extensions: [ extension ] } ];

      dialog.showOpenDialog( { defaultPath : defaultPath, filters: filters },
        (fileNames) => {
        if (fileNames === undefined) {
          return;
        }

        fs.readFile(fileNames[0], 'utf-8', function (err, code) {
          /* Load code into div and trigger update */
          if (environment.text() === "my-code-editor") {
            var highlevelCode = $("#highlevelCode");
            highlevelCode.empty();
            highlevelCode.text(code);
            highlevelCode.trigger("LOADED_CODE");
          }
          /* Clear Blockly workspace and load file one */
          else {
            Blockly.mainWorkspace.clear();
            var blockCode = code.split('$$$$');
            if (blockCode.length != 3) {
              alert("This file was not a blocks file.");
            }
            else {
              updateBlocklyToolbox(blockCode[0], blockCode[1])
              Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom(blockCode[2]), Blockly.mainWorkspace);
            }
          }
        })
    })
   }
   $(this).blur();
  }

  function saveCode() {
    var code, environment = $("#environment"),
      defaultPath, filters, name, extension;
    /* Text Editor file .sck extension */
    if (environment.text() === "my-code-editor") {
      code = $("#highlevelCode").text();
      extension = "sck";
      name = "." + extension;
    }
    /* Block Editor file .bck extension */
    else {
      code = $("#allProceduresUsage").text() + "$$$$" + $("#allFunctionsUsage").text() + "$$$$" + Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(Blockly.mainWorkspace));
      extension = "bck";
      name = "." + extension;
    }

    defaultPath = require('electron').remote.app.getAppPath() + "/mycode." + extension;
    filters = [ { name: name, extensions: [ extension ] } ]


    dialog.showSaveDialog( { defaultPath : defaultPath, filters: filters },
      (fileName) => {
      if (fileName === undefined) {
        return;
      }


      fs.writeFile(fileName, code, (err) => {
        if (err) {
          alert("An error occured creating the file " + err.message)
        }
      })
    })
    $(this).blur();
  }

  function switchEnvironment() {
    var environment = $("#environment");
    if (environment.text() === "my-code-editor") {
      $("#blocklyApp").removeClass("hidden");
      $("#codingApp").addClass("hidden");
      $("#formattingMenu").addClass("hidden");
      environment.text('my-blockly');
      $("#bottom").trigger("POSITION_CHANGE")
    }
    else {
      $("#blocklyApp").addClass("hidden");
      $("#codingApp").removeClass("hidden");
      $("#formattingMenu").removeClass("hidden");
      environment.text('my-code-editor');
    }
    $(this).blur();
  }

  function clearCode() {
    if (confirm("By continuing, you will lose all unsaved code.\n Are you sure you want to continue?")) {
      Blockly.mainWorkspace.clear();
      var start_xml = Blockly.Xml.textToDom("<xml xmlns=\"http://www.w3.org/1999/xhtml\"><block type=\"start_block\" id=\"rhhd+t8qB84Y,Vy`{8Mp\" x=\"250\" y=\"236\"></block></xml>");
      Blockly.Xml.domToWorkspace(start_xml, Blockly.mainWorkspace);
      var highlevelCode = $("#highlevelCode");

      highlevelCode.empty();
      highlevelCode.trigger("LOADED_CODE");

    }
    $(this).blur();
  }

  function autoFormatting() {
    $("#formattingMode").text(1);
    $("#noFormatting").removeClass("disabled");
    $("#autoFormatting").addClass("disabled");
  }

  function noFormatting() {
    $("#formattingMode").text(0);
    $("#noFormatting").addClass("disabled");
    $("#autoFormatting").removeClass("disabled");
  }

  function setBrightnessLevel(brightness) {
    $("#brightnessLevel").text(brightness);
    switch (brightness) {
      case 0:
        $("#brightnessBar1").css("background-color", "#505000");
        $("#brightnessBar2").css("background-color", "#505000");
        $("#brightnessBar3").css("background-color", "#505000");
        break;
      case 1:
        $("#brightnessBar1").css("background-color", "#BBBB00");
        $("#brightnessBar2").css("background-color", "#505000");
        $("#brightnessBar3").css("background-color", "#505000");
        break;
      case 2:
        $("#brightnessBar1").css("background-color", "#BBBB00");
        $("#brightnessBar2").css("background-color", "#BBBB00");
        $("#brightnessBar3").css("background-color", "#505000");
        break;
      case 3:
        $("#brightnessBar1").css("background-color", "#BBBB00");
        $("#brightnessBar2").css("background-color", "#BBBB00");
        $("#brightnessBar3").css("background-color", "#AAAA00");
        break;
      default:
        break;
    }
  }

  function initBrightnessBar() {
    setBrightnessLevel(2);
  }

  function brightnessDown() {
    var brightness;
    brightness = parseInt($("#brightnessLevel").text());
    if (brightness > 0) {
      setBrightnessLevel(brightness - 1);
    }
    $(this).blur();
  }

  function brightnessUp() {
    var brightness;
    brightness = parseInt($("#brightnessLevel").text());
    if (brightness < 3 ) {
      setBrightnessLevel(brightness + 1);
    }
    $(this).blur();
  }

  $(function(){

    var menuButton, loadButton, saveButton, flashMicrobitButton,
      switchEnvironmentButton, clearButton, brightnessButtonMinus,
      brightnessButtonPlus, noFormattingButton, autoFormattingButton;

      menuButton = $("#menuButton");
      menuButton.click(menuConfirm);

      loadButton = $("#loadFileButton");
      loadButton.click(loadCode);

      saveButton = $("#saveFileButton");
      saveButton.click(saveCode);

      flashMicrobitButton = $("#flashMicrobitButton");
      flashMicrobitButton.click(flashMicrobit);

      switchEnvironmentButton = $("#switchEnvironmentButton");
      switchEnvironmentButton.click(switchEnvironment);

      clearButton = $("#clearButton");
      clearButton.click(clearCode);

      noFormattingButton = $("#noFormatting");
      noFormattingButton.click(noFormatting);

      autoFormattingButton = $("#autoFormatting");
      autoFormattingButton.click(autoFormatting);

      initBrightnessBar();
      brightnessButtonMinus = $("#brightnessButtonMinus");
      brightnessButtonMinus.click(brightnessDown);
      brightnessButtonPlus = $("#brightnessButtonPlus");
      brightnessButtonPlus.click(brightnessUp);
  })


}
