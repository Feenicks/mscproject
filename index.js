var app = require('electron').app;
var BrowserWindow = require('electron').BrowserWindow;


app.on('ready', function(){
  var mainWindow = new BrowserWindow({
    width: 1600,
    height: 1200
  });
  mainWindow.loadURL('file://' + __dirname + '/index.html');

});
