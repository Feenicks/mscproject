'use strict'


function Stdlib () {
  return {
    procedures : [
    /*  [  "sleep"
      , [ "[0x80 0x01]", 1 ]
    ]
    , */ [ "tone"
      , [ "[0x81 0x01]", 1 ]
      ]
    ,  [ "beep"
      , [ "[0x82 0x02]", 2 ]
      ]
    , [ "rgb"
      , [ "[0x83 0x03]", 3 ]
      ]
    , [ "colour"
      , [ "[0x84 0x01]", 1 ]
    ]
    , [ "flash"
      , [ "[0x85 0x02]", 2 ]
    ]
    , [ "wait"
      , [ "[0x1f]", 1 ]
    ]
    , [ "led"
      , [ "[0x88 0x02]", 2 ]
    ]
    ]

  , functions : [
    [ "accel"
    , [ "[0x87 0x30]", 0, 3 ]
    ]
  , [ "next_note"
    , [ "18904 MUL 17843 DIV", 1, 1 ]
    ]
  , [ "prev_note"
    , [ "17843 MUL 18904 DIV 1 ADD", 1, 1 ]
    ]
  , [ "next_colour"
    , [ "1 ADD 8 MOD", 1, 1 ]
  ]
  , [ "prev_colour"
    , [ "7 ADD 8 MOD", 1, 1 ]
  ]
  , [ "random"
    , [ "[0x17]", 1, 1 ]
  ]
  ]

  , proceduresUsage: [
  /*    "sleep time_in_ms"
    , */"wait time_in_ms"
    , "tone pitch_in_hz"
    , "beep pitch_in_hz time_in_ms"
    , "rgb red green blue"
    , "colour colour"
    , "flash colour time_in_ms"
    , "led colour pixel_number"
    ]

  , functionsUsage: [
      "accel"
    , "next_note pitch_in_hz"
    , "prev_note pitch_in_hz"
    , "next_colour colour"
    , "prev_colour colour"
    , "random upper_bound"
    ]
  }
}
